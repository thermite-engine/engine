#define g_PI 3.1415926535897932384626433832795

// Trowbridge-Reitz GGX normal distribution function (NDF)
// N = surface normal?
// H = halfway vector (halfway between light vector and view vector)
float DistributionGGX( const float NdotH, const float roughness )
{
	const float a = roughness * roughness;
	const float a2 = a * a;
	const float NdotH2 = NdotH * NdotH;
	
	const float numerator = a2;
	float denominator = ( NdotH2 * ( a2 - 1.0 ) + 1.0 );
	denominator = g_PI * denominator * denominator;
	
	return numerator / denominator;
}

float GeometrySchlickGGX( float NdotV, float roughness )
{
	float r = ( roughness + 1.0 );
	float k = ( r * r ) / 8.0;

	float num = NdotV;
	float denom = NdotV * ( 1.0 - k ) + k;

	return num / denom;
}

float GeometrySmith( const float NdotL, const float NdotV, const float roughness )
{
	const float ggx2 = GeometrySchlickGGX( NdotV, roughness );
    const float ggx1 = GeometrySchlickGGX( NdotL, roughness );
	
    return ggx1 * ggx2;
}

vec3 fresnelSchlick( float cosTheta, vec3 F0 )
{
	return F0 + ( 1.0 - F0 ) * pow( 1.0 - cosTheta, 5.0 );
}