#version 460

struct Transformation
{
	mat4 modelView;
	mat4 projection;
};


layout ( std430, binding = 1 ) buffer ModelView_Projection {
	Transformation transforms[];
};

layout( push_constant ) uniform Properties
{
	vec4 color;
} properties;


layout ( location = 0 ) in vec3 inPosition;
layout ( location = 1 ) in vec2 inTexCoord;
layout ( location = 2 ) in vec4 inColor;

layout ( location = 0 ) out vec2 outfragTexCoord;
layout ( location = 1 ) out vec4 outfragColor;

void main()
{
	if (gl_InstanceIndex >= transforms.length())
		return;
		
	outfragColor = inColor * properties.color;
	outfragTexCoord = inTexCoord;
	
	vec4 P = transforms[gl_InstanceIndex].modelView * vec4(inPosition, 1.0);
	gl_Position = transforms[gl_InstanceIndex].projection * P;
}
