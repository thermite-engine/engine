#version 460

struct Transformation
{
	mat4 mvp;
	mat4 model;
};

layout ( std430, binding = 3 ) buffer MVPModelBlock
{
	Transformation transformations[];
};

layout ( binding = 5 ) uniform View
{
	vec3 pos;
} view;

// UNUSED
layout ( location = 4 ) in uvec4 inBoneIndex;
layout ( location = 5 ) in vec4 inBoneWeight;

layout ( location = 0 ) in vec3 inPosition;
layout ( location = 1 ) in vec3 inNormal;
layout ( location = 2 ) in vec2 inTexCoord;
layout ( location = 3 ) in vec4 inColor;

layout ( location = 0 ) out vec2 outfragTexCoord;
layout ( location = 1 ) out vec4 outfragColor;
layout ( location = 2 ) out vec3 outfragPos;
layout ( location = 3 ) out vec3 outViewPos;
layout ( location = 4 ) out vec3 outNormal;

void main()
{					
	if (gl_InstanceIndex >= transformations.length())
		return;

	gl_Position = transformations[gl_InstanceIndex].mvp * vec4( inPosition, 1.0 );
	
	outfragColor = inColor;
	outfragTexCoord = inTexCoord;
	outNormal = inNormal;
	outfragPos = vec3( transformations[gl_InstanceIndex].model * vec4( inPosition, 1.0 ) );
	outViewPos = view.pos;
}
