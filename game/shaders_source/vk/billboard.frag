#version 460

layout( location = 0 ) out vec4 outFragColor;


layout ( location = 0 ) in vec2 inFragTexCoord;
layout ( location = 1 ) in vec4 inFragColor; // Vertex color

layout( binding = 0 ) uniform sampler2D billboardTexture;

void main()
{
	vec4 finalColor = inFragColor * texture( billboardTexture, inFragTexCoord );
	
	if (finalColor.a < 0.99)
		discard;

	outFragColor = finalColor;
}
