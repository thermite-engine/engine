#version 460

layout( location = 0 ) out vec3 _modulate;
layout( location = 1 ) out vec4 _accum;
layout( location = 2 ) out float _revealage;


layout ( location = 0 ) in vec2 inFragTexCoord;
layout ( location = 1 ) in vec4 inFragColor; // Vertex color

layout( binding = 0 ) uniform sampler2D billboardTexture;

layout ( set = 1, input_attachment_index = 0, binding = 0 ) uniform subpassInput samplerDepth;

#include "common_transparent.fsh"

void main()
{
	vec4 finalColor = inFragColor * texture( billboardTexture, inFragTexCoord );

	if (finalColor.a >= 0.99)
	{
		discard;
	}
	else
	{
		//outFragColor = finalColor;
		const vec3 transmit = vec3(0.0, 0.0, 0.0);
		writePixel(finalColor, transmit);
	}
}
