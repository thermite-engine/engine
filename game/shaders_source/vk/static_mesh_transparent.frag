#version 460

struct PointLight
{
	vec3 position;
	vec3 color;
};

layout ( binding = 2 ) uniform LightInfo
{
	uint pointLightCount;
	vec3 ambientLightColor;
} lightInfo;

layout ( std430, binding = 4 ) buffer PointLightBuffer
{
	PointLight lights[];
} lightBuffer;

const float g_metallic = 1.0f;
const float g_roughness = 0.5f;
const float g_ao = 1.0f;

layout( location = 0 ) out vec3 _modulate;
layout( location = 1 ) out vec4 _accum;
layout( location = 2 ) out float _revealage;

layout ( location = 0 ) in vec2 inFragTexCoord;
layout ( location = 1 ) in vec4 inFragColor; // Vertex color
layout ( location = 2 ) in vec3 inFragPos;
layout ( location = 3 ) in vec3 inViewPos;
layout ( location = 4 ) in vec3 inNormal;

layout( binding = 0 ) uniform sampler2D diffuseSampler;
layout( binding = 1 ) uniform sampler2D normalSampler;

layout ( set = 1, input_attachment_index = 0, binding = 0 ) uniform subpassInput samplerDepth;

#include "common.fsh"
#include "common_transparent.fsh"

vec3 GetNormalFromMap()
{
	vec3 tangentNormal = texture( normalSampler, inFragTexCoord ).rgb * 2.0 - 1.0;
	
	// I believe these are supposed to represent edges, but I'm not certain
	vec3 edge1 = dFdx( inFragPos );
	vec3 edge2 = dFdy( inFragPos );
	vec2 uv1 = dFdx( inFragTexCoord );
	vec2 uv2 = dFdy( inFragTexCoord );
	
	vec3 N = normalize( inNormal );
	vec3 T = normalize( edge1 * uv2.y - edge2 * uv1.y );
	vec3 B = normalize( cross( N, T ) );
	mat3 TBN = mat3( T, B, N );
	
	return normalize(TBN * tangentNormal);
}

void main()
{
	// Base color sampled from diffuse texture
	vec4 albedo = texture( diffuseSampler, inFragTexCoord );

	if (albedo.a >= 0.99)
		discard;
	
	// Normal
	vec3 N = GetNormalFromMap();
	
	// Direction from view to fragment position
	vec3 V = normalize( inViewPos - inFragPos );
	
	vec3 F0 = vec3( 0.04 );
	F0 = mix( F0, albedo.xyz, g_metallic );
	
	const float NdotV = max( dot( N, V ), 0.0 );
	
	vec3 Lo = vec3( 0.0 );	
	for ( uint i = 0; i < lightInfo.pointLightCount; ++i )
	{
		const vec3 L = normalize( lightBuffer.lights[ i ].position - inFragPos );
		const vec3 H = normalize( V + L );
		
		const float NdotL = max( dot( N, L ), 0.0 );
		const float NdotH = max( dot( N, H ), 0.0 );
		
		const float distance = length( lightBuffer.lights[ i ].position - inFragPos );
		const float attenuation = 1.0 / max( distance * distance, 0.0001 );

		const vec3 radiance = lightBuffer.lights[ i ].color * attenuation;
		
		const float NDF = DistributionGGX( NdotH, g_roughness );
		const float G = GeometrySmith( NdotL, NdotV, g_roughness );
		const vec3 F = fresnelSchlick( max( dot( H, V ), 0.0 ), F0 );
		
		const vec3 kS = F;
		vec3 kD = vec3( 1.0 ) - kS;
		kD *= 1.0 - g_metallic;
		
		const vec3 numerator = NDF * G * F;
		const float denominator = 4.0 * NdotV * NdotL;
		const vec3 specular = numerator / max( denominator, 0.0001 );
		
		Lo += ( ( kD * albedo.xyz / g_PI ) + specular ) * radiance * NdotL;
	}
	
	vec3 ambient = lightInfo.ambientLightColor.xyz * albedo.xyz * g_ao;
	
	// No idea how the hell transmission actually works
	//const vec3 transmit = vec3( 0.3, 0.55, 0.73 );
	const vec3 transmit = vec3( 0.0, 0.0, 0.0 );
	vec4 color = vec4( ambient + Lo, albedo.w );
	
	writePixel(color, transmit);
}