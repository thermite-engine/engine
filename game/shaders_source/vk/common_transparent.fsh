// TODO: Pass these in as part of a buffer or something, this is dumb
const float g_zNear = 0.01f;
const float g_zFar = 10000.0f;

float linearize_depth(float d, float zNear, float zFar)
{
    return zNear * zFar / (zFar + d * (zNear - zFar));
}

void writePixel(vec4 premultipliedReflect, vec3 transmit)
{
	 /* NEW: Perform this operation before modifying the coverage to account for transmission. */
	_modulate = premultipliedReflect.a * (vec3(1.0) - transmit);
	
    /* Modulate the net coverage for composition by the transmission. This does not affect the color channels of the
       transparent surface because the caller's BSDF model should have already taken into account if transmission modulates
       reflection. This model doesn't handled colored transmission, so it averages the color channels. See 

          McGuire and Enderton, Colored Stochastic Shadow Maps, ACM I3D, February 2011
          http://graphics.cs.williams.edu/papers/CSSM/

       for a full explanation and derivation.*/

	premultipliedReflect.a *= 1.0 - clamp((transmit.r + transmit.g + transmit.b) * (1.0 / 3.0), 0, 1);

    
	// Intermediate terms to be cubed
	float tmp = (premultipliedReflect.a * 8.0 + 0.01) * (-gl_FragCoord.z * 0.95 + 1.0);
	
	float csZ = linearize_depth(subpassLoad(samplerDepth).r, g_zNear, g_zFar);

	//float csZ = abs(1.0 / gl_FragCoord.w);
	tmp /= sqrt(abs(csZ));
	
	float w    = clamp(tmp * tmp * tmp * 1e3, 1e-2, 3e2);
	_accum     = premultipliedReflect * w;
	_revealage = premultipliedReflect.a;
}