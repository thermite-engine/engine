#version 460

layout( location = 0 ) out vec4 outFragColor;


layout ( location = 0 ) in vec3 inFragTexCoord;
layout ( location = 1 ) in vec4 inFragColor; // Vertex color

layout ( binding = 0 ) uniform samplerCube skybox;

void main()
{
	// Base color sampled from diffuse texture
	outFragColor = texture( skybox, inFragTexCoord ) * inFragColor;
}
