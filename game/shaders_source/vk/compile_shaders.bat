SET glslValidator=%VULKAN_SDK%\Bin\glslc.exe

%glslValidator% static_mesh.vert -o ../../shaders/vk/static_mesh.vert.spv
%glslValidator% static_mesh.frag -o ../../shaders/vk/static_mesh.frag.spv

%glslValidator% static_mesh_transparent.vert -o ../../shaders/vk/static_mesh_transparent.vert.spv
%glslValidator% static_mesh_transparent.frag -o ../../shaders/vk/static_mesh_transparent.frag.spv

%glslValidator% transparent_composition.vert -o ../../shaders/vk/transparent_composition.vert.spv
%glslValidator% transparent_composition.frag -o ../../shaders/vk/transparent_composition.frag.spv

%glslValidator% ui.vert -o ../../shaders/vk/ui.vert.spv
%glslValidator% ui.frag -o ../../shaders/vk/ui.frag.spv

%glslValidator% skybox.vert -o ../../shaders/vk/skybox.vert.spv
%glslValidator% skybox.frag -o ../../shaders/vk/skybox.frag.spv

%glslValidator% billboard.vert -o ../../shaders/vk/billboard.vert.spv
%glslValidator% billboard.frag -o ../../shaders/vk/billboard.frag.spv

%glslValidator% billboard_transparent.vert -o ../../shaders/vk/billboard_transparent.vert.spv
%glslValidator% billboard_transparent.frag -o ../../shaders/vk/billboard_transparent.frag.spv