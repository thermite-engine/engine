#version 460

layout ( input_attachment_index = 0, binding = 0 ) uniform subpassInput samplerAccum;
layout ( input_attachment_index = 1, binding = 1 ) uniform subpassInput samplerRevealage;

layout ( location = 0 ) out vec4 outColor;

float maxComponent(vec4 v)
{
	return max( max( max( v.x, v.y ), v.z ), v.w );
}

void main()
{
	float revealage = subpassLoad( samplerRevealage ).r;
	
	if ( revealage == 1.0 )
	{
		// Save the blending and color texture fetch cost
		discard; 
	}

	vec4 accum = subpassLoad( samplerAccum );
	
	// Suppress overflow
	if ( isinf( maxComponent( abs( accum ) ) ) )
	{
		accum.rgb = vec3( accum.a );
	}
	
	outColor = vec4(accum.rgb / max(accum.a, 0.00001), revealage);
	//vec3 averageColor = accum.rgb / max( accum.a, 0.00001 );
	// dst' =  (accum.rgb / accum.a) * (1 - revealage) + dst * revealage
	//outColor = vec4(averageColor, 1.0 - revealage);
}
