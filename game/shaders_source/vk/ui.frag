#version 460

layout( location = 0 ) out vec4 outFragColor;


layout ( location = 0 ) in vec2 inFragTexCoord;
layout ( location = 1 ) in vec4 inFragColor; // Vertex color

layout( binding = 0 ) uniform sampler2D fontTexture;

void main()
{
	outFragColor = inFragColor * texture( fontTexture, inFragTexCoord );
}
