glslangValidator -V static_mesh.vert -o ../../shaders/vk/static_mesh.vert.spv
glslangValidator -V static_mesh.frag -o ../../shaders/vk/static_mesh.frag.spv

glslangValidator -V static_mesh_transparent.vert -o ../../shaders/vk/static_mesh_transparent.vert.spv
glslangValidator -V static_mesh_transparent.frag -o ../../shaders/vk/static_mesh_transparent.frag.spv

glslangValidator -V transparent_composition.vert -o ../../shaders/vk/transparent_composition.vert.spv
glslangValidator -V transparent_composition.frag -o ../../shaders/vk/transparent_composition.frag.spv

glslangValidator -V ui.vert -o ../../shaders/vk/ui.vert.spv
glslangValidator -V ui.frag -o ../../shaders/vk/ui.frag.spv

glslangValidator -V skybox.vert -o ../../shaders/vk/skybox.vert.spv
glslangValidator -V skybox.frag -o ../../shaders/vk/skybox.frag.spv

glslangValidator -V billboard.vert -o ../../shaders/vk/billboard.vert.spv
glslangValidator -V billboard.frag -o ../../shaders/vk/billboard.frag.spv

glslangValidator -V billboard_transparent.vert -o ../../shaders/vk/billboard_transparent.vert.spv
glslangValidator -V billboard_transparent.frag -o ../../shaders/vk/billboard_transparent.frag.spv