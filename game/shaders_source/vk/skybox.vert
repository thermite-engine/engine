#version 460

layout( push_constant ) uniform TransformBlock
{
	mat4 view;
	mat4 projection;
} transform;

layout ( location = 0 ) in vec3 inPosition;
layout ( location = 1 ) in vec4 inColor;

layout ( location = 0 ) out vec3 outfragTexCoord;
layout ( location = 1 ) out vec4 outfragColor;

void main()
{
	vec4 pos = transform.projection * transform.view * vec4( inPosition, 1.0 );
	gl_Position = pos.xyww;
	
	outfragColor = inColor;
	outfragTexCoord = inPosition;
}
