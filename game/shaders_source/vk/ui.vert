#version 460

//layout ( push_constant ) uniform MatrixBlock {
	//mat4 value;
//} ProjMatrix;

layout ( push_constant ) uniform Transformation
{
	vec2 scale;
	vec2 translate;
} transformation;

layout ( location = 0 ) in vec2 inPosition;
layout ( location = 1 ) in vec2 inTexCoord;
layout ( location = 2 ) in vec4 inColor;

layout ( location = 0 ) out vec2 outfragTexCoord;
layout ( location = 1 ) out vec4 outfragColor;

void main()
{
	outfragColor = inColor;
	outfragTexCoord = inTexCoord;
	gl_Position = vec4(inPosition * transformation.scale + transformation.translate, 0, 1);


	//gl_Position = ProjMatrix.value * vec4(inPosition, 0.0, 1.0);
	//outfragColor = vec4(inColor[0]/255.0, inColor[1]/255.0, inColor[2]/255.0, inColor[3]/255.0);
	//outfragTexCoord = inTexCoord;
}
