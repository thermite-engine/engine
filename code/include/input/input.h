#pragma once

#include <SDL2/SDL.h>
#include "service.h"
#include "defines.h"
#include "imgui.h"

using ActionSetID = uint64_t;

struct Button
{
	ActionSetID actionSet;
	uint64_t buttonMask;
};

struct ActionSetState
{
	uint64_t oldButtonState;
	uint64_t currentButtonState;
};

constexpr bool operator==(const ActionSetState &lhs, const ActionSetState &rhs)
{
	return (lhs.oldButtonState == rhs.oldButtonState) && (lhs.currentButtonState == rhs.currentButtonState);
}

constexpr bool operator!=(const ActionSetState &lhs, const ActionSetState &rhs)
{
	return (lhs.oldButtonState != rhs.oldButtonState) || (lhs.currentButtonState != rhs.currentButtonState);
}

/**
 * @class ITextInput
 * @author Spirrwell
 * @date 10/06/20
 * @brief Interface that serves as a callback for UTF-8 text input
 */
class ITextInput
{
protected:
	virtual ~ITextInput() = default;

public:

	/**
	 * @brief Called when text input is sampled from the keyboard.
	 * @param text A null-terminated UTF-8 string.
	 */
	virtual void OnTextInput(const char *text) = 0;
};

/**
 * @class IWindowEventListener
 * @author Spirrwell
 * @date 11/02/20
 */
class IWindowEventListener
{
protected:
	virtual ~IWindowEventListener() = default;

public:
	/**
	 * @brief Called when a window event is processed.
	 * @param The window event that we just got.
	 */
	virtual void OnWindowEvent(const SDL_WindowEvent &event) = 0;
};

/**
 * @class IInput
 * @author Spirrwell
 * @date 04/25/20
 * @file input.h
 * @brief Handles SDL events and input from input devices.
 * @note This is really barebones at the moment.
 */
class IInput
{
protected:
	virtual ~IInput() = default;

public:

	/**
	 * @brief Sets up the ImGuiContext for input handling/clipboard usage.
	 * @param ctx A pointer to an ImGuiContext.
	 */
	virtual void SetupImGuiContext(ImGuiContext *ctx) = 0;

	/**
	 * @brief Handles input for the given ImGuiContext.
	 * @param ctx A pointer to an ImGuiContext.
	 */
	virtual void HandleUI(ImGuiContext *ctx) = 0;

	/**
	 * @brief Gets the number of mouse button clicks.
	 * @param mouseButton The mouse button to check (SDL_BUTTON_LEFT, SDL_BUTTON_RIGHT, etc.)
	 * @return The number of clicks for the specified button.
	 */
	virtual int GetClicks(uint8_t mouseButton) const = 0;

	/**
	 * @brief Get mouse wheel motion in X direction.
	 * @return Mouse wheel motion in the X direction
	 */
	virtual int GetMouseWheelX() const = 0;

	/**
	 * @brief Get mouse wheel motion in Y direction.
	 * @return Mouse wheel motion in the Y direction
	 */
	virtual int GetMouseWheelY() const = 0;

	/**
	 * @brief Gets the X position of cursor if NOT in relative mouse mode.
	 * @return The X position of the cursor.
	 */
	virtual int GetMouseX() const = 0;

	/**
	 * @brief Gets the Y position of cursor if NOT in relative mouse mode.
	 * @return The Y position of the cursor.
	 */
	virtual int GetMouseY() const = 0;

	/**
	 * @brief Gets the position of cursor if NOT in relative mouse mode.
	 * @param x X position stored here.
	 * @param y Y position stored here.
	 */
	virtual void GetMousePos(int &x, int &y) const = 0;

	/**
	 * @brief Registers an ITextInput interface to call.
	 * @param textInput The interface to register.
	 */
	virtual void RegisterTextInputInterface(ITextInput *textInput) = 0;

	/**
	 * @brief Unregisters an ITextInput interface.
	 * @param textInput The interface to unregister.
	 */
	virtual void UnregisterTextInputInterface(ITextInput *textInput) = 0;

	/**
	 * @brief Registers an IWindowEventListener interface.
	 * @param listener The listener to register.
	 */
	virtual void RegisterWindowEventListener(IWindowEventListener *listener) = 0;

	/**
	 * @brief Unegisters an IWindowEventListener interface.
	 * @param listener The listener to unregister.
	 */
	virtual void UnregisterWindowEventListener(IWindowEventListener *listener) = 0;

	/**
	 * @brief Checks if mouse button is pressed
	 * @param mouseButton The mouse button to check (SDL_BUTTON_LEFT, SDL_BUTTON_RIGHT, etc.)
	 * @return True if the mouse button is pressed.
	 */
	virtual bool IsMouseButtonPressed(uint8_t mouseButton) const = 0;

	/**
	 * @brief Checks if mouse button is released
	 * @param mouseButton The mouse button to check (SDL_BUTTON_LEFT, SDL_BUTTON_RIGHT, etc.)
	 * @return True if the mouse button is released.
	 */
	virtual bool IsMouseButtonReleased(uint8_t mouseButton) const = 0;

	/**
	 * @brief Checks if mouse button was JUST pressed
	 * @param mouseButton The mouse button to check (SDL_BUTTON_LEFT, SDL_BUTTON_RIGHT, etc.)
	 * @return True if the mouse button was JUST pressed.
	 */
	virtual bool IsMouseButtonJustPressed(uint8_t mouseButton) const = 0;

	/**
	 * @brief Checks if mouse button was JUST released.
	 * @param mouseButton The mouse button to check (SDL_BUTTON_LEFT, SDL_BUTTON_RIGHT, etc.)
	 * @return True if the mouse button was JUST released.
	 */
	virtual bool IsMouseButtonJustReleased(uint8_t mouseButton) const = 0;

	/**
	 * @brief Checks if keyboard key is pressed.
	 * @param key The key to check.
	 * @return True if key is pressed.
	 */
	virtual bool IsKeyPressed(SDL_Keycode key) const = 0;

	/**
	 * @brief Checks if keyboard key is released.
	 * @param key The key to check.
	 * @return True if key is released.
	 */
	virtual bool IsKeyReleased(SDL_Keycode key) const = 0;

	/**
	 * @brief Checks if keyboard key was JUST pressed.
	 * @param key The key to check.
	 * @return True if key was JUST pressed.
	 */
	virtual bool IsKeyJustPressed(SDL_Keycode key) const = 0;

	/**
	 * @brief Checks if keyboard key was JUST released.
	 * @param key The key to check.
	 * @return True if key was JUST released.
	 */
	virtual bool IsKeyJustReleased(SDL_Keycode key) const = 0;

	/**
	 * @brief Gets the change in mouse position on the X axis
	 * @return The change in mouse position on the X axis
	*/
	virtual int GetMouseDeltaX() const = 0;

	/**
	 * @brief Gets the change in mouse position on the Y axis
	 * @return The change in mouse position on the Y axis
	*/
	virtual int GetMouseDeltaY() const = 0;

	/**
	 * @brief Checks if button is pressed.
	 * @param button The button to check.
	 * @return True if button is pressed.
	 */
	virtual bool IsPressed(const Button &button) const = 0;

	/**
	 * @brief Checks if button is released.
	 * @param button The button to check.
	 * @return True if button is released.
	 */
	virtual bool IsReleased(const Button &button) const = 0;

	/**
	 * @brief Checks if button was JUST pressed.
	 * @param button The button to check.
	 * @return True if button was JUST pressed.
	 */
	virtual bool IsJustPressed(const Button &button) const = 0;

	/**
	 * @brief Checks if button was JUST released.
	 * @param button The button to check.
	 * @return True if button was JUST released.
	 */
	virtual bool IsJustReleased(const Button &button) const = 0;

	/**
	 * @brief Creates an action set.
	 * @param name An identifier to associate this action set with.
	 * @return An ID that represents the action set we just created.
	*/
	virtual ActionSetID CreateActionSet(const std::string &name) = 0;

	/**
	 * @brief Creates a button for the specified action set.
	 * @param actionSet The action set to create the button for.
	 * @param name An identifier to associate this button with.
	 * @return The button we just created.
	*/
	virtual Button CreateButton(ActionSetID actionSet, const std::string &name) = 0;

	/**
	 * @brief Sets the currently active action set used for polling button input.
	 * @param actionSet The action set to be activated.
	*/
	virtual void SetActiveActionSet(ActionSetID actionSet) = 0;

	/**
	 * @brief Gets the currently active action set.
	 * @return An ID representing the current action set.
	*/
	virtual ActionSetID GetActiveActionSet() const = 0;

	/**
	 * @brief Returns the number of action sets that have been defined.
	 * @return The number of action sets defined.
	*/
	virtual uint64_t GetNumActionSets() const = 0;

	/**
	 * @brief Gets the number of buttons that have been defined for a particular action set.
	 * @param actionSet The action set to check.
	 * @return The number of buttons defined for this action set.
	*/
	virtual uint64_t GetNumButtonsForActionSet(ActionSetID actionSet) const = 0;

	/**
	 * @brief Gets the state of an action set, button masks for current and previous frames.
	 * @param actionSet The action set to check.
	 * @return The state of the action set.
	*/
	virtual ActionSetState GetActionSetState(ActionSetID actionSet) const = 0;

	/**
	 * @brief Gets the state of all defined action sets.
	 * @param states Action set states are output to this.
	*/
	virtual void GetActionSetStates(std::vector<ActionSetState> &states) const = 0;

	/**
	 * @brief Binds a key to a particular button.
	 * @param button The button to bind the key to.
	 * @param key The key we're binding to the button.
	 * @return The button we passed in, useful for syntax sugar.
	*/
	virtual const Button &BindKey(const Button &button, SDL_Keycode key) = 0;

	/**
	 * @brief Unbinds a key from a particular button.
	 * @param button The button to unbind the key from.
	 * @param key The key we're unbinding from the button.
	 * @return The button we passed in, useful for syntax sugar.
	*/
	virtual const Button &UnbindKey(const Button &button, SDL_Keycode key) = 0;

	/**
	 * @brief Binds a mouse button to a particular button.
	 * @param button The button to bind the mouse button to.
	 * @param mouseButton The mouse button we're binding to the button.
	 * @return The button we passed in, useful for syntax sugar.
	*/
	virtual const Button &BindMouseButton(const Button &button, uint8_t mouseButton) = 0;

	/**
	 * @brief Unbinds a mouse button from a particular button.
	 * @param button The button to unbind the mouse button from.
	 * @param mouseButton The mouse button we're unbinding from the button.
	 * @return The button we passed in, useful for syntax sugar.
	*/
	virtual const Button &UnbindMouseButton(const Button &button, uint8_t mouseButton) = 0;
};