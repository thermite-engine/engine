#pragma once

// lot of big includes, not ideal
#include <variant>
#include <string>
#include <vector>
#include <map>
#include <optional>

/**
 * @class CConfig
 * @brief Class representing some configuration data.
 * @author swissChili
 * @file config.h
 */
class CConfig
{
public:
	CConfig();
	/**
	 * @brief Create a config string value
	 * @param string The string.
	 */
	CConfig(std::string string);

	void MakeObject();
	void MakeList();
	void MakeString();

	/**
	 * @brief Gets the string value of the config if it is a string.
	 * @returns An optional string. Falsey if it is not a string, truthy otherwise.
	 *          Dereferencing the std::optional yields the string itself.
	 */
	std::optional<std::string> String();

	/**
	 * @brief Get the config as a list.
	 * @returns The list if it exists, std::nullopt otherwise.
	 * @note use config[index] to access list items.
	 */
	std::optional<std::vector<CConfig>> List();

	/**
	 * @brief Gets the config as a sub-object.
	 * @returns The object if it exists, std::nullopt otherwise.
	 * @note use config[key] to access object values;
	 */
	std::optional<std::map<std::string, CConfig>> Object();

	/**
	 * @brief Gets an item at an index in the list.
	 * @throws std::runtime_error if the config is not a list. Check using List() first.
	 * @param index The index.
	 * @returns The config at that index
	 */
	CConfig &operator [](std::size_t index);

	/**
	 * @brief Gets the value of the key in the sub-object.
	 * @throws std::runtime_error if the config is not an object. Check using Object() first.
	 * @param key The key.
	 * @returns The config at that key.
	 */
	CConfig &operator [](std::string key);

	// appends to lsit
	void Append(CConfig item);

	void operator =(std::string val);

private:
	std::variant<std::string, std::vector<CConfig>, std::map<std::string, CConfig>> val;
};

/**
 * @brief Parse a KeyValues string to a config. Ignores conditions.
 * @param keyValues The KeyValues string.
 * @returns The config representing the string.
 */
CConfig ParseKeyValues(std::string keyValues);
