#pragma once

#include "service.h"
#include "input/input.h"

struct ClientInputState
{
	std::vector<ActionSetState> actionSetStates;
	int32_t mouseDeltaX;
	int32_t mouseDeltaY;
};

struct ClientInfo
{
	ClientInputState inputState;
};

class INetworkCallbacks
{
public:
	virtual void OnClientPutInServer(uint32_t clientID) = 0;
	virtual void OnClientDisconnected(uint32_t clientID) = 0;
};

class INetworkServer
{
protected:
	virtual ~INetworkServer() = default;

public:
	virtual uint32_t GetMaxClients() const = 0;
	virtual uint32_t GetNumConnectedClients() const = 0;
	virtual ClientInfo GetConnectedClient(uint32_t clientID) = 0;

	virtual std::vector<ClientInfo> GetConnectedClients() = 0;
};

class INetwork
{
public:

	virtual INetworkServer *CreateServer(uint16_t portNum) = 0;
	virtual void ConnectToServer(const std::string &ip, uint16_t portNum) = 0;

	virtual bool IsHosting() const = 0;
	virtual bool IsConnected() const = 0;
};