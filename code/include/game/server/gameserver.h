#pragma once

class IGame
{
protected:
	virtual ~IGame() = default;
};
