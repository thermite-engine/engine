#pragma once

#include "service.h"

class CThermiteApp : public CServer
{
public:
	CThermiteApp();
	virtual ~CThermiteApp();

	virtual int AppMain(int argc, char *argv[]) = 0;
};
