#pragma once

#include <chrono>
#include <cstdint>

template <typename Rep, uint64_t Interval>
class CIntervalTimer
{
public:

	void Restart() { timeEnd = std::chrono::steady_clock::now() + interval; }
	bool IsReady() const { return timeEnd <= std::chrono::steady_clock::now(); }

private:
	inline static constexpr Rep interval = Rep(Interval);
	std::chrono::steady_clock::time_point timeEnd = {};
};