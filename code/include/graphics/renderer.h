#pragma once

#include "defines.h"
#include "window.h"
#include "scene.h"
#include "view.h"
#include "imgui.h"

/**
 * @class IRenderer
 * @author Arthurdead
 * @date 04/04/20
 * @file renderer.h
 * @brief Renderer interface
 */
class IRenderer
{
protected:
	/**
	 * @brief Renderer interface destructor do not call this use DeleteRenderer instead.
	 */
	virtual ~IRenderer() = default;

public:

	/**
	 * @brief Gets window we're rendering to.
	 * @return A pointer to the window we're rendering to.
	*/
	virtual IWindow *GetWindow() = 0;

	/**
	 * @brief Creates a scene.
	*/
	virtual IScene *CreateScene() = 0;

	/**
	 * @brief Deletes a scene.
	 * @param scene A pointer to the scene.
	*/
	virtual void DeleteScene(IScene *scene) = 0;

	/**
	 * @brief Set's the clear color value for color buffer.
	 * @param red Red value.
	 * @param green Green value.
	 * @param blue Blue vlue.
	 * @param alpha Alpha value.
	*/
	virtual void SetClearColor(float red, float green, float blue, float alpha) = 0;

	/**
	 * @brief Gets the renderer's set clear color.
	 * @param red Red value.
	 * @param green Green value.
	 * @param blue Blue value.
	 * @param alpha Alpha value
	 * @note nullptr values will simply be skipped.
	 */
	virtual void GetClearColor(float *red, float *green, float *blue, float *alpha) = 0;

	/**
	* @brief Begins the current frame to be rendered.
	*/
	virtual void BeginFrame() = 0;

	/**
	 * @brief Draws the current frame from internal draw lists.
	*/
	virtual void DrawFrame() = 0;

	/**
	 * @brief Draws a scene from 3D view and view matrix.
	 * @param scene A pointer to the scene to draw.
	 * @param view3D The 3D view.
	 * @param viewMatrix The view matrix.
	 */
	virtual void DrawScene(IScene *scene, const CView3D &view3D, const glm::mat4 &viewMatrix) = 0;

	/**
	 * @brief Draws UI from a given ImGuiContext.
	 * @param ctx A pointer to the ImGuiContext to draw from.
	 */
	virtual void DrawUI(ImGuiContext *ctx) = 0;

	/**
	* @brief Ends the current frame to be rendered.
	*/
	virtual void EndFrame() = 0;

	/**
	 * @brief Clears any draw commands in our draw lists.
	*/
	virtual void ClearDrawLists() = 0;
};