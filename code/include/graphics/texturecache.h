#pragma once

#include <filesystem>

#include "texture.h"
#include "imageformat.h"

enum class ETextureFallback
{
	White,
	Diffuse_Error2D,
	Diffuse_ErrorCube, // NOTE: We have no single file texture support yet (.DDS, .KTX)
	Normal_Flat
};

class ITextureCache
{
protected:
	virtual ~ITextureCache() = default;

public:

	/**
	 * @brief Loads a texture from the specified path or pulls it from the cache.
	 * @param path The path to load a texture from. (path/to/texture.png)
	 * @param fallback A preloaded fallback texture in the case of load failure.
	 * @param generateMipMaps Generate mipmaps for this texture if true.
	 * @return A pointer to loaded texture or fallback.
	 */
	virtual ITexture *LoadTexture(const std::filesystem::path &path, ETextureFallback fallback, bool generateMipMaps) = 0;

	/**
	 * @brief Destroys the specified texture and removes it from the cache if it exists.
	 * @param texture The texture to destroy.
	 */
	virtual void UnloadTexture(ITexture *texture) = 0;

	/**
	 * @brief Loads a skybox cube texture from multiple files with implicit ETextureFallback::Diffuse_ErrorCube or pulls it from the cache.
	 * @param pathNoExtension Path to texture files without the extension. (up, dn, lf, rt, ft, bk appended automatically)
	 * @param extension The extension to use when loading the skybox. (.png, .tga, .jpg)
	 * @return A pointer to the loaded texture or fallback.
	 */
	virtual ITexture *LoadSkybox(const std::filesystem::path &pathNoExtension, const std::filesystem::path &extension) = 0;

	/**
	 * @brief Loads a skybox cube texture from multiple files with implicit ETextureFallback::Diffuse_ErrorCube or pulls it from the cache.
	 * @param facePaths Paths to each of the faces in the order of front, back, up, down, right, left.
	 * @return A pointer to the loaded texture or fallback.
	 */
	virtual ITexture *LoadSkybox(const std::array<std::filesystem::path, 6> facePaths) = 0;

	/**
	 * @brief Creates a texture of a particular format and type from pixel data.
	 * @param imageType The type of image to create. (2D, Cube)
	 * @param imageFormat The format of the pixel data. (Only RGBA supported :trollface:)
	 * @param width Width of image, or first image in array/cube.
	 * @param height Height of image, or first image in array/cube
	 * @param pixels The pixel data to upload to the texture.
	 * @return A pointer to the created texture.
	 */
	virtual ITexture *CreateTexture(EImageType imageType, EImageFormat imageFormat, uint32_t width, uint32_t height, const void *pixels, bool generateMipMaps) = 0;

};
