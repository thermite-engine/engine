#pragma once

#include <glm/glm.hpp>

class IBillboardSprite
{
protected:
	virtual ~IBillboardSprite() = default;

public:
	virtual void SetColor(const glm::vec4 &color) = 0;
	virtual glm::vec4 GetColor() const = 0;
};