#pragma once

#include "model.h"

#include <filesystem>

class IModelCache
{
protected:
	virtual ~IModelCache() = default;

public:

	/**
	 * @brief Loads a 3D model from a particular path.
	 * @param path Path to the model file.
	 * @return A pointer to the model. This may be NULL on failure.
	 */
	virtual IModel *LoadModel(const std::filesystem::path &path) = 0;
};