#pragma once

#include "glm/glm.hpp"

#include <cstdint>

// TODO: I'm sorry, I was too lazy to document this :(

class IPointLight
{
protected:
	virtual ~IPointLight() = default;

public:
	virtual uint32_t GetLightIndex() const = 0;

	virtual const glm::vec3 &GetPosition() const = 0;
	virtual void SetPosition(const glm::vec3 &position) = 0;

	virtual const glm::vec3 &GetColor() const = 0;
	virtual void SetColor(const glm::vec3 &color) = 0;
};

class ILightState
{
protected:
	virtual ~ILightState() = default;

public:
	virtual IPointLight *CreatePointLight() = 0;
	virtual void DeletePointLight(IPointLight *pointLight) = 0;

	virtual uint32_t GetPointLightCount() const = 0;
	virtual IPointLight *GetPointLight(uint32_t index) = 0;

	virtual const glm::vec3 &GetAmbientLightColor() const = 0;
	virtual void SetAmbientLightColor(const glm::vec3 &color) = 0;
};