#pragma once

#include <glm/glm.hpp>

#include "entity.h"
#include "components/transform.h"

#include "log/log.h"

struct Camera
{
	// NOTE: I actually have no idea how global/local transformations work, so this terminology is probably wrong :D
	glm::vec3 local_forward;
	glm::vec3 local_back;

	glm::vec3 forward;
	glm::vec3 back;
	glm::vec3 up;
	glm::vec3 down;
	glm::vec3 right;
	glm::vec3 left;

	glm::mat4 viewMatrix;
	Entity<Transform> transform;
};

DEFINE_ENTITY(Camera)