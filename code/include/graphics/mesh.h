#pragma once

class IBaseMesh
{
protected:
	virtual ~IBaseMesh() = default;
public:
	/**
	 * @brief Is this a dynamic mesh?
	 * @return True if dynamic.
	 */
	virtual bool IsDynamic() const = 0;

	/**
	 * @brief Is this a static mesh?
	 * @return True if static.
	 */
	virtual bool IsStatic() const = 0;
};

/**
 * @brief A mesh with modifiable vertex and index buffers.
 */
class IDynamicMesh : public IBaseMesh
{
protected:
	virtual ~IDynamicMesh() = default;

public:
	bool IsDynamic() const override { return true; }
	bool IsStatic() const override { return false; }
};

/**
 * @brief A mesh with vertex and index buffers that don't change.
 */
class IStaticMesh : public IBaseMesh
{
protected:
	virtual ~IStaticMesh() = default;

public:
	bool IsDynamic() const override { return false; }
	bool IsStatic() const override { return true; }
};
