#pragma once

#include <functional>
#include <cstdint>

#include "imgui.h"

class IWindow;

class IWindowResize
{
public:
	virtual ~IWindowResize() = default;

	/**
	 * @brief Callback for window resize events.
	 * @param window The window that was resized.
	 * @param width The new width of the window.
	 * @param height The new height of the window.
	 */
	virtual void OnWindowResize(IWindow *window, uint32_t width, uint32_t height) = 0;
};

/**
 * @class IWindow
 * @author Arthurdead
 * @date 04/04/20
 * @file graphics.h
 * @brief Window interface
 */
class IWindow
{
protected:
	/**
	 * @brief Window interface destructor do not call this use IGraphics::DeleteWindow instead.
	 */
	virtual ~IWindow() = default;
	
public:

	/**
	 * @brief Registers a function to be called when the window closes.
	 * @param function The function to be called.
	 */
	virtual void RegisterOnClosed(std::function<void(IWindow *)> function) = 0;

	/**
	 * @brief Registers an IWindowResize interface as a callback for resize events.
	 * @param windowResize A pointer to the IWindowResize interface.
	 */
	virtual void RegisterWindowResize(IWindowResize *windowResize) = 0;

	/**
	 * @brief Unregisters an IWindowResize interface.
	 * @param windowResize The IWindowResize interface to unregister.
	 */
	virtual void UnregisterWindowResize(IWindowResize *windowResize) = 0;

	/**
	 * @brief Gets the width of the window.
	 * @return The width of the window.
	 */
	virtual uint32_t GetWidth() const = 0;

	/**
	 * @brief Gets the height of the window.
	 * @return The height of the window.
	 */
	virtual uint32_t GetHeight() const = 0;

	/**
	 * @brief Gets the width and height of the window.
	 * @param width The variable to store width into.
	 * @param height The variable to store height into.
	 */
	virtual void GetDimensions(uint32_t &width, uint32_t &height) const = 0;

	/**
	 * @brief Gets the ImGuiContext associated with this window.
	 * @return The ImGuiContext.
	 */
	virtual ImGuiContext *GetGuiContext() = 0;

	/**
	 * @brief Updates display size/scale and delta timefor window's ImGuiContext.
	 * @param dt Delta time in seconds.
	 * @note Does NOT call ImGui::NewFrame.
	 */
	virtual void NewGuiFrame(float dt) = 0;
};
