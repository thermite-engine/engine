#pragma once

#include <cstdint>

class ITexture
{
public:
	virtual ~ITexture() = default;

	virtual uint32_t GetWidth() const = 0;
	virtual uint32_t GetHeight() const = 0;
	virtual bool IsOpaque() const = 0;
};
