#pragma once

#include "camera.h"
#include <glm/gtx/transform.hpp>

class CView3D
{
public:

	CView3D() = default;

	/**
	 * @brief CView3D
	 * @param x View offset in the X direction in screen coordinates.
	 * @param y View offset in the Y direction in screen coordinates.
	 * @param width The width of the view.
	 * @param height The height of the view.
	 * @param zNear Near clipping plane distance.
	 * @param zFar Far clipping plane distance.
	 * @param fieldOfView The horizontal FOV in degrees.
	 * @param camera A pointer to a camera.
	 */
	CView3D(int32_t x, int32_t y, uint32_t width, uint32_t height, float zNear, float zFar, float fieldOfView, Entity<Camera> camera)
	{
		Set(x, y, width, height, zNear, zFar, fieldOfView);
		SetCamera(camera);
		ComputeProjection();
	}

	/**
	 * @brief CView3D
	 * @param x View offset in the X direction in screen coordinates.
	 * @param y View offset in the Y direction in screen coordinates.
	 * @param width The width of the view.
	 * @param height The height of the view.
	 * @param zNear Near clipping plane distance.
	 * @param zFar Far clipping plane distance.
	 * @param fieldOfView The horizontal FOV in degrees.
	 */
	CView3D(int32_t x, int32_t y, uint32_t width, uint32_t height, float zNear, float zFar, float fieldOfView)
	{
		Set(x, y, width, height, zNear, zFar, fieldOfView);
		ComputeProjection();
	}

	/**
	 * @brief Sets the view and computes its perspective projection.
	 * @param x View offset in the X direction in screen coordinates.
	 * @param y View offset in the Y direction in screen coordinates.
	 * @param width The width of the view.
	 * @param height The height of the view.
	 * @param zNear Near clipping plane distance.
	 * @param zFar Far clipping plane distance.
	 * @param fieldOfView The horizontal FOV in degrees.
	 */
	void Set(int32_t x, int32_t y, uint32_t width, uint32_t height, float zNear, float zFar, float fieldOfView)
	{
		this->x = x;
		this->y = y;
		this->width = width;
		this->height = height;
		this->zNear = zNear;
		this->zFar = zFar;
		this->fieldOfView = fieldOfView;

		ComputeProjection();
	}

	void SetCamera(Entity<Camera> camera)
	{
		this->camera = camera;
	}

	void SetFOV(float fieldOfView)
	{
		if (fieldOfView != this->fieldOfView)
		{
			this->fieldOfView = fieldOfView;
			ComputeProjection();
		}
	}

	const glm::mat4 &GetProjection() const { return projectionMatrix; }

	/**
	 * @brief Computes the perspective projection based on view settings.
	 * @note You should call this if modifying the view without calling the Set() function
	 */
	void ComputeProjection()
	{
		const float hAspect = (float)width / (float)height;
		const float vAspect = (float)height / (float)width;

		float V = 2.0f * atanf(tanf(glm::radians(fieldOfView) / 2.0f) * vAspect);

		projectionMatrix = glm::perspective(V, hAspect, zNear, zFar);
	}

	int32_t x;
	int32_t y;
	uint32_t width;
	uint32_t height;

	float zNear;
	float zFar;
	float fieldOfView;

	Entity<Camera> camera;

private:
	glm::mat4 projectionMatrix;
};
