#pragma once

#include "view.h"
#include "camera.h"
#include "model.h"
#include "texture.h"
#include "texturecache.h"
#include "modelcache.h"
#include "components/transform.h"
#include "light.h"
#include "sprite.h"

#include <filesystem>

/**
 * @class IScene
 * @author Spirrwell
 * @date 07/16/20
 * @file scene.h
 * @brief Scene interface
 */
class IScene
{
protected:
	virtual ~IScene() = default;

public:

	/**
	 * @brief Gets a pointer to the light state manager.
	 * @return A pointer to the light state manager.
	*/
	virtual ILightState *GetLightState() = 0;

	/**
	 * @brief Sets the scene's current skybox texture.
	 * @param texture A pointer to a skybox texture or nullptr for no skybox.
	 */
	virtual void SetSkyboxTexture(ITexture *texture) = 0;

	/**
	 * @brief Gets pointer to texture cache.
	 * @return A pointer to the texture cache.
	 */
	virtual ITextureCache *GetTextureCache() = 0;

	/**
	 * @brief Gets pointer to model cache.
	 * @return A pointer to the model cache.
	 */
	virtual IModelCache *GetModelCache() = 0;

	/**
	 * @brief Loads a billboard sprite from the given path.
	 * @param spritePath A path to the billboard sprite
	 * @note Billboard sprite meshes have no cache, so this will create a new one each time.
	 * @note The texture data itself is cached, so don't worry.
	 * @return A pointer to the billboard sprite.
	*/
	virtual IBillboardSprite *LoadBillboardSprite(const std::filesystem::path &spritePath) = 0;

	/**
	 * @brief Adds a model to the draw list with a particular transform.
	 * @param model A pointer to the model to draw.
	 * @param transform A transformation that represents where to draw the model.
	 */
	virtual void DrawModel(IModel *model, const Transform &transform) = 0;

	/**
	 * @brief Adds a billboard sprite to the draw list with a particular transform.
	 * @param sprite A pointer to the sprite to draw.
	 * @param transform A transformation that represents where to draw the sprite.
	*/
	virtual void DrawBillboardSprite(IBillboardSprite *sprite, const Transform &transform) = 0;

	/**
	 * @brief Clears any draw commands in our draw list.
	 */
	virtual void ClearDrawLists() = 0;
};