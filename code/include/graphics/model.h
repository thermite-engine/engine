#pragma once

class IModel
{
protected:
	virtual ~IModel() = default;
};
