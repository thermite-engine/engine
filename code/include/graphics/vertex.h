#pragma once

#include <cstdint>
#include <glm/glm.hpp>

struct BillboardVertex
{
	glm::vec3 position;
	glm::vec2 uv;
	glm::vec4 color = {1.0f, 1.0f, 1.0f, 1.0f};
};

// Basically mirror of ImDrawVtx
struct UIVertex
{
	glm::vec2 position;
	glm::vec2 uv;
	glm::u32 color;
};

struct SkyVertex
{
	glm::vec3 position;
	glm::vec4 color = {1.0f, 1.0f, 1.0f, 1.0f};
};

struct Vertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 uv;
	glm::vec4 color;
	glm::uvec4 boneindex;
	glm::vec4 boneweight; // 8th attribute
};