#pragma once

#include <filesystem>
#include "imgui.h"

class IImGuiContextSetter
{
public:
	virtual void SetCurrentContext(ImGuiContext *ctx) = 0;
};

class IImGuiService
{
protected:
	virtual ~IImGuiService() = default;

public:

	/**
	 * @brief Registers a setter instance to call when IImGuiService::SetCurrentContext is called.
	 * @param setter A pointer to the setter to register.
	 */
	virtual void RegisterContextSetter(IImGuiContextSetter *setter) = 0;

	/**
	 * @brief Unregisters a setter instance.
	 * @param setter A pointer to the setter to register.
	 */
	virtual void UnregisterContextSetter(IImGuiContextSetter *setter) = 0;

	/**
	 * @brief Calls CImGuiContextSetter::SetCurrentContext for every registered CImGuiContextSetter
	 * @param ctx A pointer to the context to set.
	 * @note This is a major fucking hack to set the context across DLL/SO boundaries because ImGui stores its current context in a global pointer.
	 * @note Don't call this directly, use the helper functions in the imgui_static library.
	 */
	virtual void SetCurrentContext(ImGuiContext *ctx) = 0;
};