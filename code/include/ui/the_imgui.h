#pragma once

#include "ui/ui.h"
#include "service.h"

namespace ThermiteImGui
{
	extern void Init(CServer *server);
	extern void Shutdown();
	extern void SetCurrentContext(ImGuiContext *ctx);
}