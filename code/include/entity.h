#pragma once

#include "log/log.h"

#include <cstdint>
#include <cassert>
#include <cstddef>
#include <type_traits>
#include <optional>
#include <memory>
#include <iterator>
#include <functional>
#include <array>
#include <typeindex>
#include <set>
#include <unordered_set>
#include <utility>

class CEntityManager;

using PoolHandle = uint64_t;

template <typename T>
class CEntityPool
{
	T *elements = nullptr;
	PoolHandle *erased = nullptr;
	bool *alive = nullptr;

	std::size_t erasedCount = 0;

	std::size_t elementCapacity = 0;
	std::size_t erasedCapacity = 0;

	template <typename U>
	U *Allocate(std::size_t count) { return static_cast<U*>(::operator new(sizeof(U) * count)); }

	template <typename U>
	void Deallocate(U *ptr) { ::operator delete(ptr); }

public:

	void PopBackErased()
	{
		erasedCount--;
	}

	T &Element(PoolHandle poolHandle) { return elements[poolHandle]; }

	void EachPoolHandle(std::function<void(PoolHandle)> callbackFn)
	{
		if (callbackFn)
		{
			for (std::size_t i = 0; i < elementCapacity; ++i)
			{
				if (!IsAlive(i))
					continue;
				
				callbackFn(i);
			}
		}
	}

	void Each(std::function<void(T&)> callbackFn)
	{
		if (callbackFn)
		{
			for (std::size_t i = 0; i < elementCapacity; ++i)
			{
				if (!IsAlive(i))
					continue;
				
				callbackFn(elements[i]);
			}
		}
	}

	bool IsAlive(PoolHandle cookie) const
	{
		if (elementCapacity == 0)
			return false;
		
		return alive[cookie];
	}

	PoolHandle ErasedBack()
	{
		return *(erased + erasedCount - 1);
	}

	void MarkDead(PoolHandle cookie)
	{
		if (erasedCapacity < erasedCount + 1)
			GrowErased(erasedCount + 1);
		
		alive[cookie] = false;
		erased[erasedCount] = cookie;
		++erasedCount;
	}

	void GrowElements(std::size_t newCapacity)
	{
		T *tmpElements = Allocate<T>(newCapacity);
		bool *tmpAlive = Allocate<bool>(newCapacity);

		for (std::size_t i = 0; i < elementCapacity; ++i)
		{
			tmpAlive[i] = alive[i];
			if (tmpAlive[i])
			{
				new (&tmpElements[i]) T(std::move(elements[i]));
				elements[i].~T();
			}
		}

		Deallocate(alive);
		alive = tmpAlive;

		for (std::size_t i = elementCapacity; i < newCapacity; ++i)
			MarkDead(i);

		Deallocate(elements);
		elements = tmpElements;
		elementCapacity = newCapacity;
	}

	void GrowErased(std::size_t newCapacity)
	{
		PoolHandle *tmpErased = Allocate<PoolHandle>(newCapacity);
		for (std::size_t i = 0; i < erasedCount; ++i)
			tmpErased[i] = erased[i];
		
		Deallocate(erased);
		erased = tmpErased;
		erasedCapacity = newCapacity;
	}

	PoolHandle Create()
	{
		if (erasedCount > 0)
		{
			const std::size_t index = ErasedBack();
			PopBackErased();

			new (&elements[index]) T;
			alive[index] = true;
			return index;
		}

		const PoolHandle index = elementCapacity;
		GrowElements(elementCapacity + 1);

		new (&elements[index]) T;
		alive[index] = true;

		return index;
	}

	void CreateFromCookie(PoolHandle cookie)
	{
		if (cookie >= elementCapacity)
		{
			GrowElements(cookie + 1);
			new (&elements[cookie]) T;
		}
		else
		{
			if (IsAlive(cookie))
				Log::FatalError("Trying to overwrite existing entity");
			
			new (&elements[cookie]) T;
		}
	}

	void Destroy(PoolHandle cookie)
	{
#ifdef _DEBUG
		if (!IsAlive(cookie))
			Log::FatalError("Trying to destroy already dead entity");
#endif

		elements[cookie].~T();

		if (erasedCapacity < erasedCount + 1)
			GrowErased(erasedCount + 1);
		
		erased[erasedCount] = cookie;
		++erasedCount;
	}
};

using EntityMetaIndex = uint64_t;

struct EntityHandle
{
	EntityMetaIndex metaIndex;
	PoolHandle poolHandle;
};

static constexpr EntityHandle NULL_ENTITY_HANDLE = {
	.metaIndex = (std::numeric_limits<EntityMetaIndex>::max)(),
	.poolHandle = (std::numeric_limits<PoolHandle>::max)(),
};

namespace EntityRegistrar
{
	template <typename T>
	struct EntityName
	{
		static constexpr std::string_view value = {};
	};
}

template <typename T>
struct Entity
{
	EntityHandle handle = NULL_ENTITY_HANDLE;
	CEntityManager *entityManager = nullptr;

	inline T *operator->();
	inline const T* operator->() const;

	inline T &operator*();
	inline const T &operator*() const;
};

template <typename T>
static constexpr Entity<T> NULL_ENTITY = {
	.handle = NULL_ENTITY_HANDLE,
	.entityManager = nullptr
};

struct EntityCreateInfo
{
	std::string typeName;
};

namespace Events
{
	template <typename T>
	struct EntityCreated
	{
		Entity<T> entity;
	};

	template <typename T>
	struct EntityRemoved
	{
		Entity<T> entity;
	};

	struct AnyEntityCreated
	{
		EntityHandle entity;
	};

	struct AnyEntityRemoved
	{
		EntityHandle entity;
	};
}

template <typename T>
struct EventReceiver
{
	virtual ~EventReceiver() = default;
	virtual void OnEvent(CEntityManager *entityManager, const T &event) = 0;
};

class CEntityManager :
	public EventReceiver<Events::AnyEntityCreated>,
	public EventReceiver<Events::AnyEntityRemoved>
{
public:

	CEntityManager()
	{
		RegisterReceiver<Events::AnyEntityCreated>(this);
		RegisterReceiver<Events::AnyEntityRemoved>(this);

		for (EntityTypeInfo &typeInfo : registeredTypes)
		{
			EntityStorageInfo &storageInfo = entityStorageMap[typeInfo.className];
			storageInfo.entityPool = typeInfo.entityPoolCreateFn();
		}
	}

	~CEntityManager()
	{
		std::vector<EntityHandle> deleteQueue;

		for (EntityTypeInfo &typeInfo : registeredTypes)
		{
			EntityStorageInfo &storageInfo = entityStorageMap.at(typeInfo.className);
			typeInfo.eachPoolHandleFn([this, &typeInfo, &deleteQueue](PoolHandle poolHandle) {
				EntityHandle handle = {
					.metaIndex = AssignMetaIndex(typeInfo.className),
					.poolHandle = poolHandle
				};

				deleteQueue.emplace_back(handle);
			}, storageInfo.entityPool);

			for (EntityHandle &handle : deleteQueue)
				DestroyEntity(handle);
			
			deleteQueue.clear();
			typeInfo.entityPoolDeleteFn(storageInfo.entityPool);
		}

		UnregisterReceiver<Events::AnyEntityCreated>(this);
		UnregisterReceiver<Events::AnyEntityRemoved>(this);
	}

	template <typename T>
	void RegisterReceiver(EventReceiver<T> *receiver)
	{
		eventReceiverMap[std::type_index(typeid(EventReceiver<T>))].insert(receiver);
	}

	template <typename T>
	void UnregisterReceiver(EventReceiver<T> *receiver)
	{
		eventReceiverMap[std::type_index(typeid(EventReceiver<T>))].erase(receiver);
	}

	template <typename T>
	void FireEvent(const T &event)
	{
		std::unordered_set<void*> receivers = eventReceiverMap[std::type_index(typeid(EventReceiver<T>))];
		for (auto receiverInterface : receivers)
		{
			EventReceiver<T> *receiver = reinterpret_cast<EventReceiver<T>*>(receiverInterface);
			receiver->OnEvent(this, event);
		}
	}

	void OnEvent(CEntityManager *entityManager, const Events::AnyEntityCreated &event) override
	{
		Log::Print("Entity {} created!\n", entityMetaData[event.entity.metaIndex].className);
	}

	void OnEvent(CEntityManager *entityManager, const Events::AnyEntityRemoved &event) override
	{
		Log::Print("Entity {} removed!\n", entityMetaData[event.entity.metaIndex].className);
	}

	EntityMetaIndex AssignMetaIndex(std::string_view name)
	{
		if (auto it = entityMetaLookup.find(name); it != entityMetaLookup.end())
			return it->second;

		const EntityMetaIndex index = entityMetaData.size();
		EntityMetaData &metaData = entityMetaData.emplace_back();

		metaData.className = name;
		entityMetaLookup[name] = index;

		return index;
	}

	template <typename T> requires (!EntityRegistrar::EntityName<T>::value.empty())
	Entity<T> CreateEntity()
	{
		constexpr std::string_view name = EntityRegistrar::EntityName<T>::value;
		EntityStorageInfo &storageInfo = entityStorageMap.at(name);
		EntityTypeInfo &typeInfo = registeredTypes.at(registeredTypeIndexMap.at(std::type_index(typeid(T))));

		const EntityMetaIndex index = AssignMetaIndex(name);

		Entity<T> entity = {
			.handle = {
				.metaIndex = index,
				.poolHandle = typeInfo.createEntityFn(storageInfo.entityPool)
			},
			.entityManager = this
		};

		typeInfo.onEntityCreated(entity.handle.poolHandle, this);

		return entity;
	}

	EntityHandle CreateEntity(std::string_view name)
	{
		EntityStorageInfo &storageInfo = entityStorageMap.at(name);
		EntityTypeInfo &typeInfo = registeredTypes.at(registeredTypeStringMap.at(name));

		const EntityMetaIndex index = AssignMetaIndex(name);

		EntityHandle handle = {
			.metaIndex = index,
			.poolHandle = typeInfo.createEntityFn(storageInfo.entityPool)
		};

		typeInfo.onEntityCreated(handle.poolHandle, this);

		return handle;
	}

	template <typename T>
	void DestroyEntity(Entity<T> entity)
	{
		constexpr std::string_view name = EntityRegistrar::EntityName<T>::value;

		EntityStorageInfo &storageInfo = entityStorageMap.at(name);
		EntityTypeInfo &typeInfo = registeredTypes.at(registeredTypeIndexMap.at(std::type_index(typeid(T))));

		typeInfo.onEntityRemoved(entity.handle.poolHandle, this);
		typeInfo.destroyEntityFn(entity.handle.poolHandle, storageInfo.entityPool);
	}

	void DestroyEntity(EntityHandle handle)
	{
		EntityMetaData &metaData = entityMetaData[handle.metaIndex];
		EntityStorageInfo &storageInfo = entityStorageMap.at(metaData.className);
		EntityTypeInfo &typeInfo = registeredTypes.at(registeredTypeStringMap.at(metaData.className));

		typeInfo.onEntityRemoved(handle.poolHandle, this);
		typeInfo.destroyEntityFn(handle.poolHandle, storageInfo.entityPool);
	}

	template <typename T> requires (!EntityRegistrar::EntityName<T>::value.empty())
	void Each(std::function<void(T&)> callbackFn)
	{
		if (callbackFn)
		{
			CEntityPool<T> &entityPool = *(reinterpret_cast<CEntityPool<T>*>(entityStorageMap.at(EntityRegistrar::EntityName<T>::value).entityPool));
			entityPool.Each(callbackFn);
		}
	}

	template <typename T> requires (!EntityRegistrar::EntityName<T>::value.empty())
	T &Get(EntityHandle entity)
	{
#ifdef _DEBUG
		if (entityMetaData[entity.metaIndex].className != EntityRegistrar::EntityName<T>::value)
			Log::FatalError("Trying to access entity of wrong type!");
#endif
		CEntityPool<T> &slotMap = *(reinterpret_cast<CEntityPool<T>*>(entityStorageMap.at(EntityRegistrar::EntityName<T>::value).entityPool));
		return slotMap.Element(entity.poolHandle);
	}

	template <typename T> requires (!EntityRegistrar::EntityName<T>::value.empty())
	static void RegisterEntity()
	{
		const std::size_t index = registeredTypes.size();

		EntityTypeInfo &typeInfo = registeredTypes.emplace_back();
		typeInfo.className = EntityRegistrar::EntityName<T>::value;

		typeInfo.entityPoolCreateFn = []() -> void* {
			return new CEntityPool<T>;
		};

		typeInfo.entityPoolDeleteFn = [](void *entityPool) {
			delete reinterpret_cast<CEntityPool<T>*>(entityPool);
		};

		typeInfo.createEntityFn = [](void *entityPool) -> PoolHandle {
			CEntityPool<T> &realEntityPool = *(reinterpret_cast<CEntityPool<T>*>(entityPool));
			return realEntityPool.Create();
		};

		typeInfo.destroyEntityFn = [](PoolHandle poolHandle, void *entityPool) {
			CEntityPool<T> &realEntityPool = *(reinterpret_cast<CEntityPool<T>*>(entityPool));
			realEntityPool.Destroy(poolHandle);
		};

		typeInfo.eachPoolHandleFn = [](std::function<void(PoolHandle)> callbackFn, void *entityPool) {
			if (callbackFn)
			{
				CEntityPool<T> &realEntityPool = *(reinterpret_cast<CEntityPool<T>*>(entityPool));
				realEntityPool.EachPoolHandle(callbackFn);
			}
		};

		typeInfo.onEntityCreated = [](PoolHandle poolHandle, CEntityManager *entityManager) {
			EntityHandle handle = {
				.metaIndex = entityManager->AssignMetaIndex(EntityRegistrar::EntityName<T>::value),
				.poolHandle = poolHandle
			};

			Entity<T> entity = {
				.handle = handle,
				.entityManager = entityManager
			};

			entityManager->FireEvent(Events::AnyEntityCreated{.entity = handle});
			entityManager->FireEvent(Events::EntityCreated<T>{.entity = entity});
		};

		typeInfo.onEntityRemoved = [](PoolHandle poolHandle, CEntityManager *entityManager) {
			EntityHandle handle = {
				.metaIndex = entityManager->AssignMetaIndex(EntityRegistrar::EntityName<T>::value),
				.poolHandle = poolHandle
			};

			Entity<T> entity = {
				.handle = handle,
				.entityManager = entityManager
			};

			entityManager->FireEvent(Events::AnyEntityRemoved{.entity = handle});
			entityManager->FireEvent(Events::EntityRemoved<T>{.entity = entity});
		};


		registeredTypeIndexMap[std::type_index(typeid(T))] = index;
		registeredTypeStringMap[EntityRegistrar::EntityName<T>::value] = index;
	}

	struct EntityTypeInfo
	{
		std::string_view className;

		std::function<void*()> entityPoolCreateFn;
		//std::function<void*(const SlotMapCreateInfo&)> slotMapCreateCopyFn;
		std::function<void(void*)> entityPoolDeleteFn;

		std::function<PoolHandle(void*)> createEntityFn;
		std::function<void(PoolHandle, void*)> destroyEntityFn;

		std::function<void(std::function<void(PoolHandle)>, void*)> eachPoolHandleFn;

		std::function<void(PoolHandle, CEntityManager*)> onEntityCreated;
		std::function<void(PoolHandle, CEntityManager*)> onEntityRemoved;
	};


	struct EntityMetaData
	{
		std::string_view className;
	};

	struct EntityStorageInfo
	{
		void *entityPool = nullptr;
	};
	
	std::vector<EntityMetaData> entityMetaData;
	std::unordered_map<std::string_view, EntityMetaIndex> entityMetaLookup;

	std::unordered_map<std::string_view, EntityStorageInfo> entityStorageMap;

	inline static std::vector<EntityTypeInfo> registeredTypes;
	inline static std::unordered_map<std::type_index, std::size_t> registeredTypeIndexMap;
	inline static std::unordered_map<std::string_view, std::size_t> registeredTypeStringMap;

private:
	std::unordered_map<std::type_index, std::unordered_set<void*>> eventReceiverMap;
};

template <typename T>
inline T *Entity<T>::operator->() { return &entityManager->Get<T>(handle); }

template <typename T>
inline const T *Entity<T>::operator->() const { return &entityManager->Get<T>(handle); }

template <typename T>
inline T &Entity<T>::operator*() { return entityManager->Get<T>(handle); }

template <typename T>
inline const T &Entity<T>::operator*() const { return entityManager->Get<T>(handle); }

namespace EntityRegistrar
{
	template <typename T> requires (!EntityRegistrar::EntityName<T>::value.empty())
	struct EntityRegister
	{
		EntityRegister() { CEntityManager::RegisterEntity<T>(); }
	};
}

#define DEFINE_ENTITY(className) namespace EntityRegistrar \
{ \
template<> \
struct EntityName<className> \
{ \
	static constexpr std::string_view value = #className; \
	inline static EntityRegistrar::EntityRegister<className> s_EntityRegister##className; \
}; \
}

DEFINE_ENTITY(EntityHandle)

// TODO: Move network stuff

enum struct ENetworkManagerMode
{
	Server,
	Client
};

class CNetworkManager : public EventReceiver<Events::AnyEntityCreated>, public EventReceiver<Events::AnyEntityRemoved>
{
public:
	CNetworkManager(ENetworkManagerMode mode) :
		managerMode(mode)
	{
	}

	void OnEvent(CEntityManager *entityManager, const Events::AnyEntityCreated &event) override
	{
		auto [entity] = event;
		std::string_view name = entityManager->entityMetaData[entity.metaIndex].className;

		if (!networkedEntitySet.contains(name))
			return;
		
		std::unordered_map<PoolHandle, void*> &networkDataMap = entityNetworkDataMap[name];
		networkDataMap[entity.poolHandle] = nullptr; // TODO: Create network data
	}

	void OnEvent(CEntityManager *entityManager, const Events::AnyEntityRemoved &event) override
	{
		auto [entity] = event;
		std::string_view name = entityManager->entityMetaData[entity.metaIndex].className;

		if (!networkedEntitySet.contains(name))
			return;
		
		std::unordered_map<PoolHandle, void*> &networkDataMap = entityNetworkDataMap[name];
		networkDataMap[entity.poolHandle] = nullptr; // TODO: Delete network data
	}

	ENetworkManagerMode managerMode;
	
	inline static std::unordered_set<std::string_view> networkedEntitySet;
	std::unordered_map<std::string_view, std::unordered_map<PoolHandle, void*>> entityNetworkDataMap;
};


struct NetworkProperty
{
	std::size_t offset;
	std::size_t size;
};

template <typename T>
struct NetworkTable
{
	static constexpr bool exists = false;
	static consteval std::size_t DataSize() { return 0; }
};

#define BEGIN_NETWORKTABLE(Type) template<> \
struct NetworkTable<Type> \
{ \
	using NetworkType = Type; \
	static constexpr bool exists = true; \
	static consteval auto Table() \
	{ \
		constexpr NetworkProperty props[] = {

#define END_NETWORKTABLE() \
		}; \
		return std::to_array(props); \
	} \
	static consteval std::size_t DataSize() \
	{ \
		constexpr auto arr = Table(); \
		std::size_t dataSize = 0; \
		for (auto &prop : arr) \
		{ \
			dataSize += prop.size; \
		} \
		return dataSize; \
	} \
};

#define NETWORK_PROPERTY_TRIVIAL(var) \
	{ .offset = offsetof(NetworkType, var), .size = sizeof(decltype(NetworkType::var)) }

#define NETWORK_PROPERTY_ENTITY(var) \
	{ .offset = offsetof(NetworkType, var.id), .size = sizeof(EntityHandle) }
// GCC complains without a newline here :D