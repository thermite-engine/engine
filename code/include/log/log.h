#pragma once

#include "fmt/format.h"
#include "fmt/color.h"
#include "logger/logger.h"
#include "service.h"

#include <SDL2/SDL.h>

namespace Log
{
	namespace Internal
	{
		/**
		 * @brief The default log channel for this module to print to
		 */
		extern ILogChannel *logChannel;
	}

	/**
	 * @brief Initializes the default log channel for this module.
	 * @param logger Pointer to the logger service.
	 * @param name The name you want to be logged when printing from this module
	 */
	extern void Init(ILogger *logger, const std::string_view &name);

	/**
	 * @brief Initializes the default log channel for this module.
	 * @param logger Pointer to the logger service.
	 * @param name The name you want to be logged when printing from this module.
	 * @param defaultColor The default color you want the log to output for this module.
	 */
	extern void Init(ILogger *logger, const std::string_view &name, fmt::color defaultColor);

	/**
	 * @brief Initializes the default log channel for this module.
	 * @param logger Pointer to the logger service.
	 * @param name The name you want to be logged when printing from this module.
	 * @param outputFile The log file you want to output to when printing from this module.
	 */
	extern void Init(ILogger *logger, const std::string_view &name, const std::filesystem::path &outputFile);

	/**
	 * @brief Initializes the default log channel for this module.
	 * @param logger Pointer to the logger service.
	 * @param name The name you want to be logged when printing from this module.
	 * @param outputFile The log file you want to output to when printing from this module.
	 * @param defaultColor The default color you want the log to output for this module.
	 */
	extern void Init(ILogger *logger, const std::string_view &name, const std::filesystem::path &outputFile, fmt::color defaultColor);

	/**
	 * @brief Print to specific log channel.
	 * @param logChannel The log channel to print to.
	 * @param severity Severity level.
	 * @param message Message to print.
	 * @param args Optional arguments passed to fmtlib to format the message.
	 */
	template<typename ... Args>
	void Print(ILogChannel *logChannel, ESeverity severity, const std::string_view &message, Args &&... args)
	{
		if constexpr (sizeof...(Args) != 0)
		{
			auto formattedMsg = fmt::format(fmt::runtime(message), std::forward<Args>(args)...);
			logChannel->Print(severity, formattedMsg);
		}
		else
		{
			logChannel->Print(severity, message);
		}
	}

	/**
	 * @brief Print to specific log channel with default severity ESeverity::Info.
	 * @param logChannel The log channel to print to.
	 * @param message Message to print.
	 * @param args Optional arguments passed to fmtlib to format the message.
	 */
	template<typename ... Args>
	void Print(ILogChannel *logChannel, const std::string_view &message, Args &&... args)
	{
		return Print(logChannel, ESeverity::Info, message, args...);
	}

	/**
	 * @brief Print to default log channel.
	 * @param severity Severity level.
	 * @param message Message to print.
	 * @param args Optional arguments passed to fmtlib to format the message.
	 */
	template<typename ... Args>
	void Print(ESeverity severity, const std::string_view &message, Args &&... args)
	{
		Print(Internal::logChannel, severity, message, std::forward<Args>(args)...);
	}

	/**
	 * @brief Print to default log channel with default severity ESeverity::Info.
	 * @param message Message to print.
	 * @param args Optional arguments passed to fmtlib to format the message.
	 */
	template<typename ... Args>
	void Print(const std::string_view &message, Args &&... args)
	{
		Print(ESeverity::Info, message, std::forward<Args>(args)...);
	}

	template <typename ... Args>
	[[noreturn]] void FatalError(const std::string_view &message, Args &&... args)
	{
		// NOTE: Abort can be used to generate crash dumps
		// We call std::abort() for each branch here so we can maybe get the formattedMsg in the debugger

		if constexpr (sizeof...(Args) != 0)
		{
			const auto formattedMsg = fmt::format(message, std::forward<Args>(args)...);

			if (Internal::logChannel)
				Internal::logChannel->Print(ESeverity::Error, formattedMsg);
			else
				fmt::print(stderr, formattedMsg);

			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Fatal Error", formattedMsg.c_str(), nullptr);

			std::abort();
		}
		else
		{
			if (Internal::logChannel)
				Internal::logChannel->Print(ESeverity::Error, message);
			else
				fmt::print(stderr, fmt::runtime(message));

			SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Fatal Error", message.data(), nullptr);

			std::abort();
		}
	}
}