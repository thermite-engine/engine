#pragma once

#if defined(_WIN32) || defined(_WIN64)
	#define THR_WINDOWS
	#define THR_DLL_EXT ".dll"
#elif defined(__APPLE__)
	#define THR_APPLE
	#define THR_DLL_EXT ".dylib"
#elif defined(__linux__)
	#define THR_LINUX
	#define THR_DLL_EXT ".so"
#elif defined(__ANDROID__)
	#define THR_ANDROID
	#define THR_DLL_EXT ".so"
#endif

#if defined (_MSC_VER)
	#define THR_EXPORT __declspec( dllexport )
	#define THR_IMPORT __declspec( dllimport )
#elif defined (__GNUC__)
	#define THR_EXPORT __attribute__( ( visibility ( "default" ) ) )
	#define THR_IMPORT
#endif