#pragma once

#include <string>

/**
 * @class IResourceDelegate
 * @author swissChili
 * @date 01/04/20
 * @file resourcedelegate.h
 * @purpose Abstract resource delegate class. Fetches a resource from it's path.
 * @note Services extending this class should set their id to ".<extension>" where extension
 *       is the extension they can load.
 */
template <typename T>
class IResourceDelegate
{
public:
	virtual T LoadResource(const std::string &resourceName) = 0;
};
