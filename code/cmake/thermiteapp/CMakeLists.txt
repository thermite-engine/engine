
set( CODE ${CMAKE_CURRENT_LIST_DIR}/../.. )

set( THERMITEAPP_SRC_FILES
	${CODE}/src/thermiteapp/thermiteapp.cpp
)

set( THERMITEAPP_INCLUDE_FILES
	${CODE}/include/thermiteapp/thermiteapp.h
)

add_library( ThermiteApp STATIC ${THERMITEAPP_SRC_FILES} ${THERMITEAPP_INCLUDE_FILES} )
set_target_properties( ThermiteApp
	PROPERTIES
		CXX_STANDARD 20
		CXX_STANDARD_REQUIRED YES
		CXX_EXTENSIONS NO
)

