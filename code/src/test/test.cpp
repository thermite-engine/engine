#include "config.h"
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>


TEST_CASE("Test CConfig")
{
	CConfig config;

	SUBCASE("string values")
	{
		config.MakeObject();
		config["somekey"] = "value";
		config["aaaa"] = "bbbb";

		CHECK(config["somekey"].String());
		CHECK(*config["aaaa"].String() == "bbbb");
	}
}

TEST_CASE("Test KeyValues object")
{
	CConfig conf = ParseKeyValues(R"(
		key value
		aaa bbb
		no yes)");

	CHECK(conf["key"].String());
	CHECK(*conf["aaa"].String() == "bbb");
}

TEST_CASE("Test KeyValues object")
{
	CConfig conf = ParseKeyValues(R"(
		subobject
		{
			key value
		}
		)");

	CHECK(conf["subobject"].Object());
	auto str = *conf["subobject"]["key"].String();
	std::cout << "Value is '" << str << "'\n";
	CHECK(str == "value");
}

TEST_CASE("Test KeyValues list")
{
	CConfig conf = ParseKeyValues(R"(
		list
		[
			a
			{
				"object in list" "yep"
			}
			c
		]
		)");

	CHECK(conf["list"][0].String());
	CHECK(*conf["list"][2].String() == "c");
	CHECK(*conf["list"][1]["object in list"].String() == "yep");
}

TEST_CASE("Test nested KeyValues objects")
{
	CConfig conf = ParseKeyValues(R"(
		top
		{
			second
			{
				third "yes"
			}
		})");

	CHECK(*conf["top"]["second"]["third"].String() == "yes");
}
