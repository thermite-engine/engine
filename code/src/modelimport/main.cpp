#include "log.h"
#include "logger/logger.h"
#include "graphics/graphics.h"
#include "input/input.h"
#include "thermiteapp/thermiteapp.h"

class CModelImportService : public CService<>, public IWindowResize
{
public:
	CModelImportService(CServer *o) : CService(o)
	{
		Log::Init(Get<ILogger>(), "ModelImport", fmt::color::dark_turquoise);

		camera = entityManager.CreateEntity<Camera>();
		camera->transform = entityManager.CreateEntity<Transform>();

		view3D.camera = camera;

		window->RegisterOnClosed(std::bind(&CModelImportService::WindowClosed, this, std::placeholders::_1));
		window->RegisterWindowResize(this);
		scene->SetSkyboxTexture(scene->GetTextureCache()->LoadSkybox("textures/skybox/interstellar_", ".tga"));
		//renderer->SetClearColor(0.39f, 0.58f, 0.93f, 1.0f);
	}

	void OnWindowResize(IWindow*, uint32_t width, uint32_t height) override
	{
		view3D.Set(0, 0, width, height, zNear, zFar, FOV);
	}

	void WindowClosed(IWindow*)
	{
		KillAll();
	}

	void KillAll()
	{
		renderer->DeleteScene(scene);
		graphics->DeleteRenderer(renderer);
		graphics->DeleteWindow(window);
		scene = nullptr;
		renderer = nullptr;
		window = nullptr;

		killed = true;
		Halt();
	}

	void Update(uint64_t) override
	{
		if (shouldQuit)
			KillAll();

		if (killed)
			return;

		if (model)
		{
			if (input->IsMouseButtonPressed(SDL_BUTTON_LEFT))
			{
				mX += input->GetMouseDeltaX() * 0.5f;
				mY += input->GetMouseDeltaY() * 0.5f;
			}

			if (input->IsMouseButtonPressed(SDL_BUTTON_RIGHT))
				camDist += input->GetMouseDeltaY() * 0.02f;

			camDist -= ((float)input->GetMouseWheelY());
		}

		auto constrain = [](float num) -> float
		{
			num = std::fmod(num, 360.0f);
			return (num < 0.0f) ? num += 360.0f : num;
		};

		mX = constrain(mX);
		mY = std::clamp(mY, -80.0f, 80.0f);
		camDist = std::clamp(camDist, 0.5f, 50.0f);

		glm::quat xRot = glm::angleAxis(glm::radians(mY), glm::normalize(glm::vec3(1.0f, 0.0f, 0.0f)));
		glm::quat yRot = glm::angleAxis(glm::radians(mX), glm::normalize(glm::vec3(0.0f, 1.0f, 0.0f)));
		camera->transform->position = glm::vec3(0.0f, 0.0f, camDist);
		camera->transform->rotation = xRot * yRot;

		camera->viewMatrix = ToOrbitalCameraTransformation(*camera->transform);

		if (model)
		{
			Transform transform;
			transform.position = {0.0f, 0.0f, 0.0f};
			scene->DrawModel(model, transform);
		}

		renderer->DrawScene(scene, view3D, camera->viewMatrix);

		renderer->BeginFrame();
		renderer->DrawFrame();
		renderer->EndFrame();

		renderer->ClearDrawLists();
		scene->ClearDrawLists();
	}

	IGraphics *graphics = Get<IGraphics>();
	IInput *input = Get<IInput>();

	IWindow *window = graphics->CreateWindow("Thermite Engine", 1280, 720);
	IRenderer *renderer = graphics->CreateRenderer(window);

	IScene *scene = renderer->CreateScene();

	CEntityManager entityManager;
	Entity<Camera> camera;

	const float zNear = 0.01f;
	const float zFar = 10000.0f;
	const float FOV = 90.0f;

	float mX = 0.0f;
	float mY = 0.0f;

	float camDist = 5.0f;

	CView3D view3D{0, 0, window->GetWidth(), window->GetHeight(), zNear, zFar, FOV, NULL_ENTITY<Camera>};
	IModel *model = scene->GetModelCache()->LoadModel("models/cube.obj");

	bool killed = false;
	bool shouldQuit = false;
};

class CModelImport : public CThermiteApp
{
public:
	int AppMain(int, char *[]) override
	{
		LoadFromConfig("modelimport.toml");
		CModelImportService modelImportService(this);

		EventLoop();
		return 0;
	}
};

int main(int argc, char *argv[])
{
	CModelImport modelImport;
	return modelImport.AppMain(argc, argv);
}