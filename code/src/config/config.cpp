#include "config.h"
#include <cstring>
#include <iostream>

// just convenience types
typedef std::vector<CConfig> KVList;
typedef std::map<std::string, CConfig> KVObject;

CConfig::CConfig()
{}

CConfig::CConfig(std::string string)
{
	this->val = string;
}

void CConfig::MakeObject()
{
	val = KVObject();
}

void CConfig::MakeList()
{
	val = KVList();
}

void CConfig::MakeString()
{
	val = std::string();
}

std::optional<std::string> CConfig::String()
{
	if (auto str = std::get_if<std::string>(&val))
	{
		return *str;
	}
	else return std::nullopt;
}

std::optional<std::vector<CConfig>> CConfig::List()
{
	if (auto list = std::get_if<KVList>(&val))
	{
		return *list;
	}
	else return std::nullopt;
}

std::optional<std::map<std::string, CConfig>> CConfig::Object()
{
	if (auto obj = std::get_if<KVObject>(&val))
	{
		return *obj;
	}
	else return std::nullopt;
}

CConfig &CConfig::operator [](std::size_t index)
{
	if (List() && List()->size() > index)
	{
		return std::get<KVList>(val)[index];
	}
	throw std::runtime_error("Attempted to index a list. This is not a list.");
}

CConfig &CConfig::operator [](std::string key)
{
	if (Object())
	{
		return std::get<KVObject>(val)[key];
	}
	std::cerr << key << std::endl;
	throw std::runtime_error("Attempted to index an object. This is not an object.");
}

void CConfig::Append(CConfig item)
{
	if (List())
	{
		std::get<KVList>(val).push_back(item);
	}
}

void CConfig::operator =(std::string str)
{
	val = str;
}

bool IsSpace(char c)
{
	return	c == ' ' ||
			c == '\t' ||
			c == '\n' ||
			c == '\r' ||
			c == '\v'; // does anyone use vertical tabs??
}

bool IsReserved(char c)
{
	return	c == '{' ||
			c == '}' ||
			c == '[' ||
			c == ']' ||
			c == '#';// maybe for preprocessor directives later on?
}

/**
 * @author Ozxy
 */
void SkipSpace(const char *str, std::size_t &i)
{
	for (; str[i] && IsSpace(str[i]); i++); //skip until new character
	if (!strncmp(str + i, "//", 2))
	{
		for (; str[i] && str[i] != '\n'; i++); //skip until new line
		i++;
		SkipSpace(str, i); //keep skipping
	}
	else if (!strncmp(str + i, "/*", 2))
	{
		for (; str[i] && strncmp(str + i, "*/", 2); i++); //skip until end of comment block
		i += 2;
		SkipSpace(str, i); //keep skipping
	}
}

std::string ReadString(const char *str, size_t& i)
{
	size_t startPos = i;

	////std::cout << "reading string from " << i << "\n";

	if (str[i] == '"') //has quotes 
	{
		startPos++;
		i++;
		for (; str[i] && str[i] != '"'; i++);
		return std::string(str + startPos, i++ - startPos);
	}

	for (; str[i] && !IsSpace(str[i]) && !IsReserved(str[i]); i++); // quoteless
	return std::string(str + startPos, i - startPos);
}

CConfig ParsePair(const char *str, std::size_t &i);

CConfig ParseKeyValues(std::string keyValues)
{
	std::size_t i = 0;
	return ParsePair(keyValues.c_str(), i);
}

CConfig ParseValue(const char *str, std::size_t &i)
{
	if (str[i] == '{')
	{
		//std::cout << "[parse value] is {\n";
		i++;
		return ParsePair(str, i);
	}
	else if (str[i] == '[')
	{
		//std::cout << "[parse value] is [\n";
		i++;
		CConfig val;
		val.MakeList();
		SkipSpace(str, i);
		std::string shit;
		for (; str[i] && str[i] != ']';)
		{
			SkipSpace(str, i);
			//std::cout << "[parser value] pushing to list, at " << str[i] << "\n";
			val.Append(ParseValue(str, i));
			//std::cout << "->" << str[i] << std::endl;
			SkipSpace(str, i);
			//std::cout << "->" << str[i] << std::endl;

			//std::cin >> shit;
		}
		return val;
	}
	else
	{
		auto string = ReadString(str, i);
		//std::cout << "[parse value] it's a string " << string << str[i] << "\n";
		return CConfig(string);
	}
}

CConfig ParsePair(const char *str, std::size_t &i)
{
	CConfig config;
	config.MakeObject();
	for (; str[i]; i++)
	{
		SkipSpace(str, i);
		
		if (str[i] == '}')
		{
			i++;
			return config;
		}

		std::string key = ReadString(str, i);
		//std::cout << "[parser] Key " << key << '\n';

		SkipSpace(str, i);
		CConfig val = ParseValue(str, i);


		config[key] = val;
	}

	return config;
}
