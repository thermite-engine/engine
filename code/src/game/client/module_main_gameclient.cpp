#include "log.h"
#include "service.h"

extern "C" THR_EXPORT void ModuleInit(CServer::CServerInstance *instance)
{
	CServer::Init(instance);
	Log::Init(instance->Get<ILogger>(), "GameServer", fmt::color::lavender_blush);
}

extern "C" THR_EXPORT void ModuleShutdown()
{
	Log::Print("Shutting down...\n");
}