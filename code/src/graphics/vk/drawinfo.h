#pragma once

#include <glm/glm.hpp>
#include <volk.h>

#include <cstdint>
#include <vector>

#include "components/transform.h"

class CMaterialInstanceVK;

struct DrawCmdBase
{
	Transform transform;
	CMaterialInstanceVK *materialInstance = nullptr; // TODO: Find a better way
	VkBuffer vertexBuffer = VK_NULL_HANDLE;
	VkBuffer indexBuffer = VK_NULL_HANDLE;
	uint32_t vertexCount = 0;
	uint32_t indexCount = 0;
};

struct DrawCmdBillboard : public DrawCmdBase
{
	glm::vec4 color; // push constant
};

struct UiPushConstant
{
	glm::vec2 scale;
	glm::vec2 translate;
};

struct UiDrawSetup
{
	UiPushConstant pushConstant;
	VkViewport viewport = {};
	VkBuffer vertexBuffer = VK_NULL_HANDLE;
	VkBuffer indexBuffer = VK_NULL_HANDLE;
};

struct UiDrawCmd
{
	VkDescriptorSet descriptorSet = VK_NULL_HANDLE;
	VkRect2D scissor = {};
	uint32_t indexCount = 0;
	uint32_t firstIndex = 0;
	int32_t vertexOffset = 0;
};

struct UiDraw
{
	UiDrawSetup setup;
	std::vector<UiDrawCmd> drawCmds;
};

struct SceneDrawSetup
{
	// TODO: Dynamic viewport and scissor for multiple subpasses?
	VkViewport viewport;
	VkRect2D scissor;
};

struct SceneMeshBase
{
	std::vector<Transform> instances;
	CMaterialInstanceVK *materialInstance = nullptr; // TODO: Find a better way
	VkDescriptorSet descriptorSet = VK_NULL_HANDLE;
	VkBuffer vertexBuffer = VK_NULL_HANDLE;
	VkBuffer indexBuffer = VK_NULL_HANDLE;
	uint32_t vertexCount = 0;
	uint32_t indexCount = 0;
};

struct SceneSkyboxMesh : public SceneMeshBase
{
	struct ViewProj
	{
		glm::mat4 view;
		glm::mat4 proj;
	};

	ViewProj pushConstant;
};

struct SceneStaticMesh : public SceneMeshBase {};
struct SceneBillboardMesh : public SceneMeshBase
{
	glm::vec4 color;
};

struct SceneDraw
{
	SceneDrawSetup setup;
	SceneSkyboxMesh skyboxMesh;
	std::vector<SceneStaticMesh> opaqueStaticMeshes;
	std::vector<SceneStaticMesh> transparentStaticMeshes;
	std::vector<SceneBillboardMesh> opaqueBillboardMeshes;
	std::vector<SceneBillboardMesh> transparentBillboardMeshes;
};