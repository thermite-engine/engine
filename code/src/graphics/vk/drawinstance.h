#pragma once

#include <vector>
#include "meshvk.h"
#include "components/transform.h"

struct DrawInstanceInfo_t
{
	CMeshVK *mesh = nullptr;
	std::vector<Transform*> transforms;
};