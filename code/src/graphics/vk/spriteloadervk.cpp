#include "spriteloadervk.h"
#include "graphicsvk.h"
#include "texturecachevk.h"
#include "vertex.h"
#include "shader_billboard.h"
#include "materialcachevk.h"

#include <cassert>

IBillboardSprite *CSpriteLoaderVK::LoadBillboardSprite(const std::filesystem::path &spritePath)
{
	CBillboardSpriteVK *sprite = billboardSprites.emplace_back(std::make_unique<CBillboardSpriteVK>()).get();

	sprite->materialInstance = std::make_unique<CMaterialInstanceVK>();
	sprite->materialInstance->graphics = graphics;
	sprite->materialInstance->material = materialCache->CreateMaterial();
	sprite->materialInstance->material->SetShader(graphics->shaderSystem->shaders.billboard);
	sprite->materialInstance->material->SetTexture("diffuse", spritePath);

	graphics->shaderSystem->shaders.billboard->InitMaterialInstance(textureCache, nullptr, sprite->materialInstance.get());
	graphics->SetupMaterialInstance(sprite->materialInstance.get());

	// TODO: Make this better
	ITexture *texture = sprite->materialInstance->GetSampler(CShaderBillboard::BINDING_DIFFUSE);

	std::vector<BillboardVertex> billboardVertices = {
		{ { -0.5f, -0.5f, 0.0f }, { 0.0f, 1.0f } },
		{ { 0.5f, -0.5f, 0.0f }, { 1.0f, 1.0f } },
		{ { -0.5f, 0.5f, 0.0f }, { 0.0f, 0.0f } },
		{ { 0.5f, 0.5f, 0.0f }, { 1.0f, 0.0f } }
	};

	std::vector<uint32_t> billboardIndices = {
		0, 1, 2,
		2, 1, 3
	};

	const float width = (float)texture->GetWidth();
	const float height = (float)texture->GetHeight();
	const float divisor = width > height ? width : height;
	const glm::vec2 scale = { width / divisor, height / divisor };

	for (auto &v : billboardVertices)
	{
		v.position.x *= scale.x;
		v.position.y *= scale.y;
	}

	sprite->vertexBuffer = graphics->CreateVertexBuffer(sizeof(BillboardVertex), (VkDeviceSize)billboardVertices.size(), billboardVertices.data());
	sprite->indexBuffer = graphics->CreateIndexBuffer(sizeof(uint32_t), (VkDeviceSize)billboardIndices.size(), billboardIndices.data());

	return sprite;
}

void CSpriteLoaderVK::UnloadBillboardSprite(IBillboardSprite *sprite)
{
	assert(sprite);

	CBillboardSpriteVK *billboardSprite = static_cast<CBillboardSpriteVK*>(sprite);

	for (auto &[binding, sb] : billboardSprite->materialInstance.get()->shaderBuffers)
	{
		graphics->shaderSystem->DeleteShaderBuffer(sb.buffer);
		sb.buffer = nullptr;
	}

	if (billboardSprite->materialInstance->descriptorPool)
		graphics->DestroyDescriptorPool(billboardSprite->materialInstance->descriptorPool);

	graphics->DestroyVertexBuffer(billboardSprite->vertexBuffer);
	graphics->DestroyIndexBuffer(billboardSprite->indexBuffer);

	for (auto it = billboardSprites.begin(); it != billboardSprites.end(); ++it)
	{
		if (it->get() == billboardSprite)
		{
			billboardSprites.erase(it);
			break;
		}
	}
}

void CSpriteLoaderVK::UnloadResources()
{
	for (auto &billboardSprite : billboardSprites)
	{
		for (auto &[binding, sb] : billboardSprite->materialInstance.get()->shaderBuffers)
		{
			graphics->shaderSystem->DeleteShaderBuffer(sb.buffer);
			sb.buffer = nullptr;
		}

		if (billboardSprite->materialInstance->descriptorPool)
			graphics->DestroyDescriptorPool(billboardSprite->materialInstance->descriptorPool);

		graphics->DestroyVertexBuffer(billboardSprite->vertexBuffer);
		graphics->DestroyIndexBuffer(billboardSprite->indexBuffer);
	}

	billboardSprites.clear();
}