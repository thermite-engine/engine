#pragma once

#include <optional>
#include <volk.h>

class CRenderWindow;

struct QueueFamilyIndices
{
	std::optional<uint32_t> graphicsFamily;
	std::optional<uint32_t> presentFamily;
	std::optional<uint32_t> computeFamily;

	bool IsComplete() const { return ( graphicsFamily.has_value() && presentFamily.has_value() && computeFamily.has_value() ); }
};

QueueFamilyIndices FindQueueFamilies(CRenderWindow *renderWindow, VkPhysicalDevice device);