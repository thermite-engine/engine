#pragma once

#include "modelcache.h"
#include "modelvk.h"
#include "resourceownervk.h"
#include "materialinstance.h"
#include "lightvk.h"

#include <memory>
#include <unordered_map>
#include <vector>
#include <string>

class CGraphicsVK;

class CModelCacheVK : public IModelCache
{
public:
	void LoadResources();
	void UnloadResources();

	IModel *LoadModel(const std::filesystem::path& path) override;

	CGraphicsVK *graphics = nullptr;
	CResourceOwnerVK *owner = nullptr;
	CLightStateVK *lightState = nullptr;


private:

	std::unordered_map<std::string, std::unique_ptr<CModelVK>> models;
	CModelVK *errorModel = nullptr;
};