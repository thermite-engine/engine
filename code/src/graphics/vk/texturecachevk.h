#pragma once

#include "texturecache.h"
#include "resourceownervk.h"

#include <string>
#include <unordered_map>
#include <vector>

class CGraphicsVK;

class CTextureCacheVK : public ITextureCache
{
public:
	void LoadFallbackTextures();
	void UnloadTextures();

	ITexture *LoadSprite(const std::filesystem::path &path, ETextureFallback fallback, bool generateMipMaps);
	ITexture *LoadTexture(const std::filesystem::path &path, ETextureFallback fallback, bool generateMipMaps) override;
	void UnloadTexture(ITexture *texture) override;
	ITexture *LoadSkybox(const std::filesystem::path &pathNoExtension, const std::filesystem::path &extension) override;
	ITexture *LoadSkybox(const std::array<std::filesystem::path, 6> facePaths) override;
	ITexture *CreateTexture(EImageType imageType, EImageFormat imageFormat, uint32_t width, uint32_t height, const void *pixels, bool generateMipMaps) override;

	ITexture *GetFallbackTexture(ETextureFallback fallback) const
	{
		switch (fallback)
		{
			case ETextureFallback::White:
				return textureWhite;
				break;
			case ETextureFallback::Diffuse_Error2D:
				return textureDiffuseError2D;
				break;
			case ETextureFallback::Diffuse_ErrorCube:
				return textureDiffuseErrorCube;
				break;
			case ETextureFallback::Normal_Flat:
				return textureNormalFlat;
				break;
			default:
				throw std::runtime_error("[TextureCache]Invalid texture fallback specified.");
				break;
		}
	}

	CGraphicsVK *graphics = nullptr;
	CResourceOwnerVK *owner = nullptr;

protected:

	std::unordered_map<std::string, ITexture*> textures;
	std::vector<ITexture*> createdTextures;

	ITexture *textureWhite = nullptr;
	ITexture *textureDiffuseError2D = nullptr;
	ITexture *textureDiffuseErrorCube = nullptr;
	ITexture *textureNormalFlat = nullptr;
};
