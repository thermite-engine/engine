#include "shadersystem.h"
#include "renderervk_impl.h"
#include "graphicsvk.h"

#include "shader_staticmesh.h"
#include "shader_staticmesh_transparent.h"
#include "shader_transparent_composition.h"
#include "shader_ui.h"
#include "shader_skybox.h"
#include "shader_billboard.h"
#include "shader_billboard_transparent.h"

#include <fstream>

void CShaderSystemVK::OnWindowResize()
{
	// NOTE: We assume vkDeviceWaitIdle has already been called
	// TODO: This function may no longer be required, remove maybe?
}

void CShaderSystemVK::LoadShaders()
{
	shaders.skybox = CreateShader<CShaderSkybox>("skybox");
	shaders.staticMesh = CreateShader<CShaderStaticMesh>("static_mesh");
	shaders.staticMeshTransparent = CreateShader<CShaderStaticMeshTransparent>("static_mesh_transparent");
	shaders.transparentComposition = CreateShader<CShaderTransparentComposition>("transparent_composition");
	shaders.billboard = CreateShader<CShaderBillboard>("billboard");
	shaders.billboardTransparent = CreateShader<CShaderBillboardTransparent>("billboard_transparent");
	shaders.ui = CreateShader<CShaderUI>("ui");
}

void CShaderSystemVK::UnloadShaders()
{
	// NOTE: We assume vkDeviceWaitIdle has already been called

	for (auto &[id, shaderHandle] : shaderMap)
	{
		for (auto shaderModule : shaderHandle->shaderModules)
		{
			if (shaderModule != VK_NULL_HANDLE)
				vkDestroyShaderModule(graphics->device, shaderModule, nullptr);
		}

		shaderHandle->shaderModules.clear();

		if (shaderHandle->pipeline != VK_NULL_HANDLE)
		{
			vkDestroyPipeline(graphics->device, shaderHandle->pipeline, nullptr);
			shaderHandle->pipeline = VK_NULL_HANDLE;
		}

		if (shaderHandle->pipelineLayout != VK_NULL_HANDLE)
		{
			vkDestroyPipelineLayout(graphics->device, shaderHandle->pipelineLayout, nullptr);
			shaderHandle->pipelineLayout = VK_NULL_HANDLE;
		}

		if (shaderHandle->descriptorSetLayout != VK_NULL_HANDLE)
		{
			vkDestroyDescriptorSetLayout(graphics->device, shaderHandle->descriptorSetLayout, nullptr);
			shaderHandle->descriptorSetLayout = VK_NULL_HANDLE;
		}
	}

	shaderMap.clear();
}

void CShaderSystemVK::RebuildPipelines()
{
	// NOTE: We assume vkDeviceWaitIdle has already been called

	for (auto &[id, shaderHandle] : shaderMap)
	{
		if (shaderHandle->pipeline != VK_NULL_HANDLE)
		{
			vkDestroyPipeline(graphics->device, shaderHandle->pipeline, nullptr);
			shaderHandle->pipeline = VK_NULL_HANDLE;
		}

		shaderHandle->CreateGraphicsPipeline();
	}
}

VkShaderModule CShaderSystemVK::CreateShaderModule(const std::filesystem::path shaderPath)
{
	// TODO: Error handling
	auto fileSize = std::filesystem::file_size(shaderPath);

	std::vector<char> code(fileSize);
	std::ifstream(shaderPath, std::ios::binary).read(code.data(), code.size());

	VkShaderModuleCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.codeSize = code.size();
	createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());

	VkShaderModule shaderModule = VK_NULL_HANDLE;
	if (vkCreateShaderModule(graphics->device, &createInfo, nullptr, &shaderModule) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create shader module");

	return shaderModule;
}

void CShaderSystemVK::ResizeStorageBuffer(CShaderBuffer *shaderBuffer, VkDeviceSize bufferSize)
{
	if (shaderBuffer->descriptorType != VK_DESCRIPTOR_TYPE_STORAGE_BUFFER)
		throw std::runtime_error("Attempting to resize shader buffer that isn't storage");

	shaderBuffer->bufferSize = bufferSize;

	for (std::size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
	{
		vmaUnmapMemory(graphics->allocator, shaderBuffer->bufferAllocation[i]);
		graphics->DeleteBuffer(shaderBuffer->buffer[i], shaderBuffer->bufferAllocation[i]);
		graphics->CreateBuffer(shaderBuffer->bufferSize, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU, shaderBuffer->buffer[i], shaderBuffer->bufferAllocation[i]);
		vmaMapMemory(graphics->allocator, shaderBuffer->bufferAllocation[i], &shaderBuffer->mapped[i]);
	}

	shaderBuffer->MarkMaterialInstancesDirty();
}

void CShaderSystemVK::DeleteShaderBuffer(CShaderBuffer *shaderBuffer)
{
	auto it = std::find_if(shaderBuffers.begin(), shaderBuffers.end(), [&](const std::unique_ptr<CShaderBuffer> &shaderBufferHandle) { return shaderBuffer == shaderBufferHandle.get(); } );
	if (it != shaderBuffers.end())
	{
		for (std::size_t i = 0; i < shaderBuffer->buffer.size(); ++i)
		{
			vmaUnmapMemory(graphics->allocator, shaderBuffer->bufferAllocation[i]);
			graphics->DeleteBuffer(shaderBuffer->buffer[i], shaderBuffer->bufferAllocation[i]);
		}

		shaderBuffers.erase(it);
	}
}

CShaderBuffer *CShaderSystemVK::CreateShaderBuffer_Internal(VkDeviceSize bufferSize, VkBufferUsageFlags bufferUsage, VkDescriptorType descriptorType)
{
	CShaderBuffer *shaderBuffer = shaderBuffers.emplace_back(std::make_unique<CShaderBuffer>()).get();
	shaderBuffer->graphics = graphics;
	shaderBuffer->bufferSize = bufferSize;
	shaderBuffer->descriptorType = descriptorType;

	for (std::size_t i = 0; i < shaderBuffer->buffer.size(); ++i)
	{
		graphics->CreateBuffer(shaderBuffer->bufferSize, bufferUsage, VMA_MEMORY_USAGE_CPU_TO_GPU, shaderBuffer->buffer[i], shaderBuffer->bufferAllocation[i]);
		vmaMapMemory(graphics->allocator, shaderBuffer->bufferAllocation[i], &shaderBuffer->mapped[i]);
	}

	return shaderBuffer;
}