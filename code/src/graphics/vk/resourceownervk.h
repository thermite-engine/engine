#pragma once

class CTextureCacheVK;
class CModelCacheVK;
class CMaterialCacheVK;
class CSpriteLoaderVK;

class CResourceOwnerVK
{
public:
	CTextureCacheVK *textureCache = nullptr;
	CModelCacheVK *modelCache = nullptr;
	CMaterialCacheVK *materialCache = nullptr;
	CSpriteLoaderVK *spriteLoader = nullptr;
};