#pragma once

#include <unordered_map>
#include <string>
#include <filesystem>

#include <glm/glm.hpp>

class CShaderVK;

class CMaterialVK
{
public:
	void SetTexture(const std::string &identifier, const std::filesystem::path &texturePath) { textures[ identifier ] = texturePath; }
	void SetInt(const std::string &identifier, glm::i32 i32) { i32s[ identifier ] = i32; }
	void SetFloat(const std::string &identifier, glm::f32 f32) { f32s[ identifier ] = f32; }

	std::filesystem::path GetTexture(const std::string &identifier) { return textures[ identifier ]; }
	glm::i32 GetInt(const std::string &identifier) { return i32s[ identifier ]; }
	glm::f32 GetFloat(const std::string &identifier) { return f32s[ identifier ]; }

	void SetShader(CShaderVK *shader) { this->shader = shader; }
	CShaderVK *GetShader() const { return shader; }

protected:
	std::unordered_map<std::string, std::filesystem::path> textures;
	std::unordered_map<std::string, glm::i32> i32s;
	std::unordered_map<std::string, glm::f32> f32s;

	CShaderVK *shader = nullptr;
};