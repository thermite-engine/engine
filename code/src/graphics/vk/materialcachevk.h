#pragma once

#include "materialvk.h"
#include "resourceownervk.h"

#include <vector>
#include <memory>

class CGraphicsVK;

class CMaterialCacheVK
{
public:
	void LoadResources();
	void UnloadResources();

	CMaterialVK *CreateMaterial();
	void DeleteMaterial(CMaterialVK *material);

	CMaterialVK *GetErrorMaterial() { return errorMaterial; }

public:
	CGraphicsVK *graphics = nullptr;
	CResourceOwnerVK *owner = nullptr;

private:
	std::vector<std::unique_ptr<CMaterialVK>> materials;
	CMaterialVK *errorMaterial = nullptr;
};