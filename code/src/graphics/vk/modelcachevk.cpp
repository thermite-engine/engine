#include "modelcachevk.h"
#include "log/log.h"
#include "graphicsvk.h"
#include "materialcachevk.h"

#include <fstream>

#include <tiny_obj_loader.h>
#include <glm/gtx/hash.hpp>

// TODO: Move this to a better place
namespace ModelLoader
{
	struct Material
	{
		std::string name;
		std::filesystem::path diffuseTexture;
		std::filesystem::path normalTexture;
	};

	struct Mesh
	{
		std::size_t materialIndex = 0;
		std::vector<Vertex> vertices;
		std::vector<uint32_t> indices;
		glm::vec3 mins = {};
		glm::vec3 maxs = {};
	};

	void LoadOBJ(const std::filesystem::path &path, std::vector<Mesh> &meshes, std::vector<Material> &materials) {
		tinyobj::ObjReader reader;
		tinyobj::ObjReaderConfig reader_config;

		reader_config.mtl_search_path = path.parent_path().string();

		if (!reader.ParseFromFile(path.string(), reader_config))
		{
			if (!reader.Error().empty()) {
				Log::Print(ESeverity::Error, "{}\n", reader.Error());
			}

			return;
		}

		if (!reader.Warning().empty()) {
			Log::Print(ESeverity::Warning, "{}\n", reader.Warning());
		}

		auto &objAttrib = reader.GetAttrib();
		auto &objShapes = reader.GetShapes();
		auto &objMaterials = reader.GetMaterials();

		materials.resize(objMaterials.size());
		meshes.resize(objMaterials.size());

		for (std::size_t i = 0; i < meshes.size(); ++i) {
			meshes[i].materialIndex = i;
		}

		for (std::size_t i = 0; i < objMaterials.size(); ++i) {
			const tinyobj::material_t &objMaterial = objMaterials[i];
			Material &material = materials[i];
			material.name = objMaterial.name;

			if (!objMaterial.diffuse_texname.empty()) {
				material.diffuseTexture = objMaterial.diffuse_texname;

				if (material.diffuseTexture.is_relative()) {
					material.diffuseTexture = path.parent_path() / material.diffuseTexture;
				}
			}

			if (!objMaterial.normal_texname.empty()) {
				material.normalTexture = objMaterial.normal_texname;

				if (material.normalTexture.is_relative()) {
					material.normalTexture = path.parent_path() / material.normalTexture;
				}
			}
		}

		for (std::size_t shapeIndex = 0; shapeIndex < objShapes.size(); ++shapeIndex)
		{
			std::size_t indexOffset = 0;
			for (std::size_t faceIndex = 0; faceIndex < objShapes[shapeIndex].mesh.num_face_vertices.size(); ++faceIndex)
			{
				const std::size_t faceVertexCount = static_cast<std::size_t>(objShapes[shapeIndex].mesh.num_face_vertices[faceIndex]);
				if (faceVertexCount != 3) {
					// TODO: This really shouldn't happen, and we should have better error handling in the event it does
					Log::FatalError("Model is not trianglated!\n");
				}

				const std::size_t materialIndex = objShapes[shapeIndex].mesh.material_ids[faceIndex] < 0 ? materials.size() - 1 : objShapes[shapeIndex].mesh.material_ids[faceIndex];
				assert(materialIndex < materials.size());

				Mesh& mesh = meshes[materialIndex];

				struct tinyobj_vec2 {
					tinyobj::real_t x;
					tinyobj::real_t y;
				};

				struct tinyobj_vec3 {
					tinyobj::real_t x;
					tinyobj::real_t y;
					tinyobj::real_t z;
				};

				for (std::size_t v = 0; v < faceVertexCount; ++v) {
					glm::vec3 vPosition = {};
					glm::vec3 vNormal = {};
					glm::vec2 vTexcoord = {};
					glm::vec4 vColor = {};

					constexpr std::size_t positionStride = 3;
					constexpr std::size_t normalStride = 3;
					constexpr std::size_t texcoordStride = 2;
					constexpr std::size_t colorStride = 3;

					const tinyobj::index_t idx = objShapes[shapeIndex].mesh.indices[indexOffset + v];
					const tinyobj_vec3& pos = reinterpret_cast<const tinyobj_vec3&>(objAttrib.vertices[positionStride * static_cast<std::size_t>(idx.vertex_index)]);

					vPosition = {
						pos.x,
						pos.y,
						pos.z
					};

					if (idx.normal_index >= 0) {
						const tinyobj_vec3& norm = reinterpret_cast<const tinyobj_vec3&>(objAttrib.normals[normalStride * static_cast<std::size_t>(idx.normal_index)]);
						vNormal = {
							norm.x,
							norm.y,
							norm.z
						};
					}

					if (idx.texcoord_index >= 0) {
						const tinyobj_vec2& texcoord = reinterpret_cast<const tinyobj_vec2&>(objAttrib.texcoords[texcoordStride * static_cast<std::size_t>(idx.texcoord_index)]);

						vTexcoord = {
							texcoord.x,
							1.0f - texcoord.y
						};
					}

					const tinyobj_vec3& color = reinterpret_cast<const tinyobj_vec3&>(objAttrib.colors[colorStride * static_cast<std::size_t>(idx.vertex_index)]);
					vColor = {
						color.x,
						color.y,
						color.z,
						1.0f
					};


					mesh.mins.x = std::min(vPosition.x, mesh.mins.x);
					mesh.mins.y = std::min(vPosition.y, mesh.mins.y);
					mesh.mins.z = std::min(vPosition.z, mesh.mins.z);

					mesh.maxs.x = std::max(vPosition.x, mesh.maxs.x);
					mesh.maxs.y = std::max(vPosition.y, mesh.maxs.y);
					mesh.maxs.z = std::max(vPosition.z, mesh.maxs.z);

					bool uniqueVertex = true;
					uint32_t newIndex = static_cast<uint32_t>(mesh.vertices.size());
					assert(mesh.vertices.size() < std::numeric_limits<uint32_t>::max());

					// Is this a duplicate vertex?
					for (std::size_t i = 0; i < mesh.vertices.size(); ++i) {
						const Vertex& vertex = mesh.vertices[i];

						if (vertex.position == vPosition && vertex.normal == vNormal && vertex.uv == vTexcoord && vertex.color == vColor) {
							uniqueVertex = false;
							newIndex = static_cast<uint32_t>(i);
							break;
						}
					}

					if (uniqueVertex) {
						Vertex &vert = mesh.vertices.emplace_back();
						vert.position = vPosition;
						vert.normal = vNormal;
						vert.uv = vTexcoord;
						vert.color = vColor;
					}

					mesh.indices.emplace_back(newIndex);
				}

				indexOffset += faceVertexCount;
			}
		}
	}
}

void CModelCacheVK::LoadResources()
{
	errorModel = CModelVK::ToModelVK(LoadModel("models/error.obj"));

	if (!errorModel)
		Log::FatalError("Failed to load error model!");
}

void CModelCacheVK::UnloadResources()
{	
	for (auto &[id, modelHandle] : models)
	{
		for (auto &mesh : modelHandle->meshes)
		{
			CMaterialInstanceVK *materialInstance = mesh->materialInstance.get();
			materialInstance->material->GetShader()->ShutdownMaterialInstance(materialInstance);

			materialInstance->shaderBuffers.clear();
			materialInstance->descriptorSets.fill(VK_NULL_HANDLE);
			
			if (materialInstance->descriptorPool != VK_NULL_HANDLE)
			{
				graphics->DestroyDescriptorPool(materialInstance->descriptorPool);
				materialInstance->descriptorPool = VK_NULL_HANDLE;
			}

			graphics->DestroyIndexBuffer(mesh->indexBuffer);
			graphics->DestroyVertexBuffer(mesh->vertexBuffer);

			mesh->indexBuffer = nullptr;
			mesh->vertexBuffer = nullptr;
		}
	}

	models.clear();
	errorModel = nullptr;
}

IModel *CModelCacheVK::LoadModel(const std::filesystem::path &path)
{
	assert(lightState);

	// TODO: Better caching
	if (auto it = models.find(path.generic_string()); it != models.end())
		return it->second.get();

	std::vector<ModelLoader::Mesh> meshes;
	std::vector<ModelLoader::Material> materials;

	if (path.extension() == ".obj") {
		Log::Print("Loading model {}\n", path.generic_string());
		ModelLoader::LoadOBJ(path, meshes, materials);
	}

	if (meshes.empty()) {
		return errorModel;
	}

	std::unique_ptr<CModelVK> modelHandle = std::make_unique<CModelVK>();

	for (std::size_t i = 0; i < meshes.size(); ++i) {
		const auto &mesh = meshes[i];
		const auto &material = materials[mesh.materialIndex];

		CMeshVK *meshVK = modelHandle->meshes.emplace_back(std::make_unique<CMeshVK>()).get();
		CMaterialVK *materialVK = owner->materialCache->GetErrorMaterial();
		CShaderVK *shader = graphics->shaderSystem->shaders.staticMesh;

		if (!material.name.empty()) {
			materialVK = owner->materialCache->CreateMaterial();
			materialVK->SetShader(shader);

			if (!material.diffuseTexture.empty()) {
				materialVK->SetTexture("diffuse", material.diffuseTexture);
			}

			if (!material.normalTexture.empty()) {
				materialVK->SetTexture("normal", material.normalTexture);
			}
		}

		meshVK->vertexBuffer = graphics->CreateVertexBuffer(sizeof(Vertex), mesh.vertices.size(), mesh.vertices.data());

		if (mesh.indices.size() > 0)
			meshVK->indexBuffer = graphics->CreateIndexBuffer(sizeof(uint32_t), mesh.indices.size(), mesh.indices.data());

		meshVK->mins = mesh.mins;
		meshVK->maxs = mesh.maxs;
		meshVK->radius = glm::distance(meshVK->mins, meshVK->maxs) / 2.0f;

		meshVK->materialInstance = std::make_unique<CMaterialInstanceVK>();

		CMaterialInstanceVK* materialInstance = meshVK->materialInstance.get();
		materialInstance->graphics = graphics;
		materialInstance->material = materialVK;

		shader->InitMaterialInstance(owner->textureCache, lightState, materialInstance);
		graphics->SetupMaterialInstance(materialInstance);
	}

	CModelVK *model = modelHandle.get();
	models[path.generic_string()] = std::move(modelHandle);

	return model;
}