#pragma once

#include "vertex.h"
#include "glm/glm.hpp"

#include <volk.h>

#include <cstdint>
#include <cstddef>
#include <vector>

inline VkVertexInputBindingDescription BillboardVertex_ToInputBindingDescription(uint32_t binding)
{
	VkVertexInputBindingDescription description = {};
	description.binding = binding;
	description.stride = sizeof(BillboardVertex);
	description.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	return description;
}

inline std::vector<VkVertexInputAttributeDescription> BillboardVertex_ToInputAttributeDescriptions(uint32_t binding)
{
	std::vector<VkVertexInputAttributeDescription> descriptions(3);

	for (std::size_t i = 0; i < descriptions.size(); ++i)
	{
		auto &desc = descriptions[i];
		desc.binding = binding;
		desc.location = static_cast<uint32_t>(i);
	}

	descriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;		// Position
	descriptions[1].format = VK_FORMAT_R32G32_SFLOAT;			// UV
	descriptions[2].format = VK_FORMAT_R32G32B32A32_SFLOAT;		// Color

	descriptions[0].offset = offsetof(BillboardVertex, position);
	descriptions[1].offset = offsetof(BillboardVertex, uv);
	descriptions[2].offset = offsetof(BillboardVertex, color);

	return descriptions;
}

inline VkVertexInputBindingDescription UIVertex_ToInputBindingDescription(uint32_t binding)
{
	VkVertexInputBindingDescription description = {};
	description.binding = binding;
	description.stride = sizeof(UIVertex);
	description.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	return description;
}

inline std::vector<VkVertexInputAttributeDescription> UIVertex_ToInputAttributeDescriptions(uint32_t binding)
{
	std::vector<VkVertexInputAttributeDescription> descriptions(3);

	for (std::size_t i = 0; i < descriptions.size(); ++i)
	{
		auto &desc = descriptions[i];
		desc.binding = binding;
		desc.location = static_cast<uint32_t>(i);
	}

	descriptions[0].format = VK_FORMAT_R32G32_SFLOAT;			// Position
	descriptions[1].format = VK_FORMAT_R32G32_SFLOAT;			// UV
	descriptions[2].format = VK_FORMAT_R8G8B8A8_UNORM;			// Color

	descriptions[0].offset = offsetof(UIVertex, position);
	descriptions[1].offset = offsetof(UIVertex, uv);
	descriptions[2].offset = offsetof(UIVertex, color);

	return descriptions;
}

inline VkVertexInputBindingDescription SkyVertex_ToInputBindingDescription(uint32_t binding)
{
	VkVertexInputBindingDescription description = {};
	description.binding = binding;
	description.stride = sizeof(SkyVertex);
	description.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	return description;
}

inline std::vector<VkVertexInputAttributeDescription> SkyVertex_ToInputAttributeDescriptions(uint32_t binding)
{
	std::vector<VkVertexInputAttributeDescription> descriptions(2);

	for (std::size_t i = 0; i < descriptions.size(); ++i)
	{
		auto &desc = descriptions[i];
		desc.binding = binding;
		desc.location = static_cast<uint32_t>(i);
	}

	descriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;		// Position
	descriptions[1].format = VK_FORMAT_R32G32B32A32_SFLOAT;		// Color

	descriptions[0].offset = offsetof(SkyVertex, position);
	descriptions[1].offset = offsetof(SkyVertex, color);

	return descriptions;
}

inline VkVertexInputBindingDescription Vertex_ToInputBindingDescription(uint32_t binding)
{
	VkVertexInputBindingDescription description = {};
	description.binding = binding;
	description.stride = sizeof(Vertex);
	description.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	return description;
}

inline std::vector<VkVertexInputAttributeDescription> Vertex_ToInputAttributeDescriptions(uint32_t binding)
{
	std::vector<VkVertexInputAttributeDescription> descriptions(6);

	for (std::size_t i = 0; i < descriptions.size(); ++i)
	{
		auto &desc = descriptions[i];
		desc.binding = binding;
		desc.location = static_cast<uint32_t>(i);
	}

	descriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;		// Position
	descriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;		// Normal
	descriptions[2].format = VK_FORMAT_R32G32_SFLOAT;			// UV
	descriptions[3].format = VK_FORMAT_R32G32B32A32_SFLOAT;		// Color
	descriptions[4].format = VK_FORMAT_R32G32B32A32_UINT;		// Bone indices
	descriptions[5].format = VK_FORMAT_R32G32B32A32_SFLOAT;		// Bone weights

	descriptions[0].offset = offsetof(Vertex, position);
	descriptions[1].offset = offsetof(Vertex, normal);
	descriptions[2].offset = offsetof(Vertex, uv);
	descriptions[3].offset = offsetof(Vertex, color);
	descriptions[4].offset = offsetof(Vertex, boneindex);
	descriptions[5].offset = offsetof(Vertex, boneweight);

	return descriptions;
}