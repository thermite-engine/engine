#pragma once

#include "mesh.h"
#include "vertexvk.h"
#include "vertexbuffervk.h"
#include "indexbuffervk.h"
#include "materialinstance.h"
#include <volk.h>
#include <vk_mem_alloc.h>
#include <memory>

class CRendererVK;

class CMeshVK : public IStaticMesh
{
public:
	std::unique_ptr<CMaterialInstanceVK> materialInstance;

	CVertexBuffer *vertexBuffer = nullptr;
	CIndexBuffer *indexBuffer = nullptr;

	glm::vec3 maxs;
	glm::vec3 mins;
	float radius = 0.0f;
};