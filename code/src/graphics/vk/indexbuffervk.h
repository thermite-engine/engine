#pragma once
#include <cstdint>
#include "buffervk.h"

template <typename T>
class CIndexBufferBase : public T
{
public:
	uint32_t indexCount = 0;
};

class CDynamicIndexBuffer : public CIndexBufferBase<CDynamicBufferVK>{};
class CIndexBuffer : public CIndexBufferBase<CStaticBufferVK>{};
