#include "materialinstance.h"
#include "graphicsvk.h"

void CMaterialInstanceVK::SetBuffer(uint32_t binding, CShaderBuffer *buffer)
{
	ShaderBuffer &sb = shaderBuffers[binding];

	if (sb.buffer)
		sb.buffer->RemoveMaterialInstance(this);

	sb.buffer = buffer;
	sb.buffer->AddMaterialInstance(this);
	sb.MarkDirty();
}

void CMaterialInstanceVK::SetSampler(uint32_t binding, ITexture *texture)
{
	Sampler &sampler = samplers[binding];
	sampler.texture = texture;
	sampler.MarkDirty();
}

void CMaterialInstanceVK::MarkBufferDirty(CShaderBuffer *buffer)
{
	for (auto &[binding, sb] : shaderBuffers)
	{
		if (sb.buffer == buffer)
		{
			sb.MarkDirty();
			break;
		}
	}
}

void CMaterialInstanceVK::UpdateDirtyDescriptorSets(uint32_t frame)
{
	for (auto &[binding, sampler] : samplers)
	{
		if (sampler.dirty[frame])
		{
			CTextureVK *texture = CTextureVK::ToTextureVK(sampler.texture);
			sampler.dirty[frame] = false;

			VkDescriptorImageInfo imageInfo = {};
			imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			imageInfo.imageView = texture->imageView;
			imageInfo.sampler = texture->sampler;

			VkWriteDescriptorSet descriptorWrite = {};
			descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrite.dstSet = descriptorSets[frame];
			descriptorWrite.dstBinding = binding;
			descriptorWrite.dstArrayElement = 0;
			descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			descriptorWrite.descriptorCount = 1;
			descriptorWrite.pImageInfo = &imageInfo;

			vkUpdateDescriptorSets(graphics->device, 1, &descriptorWrite, 0, nullptr);
		}
	}

	for (auto &[binding, sb] : shaderBuffers)
	{
		if (sb.dirty[frame])
		{
			VkDescriptorBufferInfo bufferInfo = {};
			bufferInfo.buffer = sb.buffer->buffer[frame];
			bufferInfo.offset = 0;
			bufferInfo.range = sb.buffer->bufferSize;

			VkWriteDescriptorSet descriptorWrite = {};
			descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrite.dstSet = descriptorSets[frame];
			descriptorWrite.dstBinding = binding;
			descriptorWrite.dstArrayElement = 0;
			descriptorWrite.descriptorType = sb.buffer->descriptorType;
			descriptorWrite.descriptorCount = 1;
			descriptorWrite.pBufferInfo = &bufferInfo;

			vkUpdateDescriptorSets(graphics->device, 1, &descriptorWrite, 0, nullptr);
		}
	}
}