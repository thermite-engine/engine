#pragma once

#include "graphicsvk_definitions.h"
#include "light.h"
#include "shaderbuffer.h"

#include <array>
#include <vector>
#include <memory>

class CShaderSystemVK;

class CPointLightVK : public IPointLight
{
public:
	uint32_t GetLightIndex() const override { return lightIndex; }

	const glm::vec3 &GetPosition() const override { return position; }
	void SetPosition(const glm::vec3 &position) override;

	const glm::vec3 &GetColor() const override { return color; }
	void SetColor(const glm::vec3 &color) override;

	void MarkDirty() { dirty.fill(true); }

	uint32_t lightIndex = 0;

	glm::vec3 position = { 0.0f, 0.0f, 0.0f };
	glm::vec3 color = { 1.0f, 1.0f, 1.0f };

	std::array<bool, MAX_FRAMES_IN_FLIGHT> dirty;
};

class CLightStateVK : public ILightState
{
public:

	struct alignas(16) LightInfo
	{
		alignas(16) glm::u32 pointlightCount = 0;
		alignas(16) glm::vec3 ambientLightColor = { 0.1f, 0.1f, 0.1f };
	};

	// This structure mirrors how point lights are actually stored
	struct alignas(16) PointLight
	{
		alignas(16) glm::vec3 position = { 0.0f, 0.0f, 0.0f };
		alignas(16) glm::vec3 color = { 1.0f, 1.0f, 1.0f };
	};

	CLightStateVK()
	{
		dirty.fill(true);
	}

	void CreateBuffers();
	void DeleteBuffers();

	void UpdateBuffers(uint32_t frame);

	IPointLight *CreatePointLight() override;
	void DeletePointLight(IPointLight *pointLight) override;

	uint32_t GetPointLightCount() const override { return (uint32_t)pointLights.size(); }
	IPointLight *GetPointLight(uint32_t index) override { return pointLights[index].get(); }

	const glm::vec3 &GetAmbientLightColor() const override { return ambientLightColor; }
	void SetAmbientLightColor(const glm::vec3 &color) override;

	void MarkDirty() { dirty.fill(true); }

	glm::vec3 ambientLightColor = { 1.0f, 1.0f, 1.0f };

	std::array<bool, MAX_FRAMES_IN_FLIGHT> dirty;

	CShaderSystemVK *shaderSystem = nullptr;
	CShaderBuffer *lightInfoBuffer = nullptr;
	CShaderBuffer *pointLightBuffer = nullptr;

	std::vector<std::unique_ptr<CPointLightVK>> pointLights;
};
