#pragma once

#include "graphicsvk_definitions.h"
#include "graphics.h"
#include "queuefamily.h"
#include "view.h"
#include "guimaterial.h"

#include "vertexbuffervk.h"
#include "indexbuffervk.h"

#include <volk.h>
#include <vk_mem_alloc.h>
#include <memory>
#include <array>

struct FramebufferAttachment
{
	VmaAllocation imageAllocation = VK_NULL_HANDLE;
	VkImage image = VK_NULL_HANDLE;
	VkImageView imageView = VK_NULL_HANDLE;
	VkImageLayout imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
};

struct FramebufferAttachmentDescriptor
{
	VkDescriptorPool descriptorPool = VK_NULL_HANDLE;
	std::array<VkDescriptorSet, MAX_FRAMES_IN_FLIGHT> descriptorSets;
};

class CRenderWindow : public IWindow
{
public:
	static CRenderWindow *ToRenderWindow(IWindow *window)
	{
#if _DEBUG
		return dynamic_cast<CRenderWindow*>(window);
#else
		return static_cast<CRenderWindow*>(window);
#endif
	}

	uint32_t GetWidth() const override { return swapChainExtent.width; }
	uint32_t GetHeight() const override { return swapChainExtent.height; }
	void GetDimensions(uint32_t &width, uint32_t &height) const override { width = swapChainExtent.width; height = swapChainExtent.height; }

	ImGuiContext *GetGuiContext() override { return guiContext; }
	void NewGuiFrame(float dt) override;

	float GetHorizontalAspectRatio() const { return horizontalAspectRatio; }
	float GetVerticalAspectRatio() const { return verticalAspectRatio; }
	void RegisterOnClosed(std::function<void(IWindow *)> function) override { onClosedCallbacks.push_back(function); };

	void RegisterWindowResize(IWindowResize *resize) override { windowResizeCallbacks.emplace_back(resize); }
	void UnregisterWindowResize(IWindowResize *resize) override;

	SDL_Window *windowHandle = nullptr;
	uint32_t windowID = {};

	bool isMinimized = false;

	glm::mat4 orthoProjection = glm::mat4(1.0f);

	VkSurfaceKHR surface = VK_NULL_HANDLE;

	// Swap Chain
	VkSwapchainKHR swapChainKHR = VK_NULL_HANDLE;
	std::unique_ptr<VkImage[]> swapChainImages;
	std::unique_ptr<VkImageView[]> swapChainImageViews;
	std::unique_ptr<VkFramebuffer[]> swapChainFramebuffers;
	VkExtent2D swapChainExtent = {};
	uint32_t numSwapChainImages = 0;


	struct
	{
		FramebufferAttachment color;

		FramebufferAttachment accum;
		FramebufferAttachment revealage;
		FramebufferAttachment depth;

		FramebufferAttachment accumResolve;
		FramebufferAttachment revealageResolve;
		FramebufferAttachment depthResolve;
	} attachments;

	VkSemaphore imageAvailableSemaphores[ MAX_FRAMES_IN_FLIGHT ] = { VK_NULL_HANDLE };
	VkSemaphore renderFinishedSemaphores[ MAX_FRAMES_IN_FLIGHT ] = { VK_NULL_HANDLE };
	VkFence inFlightFences[ MAX_FRAMES_IN_FLIGHT ] = { VK_NULL_HANDLE };
	std::vector<VkFence> imagesInFlight;

	struct
	{
		FramebufferAttachmentDescriptor attachments;
	} transparentSubpass;

	struct
	{
		FramebufferAttachmentDescriptor attachments;
	} transparentCompositionSubpass;

	std::vector<std::function<void(IWindow *)>> onClosedCallbacks;
	std::vector<IWindowResize*> windowResizeCallbacks;

	float horizontalAspectRatio = 0.0f;
	float verticalAspectRatio = 0.0f;

	ImGuiContext *guiContext = ImGui::CreateContext();
	std::vector<std::unique_ptr<CGuiMaterial>> guiMaterials;

	CGuiMaterial *fontMaterial = nullptr;

	CDynamicVertexBuffer *guiVertexBuffer = nullptr;
	CDynamicIndexBuffer *guiIndexBuffer = nullptr;
};