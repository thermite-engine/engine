#pragma once

#include <cstdint>
#include <string>
#include <unordered_map>
#include <memory>
#include <array>

#include "materialvk.h"
#include "texturevk.h"
#include "shaderbuffer.h"
#include "graphicsvk_definitions.h"

class CMaterialInstanceVK
{
	struct Sampler
	{
		void MarkDirty()
		{
			dirty.fill(true);
		}

		ITexture *texture = nullptr;
		std::array<bool, MAX_FRAMES_IN_FLIGHT> dirty;
	};

	struct ShaderBuffer
	{
		void MarkDirty()
		{
			dirty.fill(true);
		}

		CShaderBuffer *buffer = nullptr;
		std::array<bool, MAX_FRAMES_IN_FLIGHT> dirty;
	};

public:

	CMaterialInstanceVK()
	{
		descriptorSets.fill(VK_NULL_HANDLE);
	}

	CShaderBuffer *GetBuffer(uint32_t binding) { return shaderBuffers[binding].buffer; }
	ITexture *GetSampler(uint32_t binding) { return samplers[binding].texture; }

	void SetBuffer(uint32_t binding, CShaderBuffer *shaderBuffer);
	void SetSampler(uint32_t binding, ITexture *texture);

	void MarkBufferDirty(CShaderBuffer *buffer);
	void UpdateDirtyDescriptorSets(uint32_t frame);

	CGraphicsVK *graphics = nullptr;
	CMaterialVK *material = nullptr;

	VkDescriptorPool descriptorPool = VK_NULL_HANDLE;
	std::array<VkDescriptorSet, MAX_FRAMES_IN_FLIGHT> descriptorSets;

	std::unordered_map<uint32_t, ShaderBuffer> shaderBuffers;
	std::unordered_map<uint32_t, Sampler> samplers;

	bool opaque = true;
};