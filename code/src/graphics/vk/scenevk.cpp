#include "scenevk.h"
#include "log.h"
#include "renderervk_impl.h"
#include "graphicsvk.h"
#include "shadersystem.h"
#include "materialinstance.h"
#include "shadervk.h"

#include "materialcachevk.h"
#include "spriteloadervk.h"

#include <cmath>

#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"

void CSceneVK::SetSkyboxTexture(ITexture *texture)
{
	if (!texture && skyboxMesh)
	{
		if (skyboxMesh->materialInstance->descriptorPool)
			graphics->DestroyDescriptorPool(skyboxMesh->materialInstance->descriptorPool);

		graphics->DestroyVertexBuffer(skyboxMesh->vertexBuffer);
		graphics->DestroyIndexBuffer(skyboxMesh->indexBuffer);
		skyboxMesh.reset();
	}
	else if (texture && !skyboxMesh)
	{
		std::vector<SkyVertex> skyVertices = {
			{ { -1, 1, 1 } },
			{ { 1, 1, -1 } },
			{ { 1, 1, 1 } },
			{ { 1, 1, -1 } },
			{ { -1, -1, -1 } },
			{ { 1, -1, -1 } },
			{ { -1, 1, -1 } },
			{ { -1, -1, 1 } },
			{ { -1, -1, -1 } },
			{ { 1, -1, 1 } },
			{ { -1, -1, -1 } },
			{ { -1, -1, 1 } },
			{ { 1, 1, 1 } },
			{ { 1, -1, -1 } },
			{ { 1, -1, 1 } },
			{ { -1, 1, 1 } },
			{ { 1, -1, 1 } },
			{ { -1, -1, 1 } },
			{ { -1, 1, -1 } },
			{ { -1, 1, -1 } },
			{ { -1, 1, 1 } },
			{ { 1, -1, -1 } },
			{ { 1, 1, -1 } },
			{ { 1, 1, 1 } }
		};

		std::vector<uint32_t> skyIndices = {
			0, 1, 2,
			3, 4, 5,
			6, 7, 8,
			9, 10, 11,
			12, 13, 14,
			15, 16, 17,
			0, 18, 1,
			3, 19, 4,
			6, 20, 7,
			9, 21, 10,
			12, 22, 13,
			15, 23, 16
		};

		skyboxMesh = std::make_unique<CMeshVK>();
		skyboxMesh->vertexBuffer = graphics->CreateVertexBuffer(sizeof(SkyVertex), skyVertices.size(), skyVertices.data());
		skyboxMesh->indexBuffer = graphics->CreateIndexBuffer(sizeof(uint32_t), skyIndices.size(), skyIndices.data());

		if (!skyboxMaterial)
		{
			skyboxMaterial = materialCache->CreateMaterial();
			skyboxMaterial->SetShader(graphics->shaderSystem->shaders.skybox);
		}

		skyboxMesh->materialInstance = std::make_unique<CMaterialInstanceVK>();
		CMaterialInstanceVK *skyboxMaterialInstance = skyboxMesh->materialInstance.get();
		skyboxMaterialInstance->graphics = graphics;
		skyboxMaterialInstance->material = skyboxMaterial;
		skyboxMaterialInstance->SetSampler(0, texture);

		skyboxMaterial->GetShader()->InitMaterialInstance(textureCache, &lightState, skyboxMaterialInstance);
		graphics->SetupMaterialInstance(skyboxMaterialInstance);
	}
	else if (texture && skyboxMesh)
		skyboxMesh->materialInstance->SetSampler(0, texture);
}

IBillboardSprite *CSceneVK::LoadBillboardSprite(const std::filesystem::path &spritePath)
{
	return spriteLoader->LoadBillboardSprite(spritePath);
}

void CSceneVK::DrawModel(IModel *baseModel, const Transform &transform)
{
	CModelVK *model = CModelVK::ToModelVK(baseModel);
	for (auto &mesh : model->meshes)
	{
		if (!mesh->vertexBuffer)
			continue;

		DrawCmdBase &draw = drawList.emplace_back();
		draw.transform = transform;

		// TODO: We might want to change this so we take in the Vulkan buffers
		// This way we if somebody destroys the model after calling us, we don't explode.
		draw.materialInstance = mesh->materialInstance.get();

		draw.vertexBuffer = mesh->vertexBuffer->buffer;
		draw.vertexCount = mesh->vertexBuffer->vertexCount;

		if (mesh->indexBuffer)
		{
			draw.indexBuffer = mesh->indexBuffer->buffer;
			draw.indexCount = mesh->indexBuffer->indexCount;
		}
	}
}

void CSceneVK::DrawBillboardSprite(IBillboardSprite *baseSprite, const Transform &transform)
{
	CBillboardSpriteVK *sprite = CBillboardSpriteVK::ToBillboardSpriteVK(baseSprite);

	if (!sprite->vertexBuffer)
		return;

	DrawCmdBillboard &draw = billboardDrawList.emplace_back();
	draw.transform = transform;

	// TODO: We might want to change this so we take in the Vulkan buffers
	// This way we if somebody destroys the model after calling us, we don't explode.
	draw.materialInstance = sprite->materialInstance.get();

	draw.vertexBuffer = sprite->vertexBuffer->buffer;
	draw.vertexCount = sprite->vertexBuffer->vertexCount;

	if (sprite->indexBuffer)
	{
		draw.indexBuffer = sprite->indexBuffer->buffer;
		draw.indexCount = sprite->indexBuffer->indexCount;
	}

	draw.color = baseSprite->GetColor();
}

void CSceneVK::ClearDrawLists()
{
	drawList.clear();
	billboardDrawList.clear();
}

void CSceneVK::LoadDefaultResources()
{
	textureCache = new CTextureCacheVK;
	textureCache->graphics = graphics;
	textureCache->owner = this;

	materialCache = new CMaterialCacheVK;
	materialCache->graphics = graphics;
	materialCache->owner = this;

	modelCache = new CModelCacheVK;
	modelCache->graphics = graphics;
	modelCache->owner = this;
	modelCache->lightState = &lightState;

	spriteLoader = new CSpriteLoaderVK;
	spriteLoader->graphics = graphics;
	spriteLoader->textureCache = textureCache;
	spriteLoader->materialCache = materialCache;

	lightState.shaderSystem = graphics->shaderSystem.get();
	lightState.CreateBuffers();

	textureCache->LoadFallbackTextures();
	materialCache->LoadResources();
	modelCache->LoadResources();
}

void CSceneVK::UnloadResources()
{
	spriteLoader->UnloadResources();
	modelCache->UnloadResources();
	materialCache->UnloadResources();
	textureCache->UnloadTextures();

	// TODO: Make this better
	delete spriteLoader;
	delete modelCache;
	delete materialCache;
	delete textureCache;

	spriteLoader = nullptr;
	modelCache = nullptr;
	materialCache = nullptr;
	textureCache = nullptr;

	lightState.DeleteBuffers();

	if (skyboxMesh)
	{
		for (auto &[binding, sb] : skyboxMesh->materialInstance.get()->shaderBuffers)
		{
			graphics->shaderSystem->DeleteShaderBuffer(sb.buffer);
			sb.buffer = nullptr;
		}
		if (skyboxMesh->materialInstance->descriptorPool)
			graphics->DestroyDescriptorPool(skyboxMesh->materialInstance->descriptorPool);

		graphics->DestroyVertexBuffer(skyboxMesh->vertexBuffer);
		graphics->DestroyIndexBuffer(skyboxMesh->indexBuffer);
		skyboxMesh.reset();
	}
}