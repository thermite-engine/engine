#pragma once

#include "mesh.h"
#include "modelvk.h"
#include "texturevk.h"
#include "materialvk.h"
#include "shaderbuffer.h"
#include "scene.h"
#include "texturecachevk.h"
#include "resourceownervk.h"

#include "modelcachevk.h"
#include "lightvk.h"
#include "drawinfo.h"

#include <memory>
#include <vector>
#include <unordered_map>
#include <filesystem>

class CGraphicsVK;

class CSceneVK : public CResourceOwnerVK, public IScene
{
public:

	ILightState *GetLightState() override { return &lightState; }

	void SetSkyboxTexture(ITexture *texture) override;
	ITextureCache *GetTextureCache() override { return textureCache; }
	IModelCache *GetModelCache() override { return modelCache; }

	IBillboardSprite *LoadBillboardSprite(const std::filesystem::path &spritePath) override;
	void DrawModel(IModel *baseModel, const Transform &transform) override;
	void DrawBillboardSprite(IBillboardSprite *baseSprite, const Transform &transform) override;
	void ClearDrawLists() override;

	void LoadDefaultResources();
	void UnloadResources();

	static CSceneVK *ToSceneVK(IScene *baseScene)
	{
#ifdef _DEBUG
		return dynamic_cast<CSceneVK*>(baseScene);
#else
		return static_cast<CSceneVK*>(baseScene);
#endif
	}

	CGraphicsVK *graphics = nullptr;

	std::vector<DrawCmdBase> drawList;
	std::vector<DrawCmdBillboard> billboardDrawList;

	std::unique_ptr<CMeshVK> skyboxMesh;
	CMaterialVK *skyboxMaterial = nullptr;

	CLightStateVK lightState;
};