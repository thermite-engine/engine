#pragma once

#include "service.h"
#include "graphics.h"
#include "renderwindow.h"
#include "graphicsvk_definitions.h"
#include "renderervk_impl.h"
#include "shadersystem.h"
#include "input/input.h"
#include "vertexbuffervk.h"
#include "indexbuffervk.h"
#include "texturevk.h"
#include "texturecachevk.h"
#include "guimaterial.h"
#include "resourceownervk.h"

#include <SDL2/SDL.h>
#include <volk.h>
#include <vk_mem_alloc.h>
#include <optional>
#include <memory>

class CMaterialInstanceVK;

class CGraphicsVK : public IGraphics, public IWindowEventListener, public CResourceOwnerVK, public CService<>
{
#if VK_DEBUG
	VkResult CreateDebugUtilsMessengerEXT(const VkDebugUtilsMessengerCreateInfoEXT *pCreateInfo, const VkAllocationCallbacks *pAllocator);
	void DestroyDebugUtilsMessengerEXT(VkAllocationCallbacks *pAllocator);
#endif

	bool IsPhysicalDeviceSuitable(CRenderWindow *renderWindow, VkPhysicalDevice device);

public:
	using CService<>::CService;
	~CGraphicsVK();

	void Init(CRenderWindow *renderWindow);

	bool IsMSAAEnabled() const { return currentMSAASamples != VK_SAMPLE_COUNT_1_BIT; }
	void ToggleMSAA() override;
	ITextureCache *GetTextureCache() override { return textureCache; }
	void Update(uint64_t) override;

	void OnWindowResize(CRenderWindow *renderWindow);
	void OnWindowEvent(const SDL_WindowEvent &windowEvent) override;

	IWindow *CreateWindow(const std::string_view &title, int width, int height) override;
	IWindow *CreateWindowFullscreen(const std::string_view &title) override;

	void SetupWindow(CRenderWindow *window);

	void DeleteWindow(IWindow *window) override;

	IRenderer *CreateRenderer(IWindow *window) override;
	void DeleteRenderer(IRenderer *renderer) override;

	void DestroySwapChain(CRenderWindow *renderWindow);

private:

	void LoadExtensions(SDL_Window *window);
	void CheckValidationLayerSupport();
	void CreateInstance();
	void SetupDebugCallback();

	void CreateSurface(CRenderWindow *renderWindow);
	void PickPhysicalDevice(CRenderWindow *renderWindow);
	void GetPhysicalDeviceLimits();
	void CreateLogicalDevice();
	void CreateVmaAllocator();
	void CreateCommandPools();
	void CreateSwapChain(CRenderWindow *renderWindow);
	void CreateSwapChainImageViews(CRenderWindow *renderWindow);
	void FindDepthFormat();
	void CreateRenderPass();
	void CreateRenderPassMSAA();
	void CreateColorResources(CRenderWindow *renderWindow);
	void CreateAccumResources(CRenderWindow *renderWindow);
	void CreateRevealageResources(CRenderWindow *renderWindow);
	void CreateDepthResources(CRenderWindow *renderWindow);
	void CreateFramebuffers(CRenderWindow *renderWindow);
	void CreateSyncObjects(CRenderWindow *renderWindow);
	void CreateFramebufferDescriptorSetLayouts();
	void CreateFramebufferDescriptorSets(CRenderWindow *renderWindow);
	void SetupImGuiContext(CRenderWindow *renderWindow);

public:

	// Helper functions
	FramebufferAttachment CreateFramebufferAttachment(CRenderWindow *renderWindow, VkFormat format, VkImageUsageFlags imageUsage, VkImageAspectFlags aspectFlags, VkSampleCountFlagBits numSamples);
	void DestroyFramebufferAttachment(FramebufferAttachment &attachment);

	FramebufferAttachmentDescriptor CreateFramebufferAttachmentDescriptor(std::vector<FramebufferAttachment*> attachments, VkDescriptorSetLayout descriptorSetLayout);
	void DestroyFramebufferAttachmentDescriptor(FramebufferAttachmentDescriptor &descriptor);

	VkCommandBuffer BeginSingleTimeCommands();
	void EndSingleTimeCommands(VkCommandBuffer &commandBuffer);
	VkImage CreateImage(uint32_t arrayLayers, VkImageType imageType, uint32_t width, uint32_t height, uint32_t mipLevels, VkSampleCountFlagBits numSamples, VkFormat format, VkImageTiling tiling, VkImageUsageFlags imageUsage, VmaMemoryUsage memoryUsage, VmaAllocation &allocation);
	VkImage CreateImage2D(uint32_t width, uint32_t height, uint32_t mipLevels, VkSampleCountFlagBits numSamples, VkFormat format, VkImageTiling tiling, VkImageUsageFlags imageUsage, VmaMemoryUsage memoryUsage, VmaAllocation &allocation);
	VkImageView CreateImageView(uint32_t layerCount, VkImageViewType imageViewType, VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t mipLevels);
	VkImageView CreateImageView2D(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t mipLevels);
	void CopyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height, uint32_t faceCount = 1);
	void TransitionImageLayout(VkCommandBuffer commandBuffer, VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout, uint32_t mipLevels, uint32_t layerCount = 1);
	void TransitionImageLayoutExplicit(VkCommandBuffer commandBuffer, const VkImageMemoryBarrier &barrier, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask);
	void TransitionImageLayoutExplicitImmediate(const VkImageMemoryBarrier &barrier, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask);
	void TransitionImageLayoutImmediate(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout, uint32_t mipLevels, uint32_t layerCount = 1);
	void CreateBuffer(VkDeviceSize size, VkBufferUsageFlags bufferUsage, VmaMemoryUsage memoryUsage, VkBuffer &buffer, VmaAllocation &allocation);
	void DeleteBuffer(VkBuffer buffer, VmaAllocation allocation);
	void DeleteBufferImmediate(VkBuffer buffer, VmaAllocation allocation);
	void CopyBuffer(const VkBuffer &srcBuffer, VkBuffer &dstBuffer, VkDeviceSize size);
	void GenerateMipMaps(VkImage image, VkFormat format, uint32_t width, uint32_t height, uint32_t mipLevels);

	void UpdateDirtyBuffers(uint32_t frame);

	void DestroyStaticBuffer(CStaticBufferVK *buffer);
	void DestroyDynamicBuffer(CDynamicBufferVK *dynamicBuffer);
	void DestroyDescriptorPool(VkDescriptorPool descriptorPool);

	CDynamicVertexBuffer *CreateDynamicVertexBuffer(VkDeviceSize vertexSize, VkDeviceSize vertexCount, std::optional<const void*> vertexData);
	void DestroyDynamicVertexBuffer(CDynamicVertexBuffer *vertexBuffer) { DestroyDynamicBuffer(vertexBuffer); }

	CVertexBuffer *CreateVertexBuffer(VkDeviceSize vertexSize, VkDeviceSize vertexCount, const void *vertexData);
	void DestroyVertexBuffer(CVertexBuffer *vertexBuffer) { DestroyStaticBuffer(vertexBuffer); }

	CDynamicIndexBuffer *CreateDynamicIndexBuffer(VkDeviceSize indexSize, VkDeviceSize indexCount, std::optional<const void*> indexData);
	void DestroyDynamicIndexBuffer(CDynamicIndexBuffer *indexBuffer) { DestroyDynamicBuffer(indexBuffer); }

	CIndexBuffer *CreateIndexBuffer(VkDeviceSize indexSize, VkDeviceSize indexCount, const void *indexData);
	void DestroyIndexBuffer(CIndexBuffer *indexBuffer) { DestroyStaticBuffer(indexBuffer); }

	CTextureVK *CreateTexture(bool generateMipMaps, uint32_t width, uint32_t height, const void *pixelData, VkFormat format, VkImageType imageType, VkImageViewType imageViewType, VkSamplerCreateInfo samplerInfo);
	void DestroyTexture(CTextureVK *texture);

	CGuiMaterial *CreateGuiMaterial(CRenderWindow *renderWindow, const void *image, int width, int height);
	void DestroyGuiMaterial(CRenderWindow *renderWindow, CGuiMaterial *material);

	void SetupMaterialInstance(CMaterialInstanceVK *materialInstance);

	std::vector<const char*> requiredExtensions;
    std::vector<const char*> deviceExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME, VK_KHR_MAINTENANCE1_EXTENSION_NAME };
	std::vector<const char*> validationLayers = { "VK_LAYER_KHRONOS_validation" };

	VkDebugUtilsMessengerEXT debugCallback = VK_NULL_HANDLE;
	VkInstance instance = VK_NULL_HANDLE;
	VkDevice device = VK_NULL_HANDLE;
	VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
	VmaAllocator allocator = VK_NULL_HANDLE;
	VkRenderPass renderPass = VK_NULL_HANDLE;
	VkCommandPool commandPool = VK_NULL_HANDLE;
	VkCommandPool computeCommandPool = VK_NULL_HANDLE;
	QueueFamilyIndices queueFamilyIndices;
	VkQueue graphicsQueue = VK_NULL_HANDLE;
	VkQueue presentQueue = VK_NULL_HANDLE;
	VkQueue computeQueue = VK_NULL_HANDLE;

	VkFormat swapChainImageFormat = VK_FORMAT_UNDEFINED;
	VkFormat depthFormat = VK_FORMAT_UNDEFINED;

	VkSampleCountFlagBits maxMSAASamples = VK_SAMPLE_COUNT_1_BIT;
	VkSampleCountFlagBits currentMSAASamples = VK_SAMPLE_COUNT_1_BIT;

	// This is kinda crap, but oh well
	struct
	{
		VkDescriptorSetLayout depthOnly = VK_NULL_HANDLE; // Used by transparent subpass shaders
		VkDescriptorSetLayout accumRevealage = VK_NULL_HANDLE; // Used by composition subpass
	} descriptorSetLayouts;

	std::unique_ptr<CShaderSystemVK> shaderSystem;

	IInput *input = Get<IInput>();

private:
	struct BufferDestroy
	{
		VkBuffer buffer = VK_NULL_HANDLE;
		VmaAllocation allocation = VK_NULL_HANDLE;
		uint32_t destroyFrame = MAX_FRAMES_IN_FLIGHT;
	};

	struct TextureDestroy
	{
		VkImage image = VK_NULL_HANDLE;
		VmaAllocation allocation = VK_NULL_HANDLE;
		VkImageView imageView = VK_NULL_HANDLE;
		VkSampler sampler = VK_NULL_HANDLE;
		uint32_t destroyFrame = MAX_FRAMES_IN_FLIGHT;
	};

	struct DescriptorPoolDestroy
	{
		VkDescriptorPool descriptorPool = VK_NULL_HANDLE;
		uint32_t destroyFrame = MAX_FRAMES_IN_FLIGHT;
	};

	// There are some things that we need to initialize upon window creation to do anything
	bool isInitialized = false;

	std::unordered_map<uint32_t, std::unique_ptr<CRenderWindow>> renderWindows;
	std::vector<std::unique_ptr<CRendererVK>> renderers;

	// TODO: Need to somehow store per-window dynamic buffers.
	std::unordered_map<CDynamicBufferVK*, std::unique_ptr<CDynamicBufferVK>> dynamicBuffers;
	std::unordered_map<CStaticBufferVK*, std::unique_ptr<CStaticBufferVK>> staticBuffers;
	std::unordered_map<CTextureVK*, std::unique_ptr<CTextureVK>> staticTextures;

	std::vector<BufferDestroy> bufferDestroyList;
	std::vector<TextureDestroy> textureDestroyList;
	std::vector<DescriptorPoolDestroy> descriptorPoolDestroyList;
};