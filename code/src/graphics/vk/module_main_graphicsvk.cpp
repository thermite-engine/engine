#include "log/log.h"
#include "graphicsvk.h"
#include "input/input.h"

#include "ui/the_imgui.h"

extern "C" THR_EXPORT void ModuleInit(CServer::CServerInstance *instance)
{
	CServer::Init(instance);
	Log::Init(instance->Get<ILogger>(), "GraphicsVK", fmt::color::cornflower_blue);

	instance->MakeService<CGraphicsVK>(instance->GetServer(), "GraphicsVK");

	ThermiteImGui::Init(instance->GetServer());
}

extern "C" THR_EXPORT void ModuleShutdown()
{
	ThermiteImGui::Shutdown();
    Log::Print("Shutting down...\n");
}