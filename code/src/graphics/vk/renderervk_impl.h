#pragma once

#include "renderer.h"
#include "renderwindow.h"
#include "scenevk.h"
#include "shadersystem.h"
#include "drawinfo.h"

#include <volk.h>
#include <vector>

class CGraphicsVK;

class CRendererVK : public IRenderer
{
public:

	IWindow *GetWindow() override { return renderWindow; }

	IScene *CreateScene() override;
	void DeleteScene(IScene *baseScene) override;

	void SetClearColor(float red, float green, float blue, float alpha) override { clearColorValue = { { red, green, blue, alpha } }; }
	void GetClearColor(float *red, float *green, float *blue, float *alpha) override;

	void BeginFrame() override;
	void DrawFrame() override;
	void DrawScene(IScene *baseScene, const CView3D &view3D, const glm::mat4 &viewMatrix) override;
	void DrawUI(ImGuiContext *ctx) override;
	void EndFrame() override;
	void ClearDrawLists() override;

	CGraphicsVK *graphics = nullptr;
	CRenderWindow *renderWindow = nullptr;

	std::vector<SceneDraw> sceneDraws;
	std::vector<UiDraw> uiDraws;

	std::vector<VkCommandBuffer> commandBuffers;
	uint32_t imageIndex = 0;
	uint32_t currentFrame = 0;

	std::vector<std::unique_ptr<CSceneVK>> scenes;

	VkClearColorValue clearColorValue = { { 0.0f, 0.0f, 0.0f, 1.0f } };

	bool isReadyToDraw = false;
};