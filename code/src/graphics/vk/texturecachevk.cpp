#include "texturecachevk.h"
#include "log.h"
#include "stb/stb_image.h"
#include "graphicsvk.h"

#include <array>
#include <volk.h>

// TODO: MOVE AND COMPLETE THIS!!!
constexpr VkFormat ToVkFormat(EImageFormat imageFormat)
{
	switch (imageFormat)
	{
	case EImageFormat::UNDEFINED:
		return VK_FORMAT_UNDEFINED;
	case EImageFormat::R8G8B8_UNORM:
		return VK_FORMAT_R8G8B8_UNORM;
	case EImageFormat::R8G8B8A8_UNORM:
		return VK_FORMAT_R8G8B8A8_UNORM;
	default:
		return VK_FORMAT_UNDEFINED;
	}
}

constexpr VkImageType ToVkImageType(EImageType imageType)
{
	switch (imageType)
	{
	case EImageType::TYPE_2D:
		return VK_IMAGE_TYPE_2D;
	case EImageType::TYPE_CUBE:
		return VK_IMAGE_TYPE_2D;
	default:
		throw std::runtime_error("Invalid image type!");
	}
}

constexpr VkImageViewType ToVkImageViewType(EImageType imageType)
{
	switch (imageType)
	{
	case EImageType::TYPE_2D:
		return VK_IMAGE_VIEW_TYPE_2D;
	case EImageType::TYPE_CUBE:
		return VK_IMAGE_VIEW_TYPE_CUBE;
	default:
		throw std::runtime_error("Invalid image type!");
	}
}

void CTextureCacheVK::LoadFallbackTextures()
{
	const std::array<uint8_t, 4> white = { 0xFF, 0xFF, 0xFF, 0xFF };
	const std::array<uint8_t, 4> normal_flat = { 0x80, 0x80, 0xFF, 0xFF };

	textureWhite = CreateTexture(EImageType::TYPE_2D, EImageFormat::R8G8B8A8_UNORM, 1, 1, white.data(), false);

	textureDiffuseError2D = LoadTexture("textures/error.png", ETextureFallback::Diffuse_Error2D, false);
	textureDiffuseErrorCube = LoadSkybox(
				{
					"textures/error.png",
					"textures/error.png",
					"textures/error.png",
					"textures/error.png",
					"textures/error.png",
					"textures/error.png",
				});

	textureNormalFlat = CreateTexture(EImageType::TYPE_2D, EImageFormat::R8G8B8A8_UNORM, 1, 1, normal_flat.data(), false);

	if (!textureWhite || !textureDiffuseError2D || !textureDiffuseErrorCube || !textureNormalFlat)
		throw std::runtime_error("Failed to load default error texture!");
}

void CTextureCacheVK::UnloadTextures()
{
	for (auto &[id, texture] : textures)
		graphics->DestroyTexture(CTextureVK::ToTextureVK(texture));

	for (auto &texture : createdTextures)
		graphics->DestroyTexture(CTextureVK::ToTextureVK(texture));

	textures.clear();
	createdTextures.clear();
}

ITexture *CTextureCacheVK::LoadSprite(const std::filesystem::path &path, ETextureFallback fallback, bool generateMipMaps)
{
	if (path.empty())
		return GetFallbackTexture(fallback);

	// TODO: Better caching
	if (auto it = textures.find(path.generic_string()); it != textures.end())
		return it->second;
	
	Log::Print("Loading sprite: {}\n", path.generic_string());

	int width = 0;
	int height = 0;
	int numComponents = 0;

	stbi_uc *pixels = stbi_load(path.string().c_str(), &width, &height, &numComponents, STBI_rgb_alpha);

	if (!pixels)
	{
		Log::Print("Failed to load texture {}\n", path.string());
		return GetFallbackTexture(fallback);
	}

	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.minFilter = VK_FILTER_LINEAR;
	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerInfo.anisotropyEnable = VK_TRUE;
	samplerInfo.maxAnisotropy = 16.0f;
	samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
	samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
	samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;

	CTextureVK *texture = graphics->CreateTexture(generateMipMaps, (uint32_t)width, (uint32_t)height, pixels, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TYPE_2D, VK_IMAGE_VIEW_TYPE_2D, samplerInfo);
	texture->opaque = (numComponents < 4);

	stbi_image_free(pixels);
	textures[path.generic_string()] = texture;

	return texture;
}

ITexture *CTextureCacheVK::LoadTexture(const std::filesystem::path &path, ETextureFallback fallback, bool generateMipMaps)
{
	if (path.empty())
		return GetFallbackTexture(fallback);

	// TODO: Better caching
	if (auto it = textures.find(path.generic_string()); it != textures.end())
		return it->second;

	Log::Print("Loading texture: {}\n", path.generic_string());

	int width = 0;
	int height = 0;
	int numComponents = 0;

	stbi_uc *pixels = stbi_load(path.string().c_str(), &width, &height, &numComponents, STBI_rgb_alpha);

	if (!pixels)
	{
		Log::Print("Failed to load texture {}\n", path.string());
		return GetFallbackTexture(fallback);
	}

	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.minFilter = VK_FILTER_LINEAR;
	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerInfo.anisotropyEnable = VK_TRUE;
	samplerInfo.maxAnisotropy = 16.0f;

	CTextureVK *texture = graphics->CreateTexture(generateMipMaps, (uint32_t)width, (uint32_t)height, pixels, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TYPE_2D, VK_IMAGE_VIEW_TYPE_2D, samplerInfo);
	texture->opaque = (numComponents < 4);

	stbi_image_free(pixels);
	textures[path.generic_string()] = texture;

	return texture;
}

void CTextureCacheVK::UnloadTexture(ITexture *texture)
{
	for (auto it = textures.begin(); it != textures.end(); ++it)
	{
		if (it->second == texture)
		{
			graphics->DestroyTexture(CTextureVK::ToTextureVK(texture));
			textures.erase(it);
			return;
		}
	}

	if (auto it = std::find(createdTextures.begin(), createdTextures.end(), texture); it != createdTextures.end())
	{
		graphics->DestroyTexture(CTextureVK::ToTextureVK(texture));
		createdTextures.erase(it);
	}
}

ITexture *CTextureCacheVK::LoadSkybox(const std::filesystem::path &pathNoExtension, const std::filesystem::path &extension)
{
	if (pathNoExtension.empty() || extension.empty())
		return GetFallbackTexture(ETextureFallback::Diffuse_ErrorCube);

	// TODO: Better caching
	if (auto it = textures.find(pathNoExtension.generic_string()); it != textures.end())
		return it->second;

	Log::Print("Loading skybox texture: {}\n", pathNoExtension.generic_string());

	std::array<std::filesystem::path, 6> cubePath = {
		pathNoExtension,
		pathNoExtension,
		pathNoExtension,
		pathNoExtension,
		pathNoExtension,
		pathNoExtension
	};

	cubePath[0] += "ft";
	cubePath[1] += "bk";
	cubePath[2] += "up";
	cubePath[3] += "dn";
	cubePath[4] += "rt";
	cubePath[5] += "lf";

	int width = 0;
	int height = 0;
	int numComponents = 0;
	std::vector<unsigned char> pixelData;

	for (auto &path : cubePath)
	{
		path += extension;

		stbi_uc *pixels = stbi_load(path.generic_string().c_str(), &width, &height, &numComponents, STBI_rgb_alpha);

		if (!pixels)
			return GetFallbackTexture(ETextureFallback::Diffuse_ErrorCube);

		std::size_t imageSize = width * height * 4;
		std::size_t pixelDataStart = pixelData.size();

		pixelData.resize(pixelDataStart + imageSize);
		std::memcpy(&pixelData[pixelDataStart], pixels, imageSize);

		stbi_image_free(pixels);
	}

	if (pixelData.size() != (std::size_t)(width * height * 4 * 6))
		return GetFallbackTexture(ETextureFallback::Diffuse_ErrorCube);

	// Create Texture Sampler
	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.minFilter = VK_FILTER_LINEAR;

	CTextureVK *texture = graphics->CreateTexture(false, (uint32_t)width, (uint32_t)height, pixelData.data(), VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TYPE_2D, VK_IMAGE_VIEW_TYPE_CUBE, samplerInfo);
	texture->opaque = (numComponents < 4);

	textures[pathNoExtension.generic_string()] = texture;

	return texture;
}

ITexture *CTextureCacheVK::LoadSkybox(const std::array<std::filesystem::path, 6> facePaths)
{
	for (auto &path : facePaths)
	{
		if (path.empty())
			return GetFallbackTexture(ETextureFallback::Diffuse_ErrorCube);
	}

	std::filesystem::path pathNoExtension = facePaths[0];
	pathNoExtension.replace_extension();

	// TODO: Better caching, this is some hacky bullshit
	if (auto it = textures.find(pathNoExtension.generic_string()); it != textures.end())
		return it->second;

	Log::Print("Loading skybox texture: {}\n", pathNoExtension.generic_string());

	int width = 0;
	int height = 0;
	int numComponents = 0;
	std::vector<unsigned char> pixelData;

	for (auto &path : facePaths)
	{
		stbi_uc *pixels = stbi_load(path.generic_string().c_str(), &width, &height, &numComponents, STBI_rgb_alpha);

		if (!pixels)
			return GetFallbackTexture(ETextureFallback::Diffuse_ErrorCube);

		std::size_t imageSize = width * height * 4;
		std::size_t pixelDataStart = pixelData.size();

		pixelData.resize(pixelDataStart + imageSize);
		std::memcpy(&pixelData[pixelDataStart], pixels, imageSize);

		stbi_image_free(pixels);
	}

	if (pixelData.size() != (std::size_t)(width * height * 4 * 6))
		return GetFallbackTexture(ETextureFallback::Diffuse_ErrorCube);

	// Create Texture Sampler
	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.minFilter = VK_FILTER_LINEAR;

	CTextureVK *texture = graphics->CreateTexture(false, (uint32_t)width, (uint32_t)height, pixelData.data(), VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TYPE_2D, VK_IMAGE_VIEW_TYPE_CUBE, samplerInfo);
	texture->opaque = (numComponents < 4);

	textures[pathNoExtension.generic_string()] = texture;

	return texture;
}

ITexture *CTextureCacheVK::CreateTexture(EImageType imageType, EImageFormat imageFormat, uint32_t width, uint32_t height, const void *pixels, bool generateMipMaps)
{
	if (!pixels)
		throw std::runtime_error("pixels was nullptr!");

	VkFormat format = ToVkFormat(imageFormat);

	// TODO: Support other formats :trollface:
	if (format != VK_FORMAT_R8G8B8A8_UNORM)
		throw std::runtime_error("Unsupported format!");

	VkImageType vkImageType = ToVkImageType(imageType);
	VkImageViewType vkImageViewType = ToVkImageViewType(imageType);

	// Create Texture Sampler
	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.minFilter = VK_FILTER_LINEAR;
	samplerInfo.anisotropyEnable = VK_TRUE;
	samplerInfo.maxAnisotropy = 16.0f;
	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;

	bool opaque = true;
	const uint32_t byteCount = width * height * 4;
	const uint8_t *data = (const uint8_t*)pixels;
	for (uint32_t i = 3; i < byteCount; i += 4)
	{
		if (data[i] < 255)
		{
			opaque = false;
			break;
		}
	}

	CTextureVK *texture = graphics->CreateTexture(generateMipMaps, width, height, pixels, format, vkImageType, vkImageViewType, samplerInfo);
	texture->opaque = opaque;

	createdTextures.emplace_back(texture);

	return texture;
}

