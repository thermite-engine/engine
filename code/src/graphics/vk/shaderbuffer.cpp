#include "shaderbuffer.h"
#include "graphicsvk.h"
#include "materialinstance.h"

void CShaderBuffer::AddMaterialInstance(CMaterialInstanceVK *instance)
{
	materialInstances.insert(instance);
}
void CShaderBuffer::RemoveMaterialInstance(CMaterialInstanceVK *instance)
{
	materialInstances.erase(instance);
}

void CShaderBuffer::MarkMaterialInstancesDirty()
{
	for (CMaterialInstanceVK *instance : materialInstances)
		instance->MarkBufferDirty(this);
}

void CShaderBuffer::UpdateBuffer(uint32_t imageIndex, const void *src, std::size_t size)
{
	VmaAllocation &allocation = bufferAllocation[imageIndex];

	if (allocation != VK_NULL_HANDLE)
		std::memcpy(mapped[imageIndex], src, size);
}