#include "renderwindow.h"
#include "ui/the_imgui.h"

#include <SDL2/SDL_vulkan.h>

void CRenderWindow::NewGuiFrame(float dt)
{
	ThermiteImGui::SetCurrentContext(GetGuiContext());

	ImGuiIO &io = ImGui::GetIO();
	IM_ASSERT(io.Fonts->IsBuilt() && "Font atlas not built! It is generally built by the renderer back-end. Missing call to renderer _NewFrame() function? e.g. ImGui_ImplOpenGL3_NewFrame().");

	int w = GetWidth();
	int h = GetHeight();
	int display_w = 0;
	int display_h = 0;

	SDL_Vulkan_GetDrawableSize(windowHandle, &display_w, &display_h);
	io.DisplaySize = ImVec2((float)w, (float)h);

	if (w > 0 && h > 0)
		io.DisplayFramebufferScale = ImVec2((float)display_w / w, (float)display_h / h);

	io.DeltaTime = dt;
}

void CRenderWindow::UnregisterWindowResize(IWindowResize *resize)
{
	auto it = std::find_if(windowResizeCallbacks.begin(), windowResizeCallbacks.end(), [&](IWindowResize *find){ return (resize == find); });

	if (it != windowResizeCallbacks.end())
		windowResizeCallbacks.erase(it);
}