#include "materialcachevk.h"
#include "graphicsvk.h"

#include <cassert>

void CMaterialCacheVK::LoadResources()
{
	errorMaterial = CreateMaterial();

	errorMaterial->SetShader(graphics->shaderSystem->shaders.staticMesh);
	errorMaterial->SetTexture("diffuse", "textures/error.png");
}

void CMaterialCacheVK::UnloadResources()
{
	materials.clear();
	errorMaterial = nullptr;
}

CMaterialVK *CMaterialCacheVK::CreateMaterial()
{
	return materials.emplace_back(std::make_unique<CMaterialVK>()).get();
}

void CMaterialCacheVK::DeleteMaterial(CMaterialVK *material)
{
	// TODO: We probably really don't want the explicit deletion of materials
	// Maybe we should implement some kind of reference counting system or something

	assert(material != errorMaterial);
	auto material_it = std::find_if(materials.begin(), materials.end(), [&material](const std::unique_ptr<CMaterialVK> &materialHandle) { return (material == materialHandle.get()); });

	if (material_it != materials.end())
		materials.erase(material_it);
}