#pragma once

#include "shadervk.h"

class CShaderUI : public CShaderVK
{
	static constexpr uint32_t BINDING_DIFFUSE = 0;

public:
	void InitShader() override {}
	void InitMaterialInstance(CTextureCacheVK *textureCache, CLightStateVK *lightState, CMaterialInstanceVK *instance) override;
	void ShutdownMaterialInstance(CMaterialInstanceVK*) override {}

	void LoadShaderModules() override;
	void CreateGraphicsPipelineLayout() override;
	void CreateGraphicsPipeline() override;

	std::vector<VkDescriptorSetLayoutBinding> GetDescriptorSetLayoutBindings() const override
	{
		return
		{
			{
				// Diffuse sampler
				.binding = BINDING_DIFFUSE,
				.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
				.descriptorCount = 1,
				.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
				.pImmutableSamplers = nullptr
			}
		};
	}
};