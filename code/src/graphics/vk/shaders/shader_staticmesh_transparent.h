#pragma once

#include "shadervk.h"

class CShaderStaticMeshTransparent : public CShaderVK
{
	static constexpr uint32_t BINDING_DIFFUSE = 0;
	static constexpr uint32_t BINDING_NORMAL = 1;
	static constexpr uint32_t BINDING_LIGHTINFO = 2;
	static constexpr uint32_t BINDING_MVP = 3;
	static constexpr uint32_t BINDING_POINTLIGHTS = 4;
	static constexpr uint32_t BINDING_VIEWPOS = 5;
	static constexpr uint32_t BINDING_DEPTH = 6;

public:
	void InitShader() override;

	void LoadShaderModules() override;
	void CreateGraphicsPipelineLayout() override;
	void CreateGraphicsPipeline() override;

	std::vector<VkDescriptorSetLayoutBinding> GetDescriptorSetLayoutBindings() const override
	{
		return
		{
			{
				// Diffuse sampler
				.binding = BINDING_DIFFUSE,
				.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
				.descriptorCount = 1,
				.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
				.pImmutableSamplers = nullptr
			},
			{
				// Normal sampler
				.binding = BINDING_NORMAL,
				.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
				.descriptorCount = 1,
				.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
				.pImmutableSamplers = nullptr
			},
			{
				// LightInfo Uniform Buffer
				.binding = BINDING_LIGHTINFO,
				.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
				.descriptorCount = 1,
				.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
				.pImmutableSamplers = nullptr
			},
			{
				// MVP Storage buffer
				.binding = BINDING_MVP,
				.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
				.descriptorCount = 1,
				.stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
				.pImmutableSamplers = nullptr
			},
			{
				// Point Light storage buffer
				.binding = BINDING_POINTLIGHTS,
				.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
				.descriptorCount = 1,
				.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
				.pImmutableSamplers = nullptr
			},
			{
				// View Pos uniform buffer
				.binding = BINDING_VIEWPOS,
				.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
				.descriptorCount = 1,
				.stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
				.pImmutableSamplers = nullptr
			}
		};
	}
};