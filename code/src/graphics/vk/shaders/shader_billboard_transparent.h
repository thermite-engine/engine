#pragma once

#include "shadervk.h"

class CShaderBillboardTransparent : public CShaderVK
{
	static constexpr uint32_t BINDING_DIFFUSE = 0;
	static constexpr uint32_t BINDING_MVP = 1;

public:
	void InitShader() override {}

	void LoadShaderModules() override;
	void CreateGraphicsPipelineLayout() override;
	void CreateGraphicsPipeline() override;

	std::vector<VkDescriptorSetLayoutBinding> GetDescriptorSetLayoutBindings() const override
	{
		return
		{
			{
				// Diffuse sampler
				.binding = BINDING_DIFFUSE,
				.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
				.descriptorCount = 1,
				.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
				.pImmutableSamplers = nullptr
			},
			{
				// MVP Storage buffer
				.binding = BINDING_MVP,
				.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
				.descriptorCount = 1,
				.stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
				.pImmutableSamplers = nullptr
			}
		};
	}
};