#include "shader_staticmesh.h"
#include "materialinstance.h"
#include "shadersystem.h"
#include "renderervk_impl.h"
#include "graphicsvk.h"
#include "scenevk.h"
#include "log.h"
#include "texturecachevk.h"

#include <array>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

void CShaderStaticMesh::InitShader()
{
	
}

void CShaderStaticMesh::InitMaterialInstance(CTextureCacheVK *textureCache, CLightStateVK *lightState, CMaterialInstanceVK *instance)
{
	CMaterialVK *material = instance->material;

	const std::filesystem::path diffusePath = material->GetTexture("diffuse");
	const std::filesystem::path normalPath = material->GetTexture("normal");

	ITexture *diffuse = textureCache->LoadTexture(diffusePath,
		diffusePath.empty() ? ETextureFallback::White : ETextureFallback::Diffuse_Error2D, false);
	ITexture *normal = textureCache->LoadTexture(normalPath, ETextureFallback::Normal_Flat, false);
		
	instance->opaque = diffuse->IsOpaque();

	instance->SetSampler(BINDING_DIFFUSE, diffuse);
	instance->SetSampler(BINDING_NORMAL, normal);
	instance->SetBuffer(BINDING_LIGHTINFO, lightState->lightInfoBuffer);
	instance->SetBuffer(BINDING_MVP, shaderSystem->CreateStorageBuffer(sizeof(MVPModel) * 100));
	instance->SetBuffer(BINDING_POINTLIGHTS, lightState->pointLightBuffer);
	instance->SetBuffer(BINDING_VIEWPOS, shaderSystem->CreateUniformBuffer<ViewPos>());
}

void CShaderStaticMesh::ShutdownMaterialInstance(CMaterialInstanceVK *instance)
{
	shaderSystem->DeleteShaderBuffer(instance->GetBuffer(BINDING_MVP));
	shaderSystem->DeleteShaderBuffer(instance->GetBuffer(BINDING_VIEWPOS));
}

void CShaderStaticMesh::UpdateBuffers(CMaterialInstanceVK *materialInstance, const std::vector<Transform> &instances, const CView3D &view3D, uint32_t frame)
{
	CShaderBuffer *transformationBuffer = materialInstance->GetBuffer(BINDING_MVP);

	if (instances.size() > (transformationBuffer->bufferSize / sizeof(MVPModel)))
		shaderSystem->ResizeStorageBuffer(transformationBuffer, (sizeof(MVPModel) * instances.size()) + (sizeof(MVPModel) * 10));

	std::vector<MVPModel> transformations(instances.size());
	for (std::size_t i = 0; i < transformations.size(); ++i)
	{
		glm::mat4 modelMatrix = ToTransformation(instances[i]);

		transformations[i] = {
			view3D.GetProjection() * view3D.camera->viewMatrix * modelMatrix,
			modelMatrix
		};
	}

	transformationBuffer->UpdateBuffer(frame, transformations.data(), sizeof(MVPModel) * transformations.size());

	ViewPos viewPos = {};
	viewPos = { { view3D.camera->transform->position.x, view3D.camera->transform->position.y, view3D.camera->transform->position.z } };

	CShaderBuffer *viewPosBuffer = materialInstance->GetBuffer(BINDING_VIEWPOS);
	viewPosBuffer->UpdateBuffer(frame, &viewPos, sizeof(viewPos));
}

void CShaderStaticMesh::LoadShaderModules()
{
	shaderModules = {
		shaderSystem->CreateShaderModule("shaders/vk/static_mesh.vert.spv"),
		shaderSystem->CreateShaderModule("shaders/vk/static_mesh.frag.spv")
	};
}

void CShaderStaticMesh::CreateGraphicsPipelineLayout()
{
	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = 1;
	pipelineLayoutInfo.pSetLayouts = &descriptorSetLayout;
	pipelineLayoutInfo.pushConstantRangeCount = 0;
	pipelineLayoutInfo.pPushConstantRanges = nullptr;

	if (vkCreatePipelineLayout(graphics->device, &pipelineLayoutInfo, nullptr, &pipelineLayout) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create pipeline layout");
}

void CShaderStaticMesh::CreateGraphicsPipeline()
{
	VkPipelineShaderStageCreateInfo vertShaderStageInfo = {};
	vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
	vertShaderStageInfo.module = shaderModules[0];
	vertShaderStageInfo.pName = "main";

	VkPipelineShaderStageCreateInfo fragShaderStageInfo = {};
	fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
	fragShaderStageInfo.module = shaderModules[1];
	fragShaderStageInfo.pName = "main";

	VkPipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };

	VkVertexInputBindingDescription bindingDescription = Vertex_ToInputBindingDescription(0);
	std::vector<VkVertexInputAttributeDescription> attribDescriptions = Vertex_ToInputAttributeDescriptions(0);

	VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
	vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertexInputInfo.vertexBindingDescriptionCount = 1;
	vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;

	vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>( attribDescriptions.size() );
	vertexInputInfo.pVertexAttributeDescriptions = attribDescriptions.data();

	VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssembly.primitiveRestartEnable = VK_FALSE;

	VkPipelineViewportStateCreateInfo viewportState = {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.scissorCount = 1;

	VkPipelineRasterizationStateCreateInfo rasterizer = {};
	rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizer.depthClampEnable = VK_FALSE;
	rasterizer.rasterizerDiscardEnable = VK_FALSE;
	rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizer.lineWidth = 1.0f;
	rasterizer.cullMode = VK_CULL_MODE_BACK_BIT; // TODO: Revisit this
	rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE; // TODO: Revisit this
	rasterizer.depthBiasEnable = VK_FALSE;

	VkPipelineMultisampleStateCreateInfo multisampling = {};
	multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_FALSE;
	multisampling.rasterizationSamples = graphics->currentMSAASamples;

	VkPipelineDepthStencilStateCreateInfo depthStencilState = {};
	depthStencilState.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
	depthStencilState.depthTestEnable = VK_TRUE;
	depthStencilState.depthWriteEnable = VK_TRUE;
	depthStencilState.depthCompareOp = VK_COMPARE_OP_LESS;
	depthStencilState.depthBoundsTestEnable = VK_FALSE;
	depthStencilState.stencilTestEnable = VK_FALSE;

	VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
	colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

	// Alpha Blending
	colorBlendAttachment.blendEnable = VK_TRUE;
	colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
	colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
	colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
	colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
	colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

	VkPipelineColorBlendStateCreateInfo colorBlending = {};
	colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.logicOp = VK_LOGIC_OP_COPY;
	colorBlending.attachmentCount = 1;
	colorBlending.pAttachments = &colorBlendAttachment;

	std::array<VkDynamicState, 2> dynamicStateEnables = { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR };

	VkPipelineDynamicStateCreateInfo dynamicState = {};
	dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamicState.pDynamicStates = dynamicStateEnables.data();
	dynamicState.dynamicStateCount = static_cast<uint32_t>(dynamicStateEnables.size());

	VkGraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineInfo.stageCount = static_cast<uint32_t>(shaderModules.size());
	pipelineInfo.pStages = shaderStages;
	pipelineInfo.pVertexInputState = &vertexInputInfo;
	pipelineInfo.pInputAssemblyState = &inputAssembly;
	pipelineInfo.pViewportState = &viewportState;
	pipelineInfo.pRasterizationState = &rasterizer;
	pipelineInfo.pMultisampleState = &multisampling;
	pipelineInfo.pDepthStencilState = &depthStencilState;
	pipelineInfo.pColorBlendState = &colorBlending;
	pipelineInfo.pDynamicState = &dynamicState;
	pipelineInfo.layout = pipelineLayout;
	pipelineInfo.renderPass = graphics->renderPass;
	pipelineInfo.subpass = 0;
	
	if (vkCreateGraphicsPipelines(graphics->device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &pipeline) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create graphics pipelines");
}