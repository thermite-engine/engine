#pragma once

#include "shadervk.h"

class CShaderTransparentComposition : public CShaderVK
{
	static constexpr uint32_t BINDING_ACCUM_ATTACHMENT = 0;
	static constexpr uint32_t BINDING_REVEALAGE_ATTACHMENT = 1;

public:
	void InitShader() override;

	void LoadShaderModules() override;
	void CreateGraphicsPipelineLayout() override;
	void CreateGraphicsPipeline() override;

	std::vector<VkDescriptorSetLayoutBinding> GetDescriptorSetLayoutBindings() const override
	{
		return {}; // The input attachments are handled by the graphics API
	}
};