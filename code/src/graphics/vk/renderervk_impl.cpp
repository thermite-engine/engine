#include "renderervk_impl.h"
#include "log.h"
#include "graphicsvk.h"
#include "ui/the_imgui.h"

#include <array>
#include <set>

IScene *CRendererVK::CreateScene()
{
	CSceneVK *scene = scenes.emplace_back(std::make_unique<CSceneVK>()).get();

	scene->graphics = graphics;
	scene->LoadDefaultResources();

	return scene;
}

void CRendererVK::DeleteScene(IScene *baseScene)
{
	auto scene_it = std::find_if(scenes.begin(), scenes.end(), [ baseScene ](const std::unique_ptr<CSceneVK> &scene){ return baseScene == scene.get(); });

	if (scene_it != scenes.end())
	{
		vkDeviceWaitIdle(graphics->device);

		CSceneVK *scene = scene_it->get();
		scene->UnloadResources();
		scenes.erase(scene_it);
	}
}

void CRendererVK::GetClearColor(float *red, float *green, float *blue, float *alpha)
{
	if (red)
		*red = clearColorValue.float32[0];
	if (green)
		*green = clearColorValue.float32[1];
	if (blue)
		*blue = clearColorValue.float32[2];
	if (alpha)
		*alpha = clearColorValue.float32[3];
}

void CRendererVK::BeginFrame()
{
	if (renderWindow->isMinimized)
		return;

	vkWaitForFences(graphics->device, 1, &renderWindow->inFlightFences[currentFrame], VK_TRUE, std::numeric_limits<uint64_t>::max());

	if (vkAcquireNextImageKHR(graphics->device, renderWindow->swapChainKHR, std::numeric_limits<uint64_t>::max(), renderWindow->imageAvailableSemaphores[currentFrame], VK_NULL_HANDLE, &imageIndex) != VK_SUCCESS)
	{
		Log::Print(ESeverity::Warning, "Failed to acquire swap chain image\n");
		return;
	}

	if (renderWindow->imagesInFlight[imageIndex] != VK_NULL_HANDLE)
		vkWaitForFences(graphics->device, 1, &renderWindow->imagesInFlight[imageIndex], VK_TRUE, std::numeric_limits<uint64_t>::max());

	renderWindow->imagesInFlight[imageIndex] = renderWindow->inFlightFences[currentFrame];
	graphics->UpdateDirtyBuffers(currentFrame);

	auto &commandBuffer = commandBuffers[imageIndex];

	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
	beginInfo.pInheritanceInfo = nullptr; // Optional

	if (vkBeginCommandBuffer(commandBuffer, &beginInfo) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to begin command buffer");

	VkRenderPassBeginInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassInfo.renderPass = graphics->renderPass;
	renderPassInfo.framebuffer = renderWindow->swapChainFramebuffers[ imageIndex ];
	renderPassInfo.renderArea.offset = { 0, 0 };
	renderPassInfo.renderArea.extent = renderWindow->swapChainExtent;

	std::vector<VkClearValue> clearValues;

	if (graphics->IsMSAAEnabled())
	{
		clearValues.resize(5);
		clearValues[0].color = clearColorValue;
		clearValues[1].color = { 0.0f, 0.0f, 0.0f, 0.0f };
		clearValues[2].color = { 1.0f, 1.0f, 1.0f, 1.0f };
		clearValues[3].depthStencil = { 1.0f, 0 };
		clearValues[4].color = clearColorValue;
	}
	else
	{
		clearValues.resize(4);
		clearValues[0].color = clearColorValue;
		clearValues[1].color = { 0.0f, 0.0f, 0.0f, 0.0f };
		clearValues[2].color = { 1.0f, 1.0f, 1.0f, 1.0f };
		clearValues[3].depthStencil = { 1.0f, 0 };
	}

	renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
	renderPassInfo.pClearValues = clearValues.data();

	vkCmdBeginRenderPass(commandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
	isReadyToDraw = true;
}

void CRendererVK::DrawFrame()
{
	if (isReadyToDraw)
	{
		VkCommandBuffer &commandBuffer = commandBuffers[imageIndex];

		// Opaque subpass
		for (auto &sceneDraw : sceneDraws)
		{
			vkCmdSetViewport(commandBuffer, 0, 1, &sceneDraw.setup.viewport);
			vkCmdSetScissor(commandBuffer, 0, 1, &sceneDraw.setup.scissor);

			// Always draw skybox first
			if (sceneDraw.skyboxMesh.vertexBuffer != VK_NULL_HANDLE)
			{
				CShaderVK *shader = graphics->shaderSystem->shaders.skybox;

				vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipeline);
				vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipelineLayout, 0, 1, &sceneDraw.skyboxMesh.descriptorSet, 0, nullptr);

				vkCmdPushConstants(commandBuffer, shader->pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(sceneDraw.skyboxMesh.pushConstant), &sceneDraw.skyboxMesh.pushConstant);

				const VkDeviceSize offset = 0;
				vkCmdBindVertexBuffers(commandBuffer, 0, 1, &sceneDraw.skyboxMesh.vertexBuffer, &offset);
				vkCmdBindIndexBuffer(commandBuffer, sceneDraw.skyboxMesh.indexBuffer, 0, VK_INDEX_TYPE_UINT32);

				// Draw
				vkCmdDrawIndexed(commandBuffer, sceneDraw.skyboxMesh.indexCount, 1, 0, 0, 0);
			}

			// All opaque meshes
			for (auto &opaqueStaticMesh : sceneDraw.opaqueStaticMeshes)
			{
				CShaderVK *shader = graphics->shaderSystem->shaders.staticMesh;

				vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipeline);
				vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipelineLayout, 0, 1, &opaqueStaticMesh.descriptorSet, 0, nullptr);

				const VkDeviceSize offset = 0;
				vkCmdBindVertexBuffers(commandBuffer, 0, 1, &opaqueStaticMesh.vertexBuffer, &offset);

				if (opaqueStaticMesh.indexBuffer != VK_NULL_HANDLE)
				{
					vkCmdBindIndexBuffer(commandBuffer, opaqueStaticMesh.indexBuffer, 0, VK_INDEX_TYPE_UINT32);
					vkCmdDrawIndexed(commandBuffer, opaqueStaticMesh.indexCount, static_cast<uint32_t>(opaqueStaticMesh.instances.size()), 0, 0, 0);
				}
				else
					vkCmdDraw(commandBuffer, opaqueStaticMesh.vertexCount, static_cast<uint32_t>(opaqueStaticMesh.instances.size()), 0, 0);
			}

			for (auto &opaqueBillboardMesh : sceneDraw.opaqueBillboardMeshes)
			{
				CShaderVK *shader = graphics->shaderSystem->shaders.billboard;

				vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipeline);
				vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipelineLayout, 0, 1, &opaqueBillboardMesh.descriptorSet, 0, nullptr);

				vkCmdPushConstants(commandBuffer, shader->pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(glm::vec4), &opaqueBillboardMesh.color);

				const VkDeviceSize offset = 0;
				vkCmdBindVertexBuffers(commandBuffer, 0, 1, &opaqueBillboardMesh.vertexBuffer, &offset);

				if (opaqueBillboardMesh.indexBuffer)
				{
					vkCmdBindIndexBuffer(commandBuffer, opaqueBillboardMesh.indexBuffer, 0, VK_INDEX_TYPE_UINT32);
					vkCmdDrawIndexed(commandBuffer, opaqueBillboardMesh.indexCount, static_cast<uint32_t>(opaqueBillboardMesh.instances.size()), 0, 0, 0);
				}
				else
					vkCmdDraw(commandBuffer, opaqueBillboardMesh.vertexCount, static_cast<uint32_t>(opaqueBillboardMesh.instances.size()), 0, 0);
			}
		}

		// Here we draw all our transparent objects are part of the opaque subpass
		// The shaders will simply discard fragments with alpha so fully opaque fragments can be depth tested

		for (auto &sceneDraw : sceneDraws)
		{
			vkCmdSetViewport(commandBuffer, 0, 1, &sceneDraw.setup.viewport);
			vkCmdSetScissor(commandBuffer, 0, 1, &sceneDraw.setup.scissor);

			for (auto &transparentStaticMesh : sceneDraw.transparentStaticMeshes)
			{
				CShaderVK *shader = graphics->shaderSystem->shaders.staticMesh;

				vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipeline);
				vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipelineLayout, 0, 1, &transparentStaticMesh.descriptorSet, 0, nullptr);

				const VkDeviceSize offset = 0;
				vkCmdBindVertexBuffers(commandBuffer, 0, 1, &transparentStaticMesh.vertexBuffer, &offset);

				if (transparentStaticMesh.indexBuffer != VK_NULL_HANDLE)
				{
					vkCmdBindIndexBuffer(commandBuffer, transparentStaticMesh.indexBuffer, 0, VK_INDEX_TYPE_UINT32);
					vkCmdDrawIndexed(commandBuffer, transparentStaticMesh.indexCount, static_cast<uint32_t>(transparentStaticMesh.instances.size()), 0, 0, 0);
				}
				else
					vkCmdDraw(commandBuffer, transparentStaticMesh.vertexCount, static_cast<uint32_t>(transparentStaticMesh.instances.size()), 0, 0);
			}

			for (auto &transparentBillboardMesh : sceneDraw.transparentBillboardMeshes)
			{
				CShaderVK *shader = graphics->shaderSystem->shaders.billboard;

				vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipeline);
				vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipelineLayout, 0, 1, &transparentBillboardMesh.descriptorSet, 0, nullptr);

				vkCmdPushConstants(commandBuffer, shader->pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(glm::vec4), &transparentBillboardMesh.color);

				const VkDeviceSize offset = 0;
				vkCmdBindVertexBuffers(commandBuffer, 0, 1, &transparentBillboardMesh.vertexBuffer, &offset);
				if (transparentBillboardMesh.indexBuffer)
				{
					vkCmdBindIndexBuffer(commandBuffer, transparentBillboardMesh.indexBuffer, 0, VK_INDEX_TYPE_UINT32);
					vkCmdDrawIndexed(commandBuffer, transparentBillboardMesh.indexCount, static_cast<uint32_t>(transparentBillboardMesh.instances.size()), 0, 0, 0);
				}
				else
					vkCmdDraw(commandBuffer, transparentBillboardMesh.vertexCount, static_cast<uint32_t>(transparentBillboardMesh.instances.size()), 0, 0);
			}
		}

		vkCmdNextSubpass(commandBuffer, VK_SUBPASS_CONTENTS_INLINE);

		// Transparent subpass
		for (auto &sceneDraw : sceneDraws)
		{
			vkCmdSetViewport(commandBuffer, 0, 1, &sceneDraw.setup.viewport);
			vkCmdSetScissor(commandBuffer, 0, 1, &sceneDraw.setup.scissor);

			for (auto &transparentStaticMesh : sceneDraw.transparentStaticMeshes)
			{
				CShaderVK *shader = graphics->shaderSystem->shaders.staticMeshTransparent;

				vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipeline);

				VkDescriptorSet descriptorSets[] = { transparentStaticMesh.descriptorSet, renderWindow->transparentSubpass.attachments.descriptorSets[currentFrame] };
				vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipelineLayout, 0, 2, descriptorSets, 0, nullptr);

				const VkDeviceSize offset = 0;
				vkCmdBindVertexBuffers(commandBuffer, 0, 1, &transparentStaticMesh.vertexBuffer, &offset);

				if (transparentStaticMesh.indexBuffer != VK_NULL_HANDLE)
				{
					vkCmdBindIndexBuffer(commandBuffer, transparentStaticMesh.indexBuffer, 0, VK_INDEX_TYPE_UINT32);
					vkCmdDrawIndexed(commandBuffer, transparentStaticMesh.indexCount, static_cast<uint32_t>(transparentStaticMesh.instances.size()), 0, 0, 0);
				}
				else
					vkCmdDraw(commandBuffer, transparentStaticMesh.vertexCount, static_cast<uint32_t>(transparentStaticMesh.instances.size()), 0, 0);
			}

			for (auto &transparentBillboardMesh : sceneDraw.transparentBillboardMeshes)
			{
				CShaderVK *shader = graphics->shaderSystem->shaders.billboardTransparent;

				vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipeline);

				VkDescriptorSet descriptorSets[] = { transparentBillboardMesh.descriptorSet, renderWindow->transparentSubpass.attachments.descriptorSets[currentFrame] };
				vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipelineLayout, 0, 2, descriptorSets, 0, nullptr);

				vkCmdPushConstants(commandBuffer, shader->pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(glm::vec4), &transparentBillboardMesh.color);

				const VkDeviceSize offset = 0;
				vkCmdBindVertexBuffers(commandBuffer, 0, 1, &transparentBillboardMesh.vertexBuffer, &offset);
				if (transparentBillboardMesh.indexBuffer)
				{
					vkCmdBindIndexBuffer(commandBuffer, transparentBillboardMesh.indexBuffer, 0, VK_INDEX_TYPE_UINT32);
					vkCmdDrawIndexed(commandBuffer, transparentBillboardMesh.indexCount, static_cast<uint32_t>(transparentBillboardMesh.instances.size()), 0, 0, 0);
				}
				else
					vkCmdDraw(commandBuffer, transparentBillboardMesh.vertexCount, static_cast<uint32_t>(transparentBillboardMesh.instances.size()), 0, 0);
			}
		}

		vkCmdNextSubpass(commandBuffer, VK_SUBPASS_CONTENTS_INLINE);

		// Composition
		vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, graphics->shaderSystem->shaders.transparentComposition->pipelineLayout, 0, 1, &renderWindow->transparentCompositionSubpass.attachments.descriptorSets[currentFrame], 0, nullptr);
		vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, graphics->shaderSystem->shaders.transparentComposition->pipeline);
		vkCmdDraw(commandBuffer, 3, 1, 0, 0);

		vkCmdNextSubpass(commandBuffer, VK_SUBPASS_CONTENTS_INLINE);

		for (auto &uiDraw : uiDraws)
		{
			CShaderVK *shader = graphics->shaderSystem->shaders.ui;
			vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipeline);

			const VkDeviceSize offset = 0;
			vkCmdBindVertexBuffers(commandBuffer, 0, 1, &uiDraw.setup.vertexBuffer, &offset);
			vkCmdBindIndexBuffer(commandBuffer, uiDraw.setup.indexBuffer, 0, VK_INDEX_TYPE_UINT32);

			vkCmdSetViewport(commandBuffer, 0, 1, &uiDraw.setup.viewport);
			vkCmdPushConstants(commandBuffer, shader->pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(UiPushConstant), &uiDraw.setup.pushConstant);

			for (auto &drawCmd : uiDraw.drawCmds)
			{
				vkCmdSetScissor(commandBuffer, 0, 1, &drawCmd.scissor);

				if (drawCmd.descriptorSet)
					vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipelineLayout, 0, 1, &drawCmd.descriptorSet, 0, nullptr);

				// Draw
				vkCmdDrawIndexed(commandBuffer, drawCmd.indexCount, 1, drawCmd.firstIndex, drawCmd.vertexOffset, 0);
			}
		}
	}
}

void CRendererVK::DrawScene(IScene *baseScene, const CView3D &view3D, const glm::mat4 &viewMatrix)
{
	SceneDraw &sceneDraw = sceneDraws.emplace_back();

	CSceneVK *scene = CSceneVK::ToSceneVK(baseScene);
	scene->lightState.UpdateBuffers(currentFrame);

	VkViewport &viewport = sceneDraw.setup.viewport;
	viewport.x = static_cast<float>(view3D.x);
	viewport.y = static_cast<float>(view3D.height) + static_cast<float>(view3D.y);
	viewport.width = static_cast<float>(view3D.width);
	viewport.height = static_cast<float>(view3D.height) * -1.0f;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	VkRect2D &scissor = sceneDraw.setup.scissor;
	scissor.offset = { view3D.x, view3D.y };
	scissor.extent.width = view3D.width;
	scissor.extent.height = view3D.height;

	for (auto &draw : scene->drawList)
	{
		auto &meshList = draw.materialInstance->opaque ? sceneDraw.opaqueStaticMeshes : sceneDraw.transparentStaticMeshes;
		bool meshFound = false;

		for (auto &mesh : meshList)
		{
			if (mesh.vertexBuffer == draw.vertexBuffer)
			{
				meshFound = true;
				mesh.instances.emplace_back(draw.transform);
				break;
			}
		}

		if (!meshFound)
		{
			auto &mesh = meshList.emplace_back();
			mesh.instances.emplace_back(draw.transform);
			mesh.materialInstance = draw.materialInstance;
			mesh.descriptorSet = draw.materialInstance->descriptorSets[currentFrame];
			mesh.vertexBuffer = draw.vertexBuffer;
			mesh.indexBuffer = draw.indexBuffer;
			mesh.vertexCount = draw.vertexCount;
			mesh.indexCount = draw.indexCount;
		}
	}

	for (auto &mesh : sceneDraw.opaqueStaticMeshes)
		graphics->shaderSystem->shaders.staticMesh->UpdateBuffers(mesh.materialInstance, mesh.instances, view3D, currentFrame);

	for (auto &mesh : sceneDraw.transparentStaticMeshes)
		graphics->shaderSystem->shaders.staticMesh->UpdateBuffers(mesh.materialInstance, mesh.instances, view3D, currentFrame);

	const bool drawSkybox = true;
	if (drawSkybox)
	{
		CMeshVK *mesh = scene->skyboxMesh.get();
		if (mesh)
		{
			CMaterialInstanceVK *materialInstance = mesh->materialInstance.get();
			materialInstance->UpdateDirtyDescriptorSets(currentFrame);

			sceneDraw.skyboxMesh.materialInstance = materialInstance;
			sceneDraw.skyboxMesh.descriptorSet = materialInstance->descriptorSets[currentFrame];
			sceneDraw.skyboxMesh.vertexBuffer = mesh->vertexBuffer->buffer;
			sceneDraw.skyboxMesh.indexBuffer = mesh->indexBuffer->buffer;
			sceneDraw.skyboxMesh.vertexCount = mesh->vertexBuffer->vertexCount;
			sceneDraw.skyboxMesh.indexCount = mesh->indexBuffer->indexCount;

			sceneDraw.skyboxMesh.pushConstant.view = glm::mat3(view3D.camera->viewMatrix);
			sceneDraw.skyboxMesh.pushConstant.proj = view3D.GetProjection();
		}
	}

	const bool drawBill = true;
	if (drawBill)
	{
		for (auto &draw : scene->billboardDrawList)
		{
			auto &meshList = draw.materialInstance->opaque ? sceneDraw.opaqueBillboardMeshes : sceneDraw.transparentBillboardMeshes;
			bool meshFound = false;

			for (auto &mesh : meshList)
			{
				if (mesh.vertexBuffer == draw.vertexBuffer)
				{
					meshFound = true;
					mesh.instances.emplace_back(draw.transform);
					break;
				}
			}

			if (!meshFound)
			{
				auto &mesh = meshList.emplace_back();
				mesh.instances.emplace_back(draw.transform);
				mesh.materialInstance = draw.materialInstance;
				mesh.descriptorSet = draw.materialInstance->descriptorSets[currentFrame];
				mesh.vertexBuffer = draw.vertexBuffer;
				mesh.indexBuffer = draw.indexBuffer;
				mesh.vertexCount = draw.vertexCount;
				mesh.indexCount = draw.indexCount;
				mesh.color = draw.color;
			}
		}

		for (auto &mesh : sceneDraw.opaqueBillboardMeshes)
			graphics->shaderSystem->shaders.billboard->UpdateBuffers(mesh.materialInstance, mesh.instances, view3D, currentFrame);

		for (auto &mesh : sceneDraw.transparentBillboardMeshes)
			graphics->shaderSystem->shaders.billboard->UpdateBuffers(mesh.materialInstance, mesh.instances, view3D, currentFrame);
	}
}

void CRendererVK::DrawUI(ImGuiContext *ctx)
{
	static_assert(sizeof(ImDrawIdx) == sizeof(uint32_t), "Please fix ImDrawIdx definition in imconfig.h");

	ThermiteImGui::SetCurrentContext(ctx);
	ImDrawData *drawData = ImGui::GetDrawData();

	int fb_width = (int)(drawData->DisplaySize.x * drawData->FramebufferScale.x);
	int fb_height = (int)(drawData->DisplaySize.y * drawData->FramebufferScale.y);

	if (drawData->TotalVtxCount > 0)
	{
		UiDraw &uiDraw = uiDraws.emplace_back();

		if (!renderWindow->guiVertexBuffer)
			renderWindow->guiVertexBuffer = graphics->CreateDynamicVertexBuffer(sizeof(ImDrawVert), drawData->TotalVtxCount, std::nullopt);

		if (!renderWindow->guiIndexBuffer)
			renderWindow->guiIndexBuffer = graphics->CreateDynamicIndexBuffer(sizeof(ImDrawIdx), drawData->TotalIdxCount, std::nullopt);

		assert(drawData->TotalVtxCount >= 0);
		assert(drawData->TotalVtxCount >= 0);

		if (renderWindow->guiVertexBuffer->vertexCount < (uint32_t)drawData->TotalVtxCount)
		{
			graphics->DestroyDynamicVertexBuffer(renderWindow->guiVertexBuffer);
			renderWindow->guiVertexBuffer = graphics->CreateDynamicVertexBuffer(sizeof(ImDrawVert), drawData->TotalVtxCount, std::nullopt);
		}

		if (renderWindow->guiIndexBuffer->indexCount < (uint32_t)drawData->TotalIdxCount)
		{
			graphics->DestroyDynamicIndexBuffer(renderWindow->guiIndexBuffer);
			renderWindow->guiIndexBuffer = graphics->CreateDynamicIndexBuffer(sizeof(ImDrawIdx), drawData->TotalIdxCount, std::nullopt);
		}


		ImDrawVert *vtxData = (ImDrawVert*)renderWindow->guiVertexBuffer->mapped[currentFrame];
		ImDrawIdx *idxData = (ImDrawIdx*)renderWindow->guiIndexBuffer->mapped[currentFrame];

		for (int i = 0; i < drawData->CmdListsCount; ++i)
		{
			const ImDrawList *cmdList = drawData->CmdLists[i];
			std::memcpy(vtxData, cmdList->VtxBuffer.Data, cmdList->VtxBuffer.Size * sizeof(ImDrawVert));
			std::memcpy(idxData, cmdList->IdxBuffer.Data, cmdList->IdxBuffer.Size * sizeof(ImDrawIdx));

			vtxData += cmdList->VtxBuffer.Size;
			idxData += cmdList->IdxBuffer.Size;
		}

		uiDraw.setup.vertexBuffer = renderWindow->guiVertexBuffer->buffer[currentFrame];
		uiDraw.setup.indexBuffer = renderWindow->guiIndexBuffer->buffer[currentFrame];

		VkViewport &viewport = uiDraw.setup.viewport;
		viewport.x = 0;
		viewport.y = 0;
		viewport.width = (float)fb_width;
		viewport.height = (float)fb_height;
		viewport.minDepth = 0.0f;
		viewport.maxDepth = 1.0f;

		uiDraw.setup.pushConstant.scale.x = 2.0f / drawData->DisplaySize.x;
		uiDraw.setup.pushConstant.scale.y = 2.0f / drawData->DisplaySize.y;

		uiDraw.setup.pushConstant.translate.x = -1.0f - drawData->DisplayPos.x * uiDraw.setup.pushConstant.scale.x;
		uiDraw.setup.pushConstant.translate.y = -1.0f - drawData->DisplayPos.y * uiDraw.setup.pushConstant.scale.y;

		// Will project scissor/clipping rectangles into framebuffer space
		ImVec2 clip_off = drawData->DisplayPos;         // (0,0) unless using multi-viewports
		ImVec2 clip_scale = drawData->FramebufferScale; // (1,1) unless using retina display which are often (2,2)

		// Render command lists
		// (Because we merged all buffers into a single one, we maintain our own offset into them)
		int vtxOffset = 0;
		int idxOffset = 0;

		for (int i = 0; i < drawData->CmdListsCount; i++)
		{
			const ImDrawList *cmdList = drawData->CmdLists[i];

			for (int cmd_i = 0; cmd_i < cmdList->CmdBuffer.Size; cmd_i++)
			{
				const ImDrawCmd *drawCmd = &cmdList->CmdBuffer[cmd_i];
				if (drawCmd->UserCallback != nullptr)
				{
					// User callback, registered via ImDrawList::AddCallback()
					// (ImDrawCallback_ResetRenderState is a special callback value used by the user to request the renderer to reset render state.)
					if (drawCmd->UserCallback == ImDrawCallback_ResetRenderState)
						Log::Print(ESeverity::Warning, "Unsupported ImGui draw callback!\n");
					else
						drawCmd->UserCallback(cmdList, drawCmd);
				}
				else
				{
					// Project scissor/clipping rectangles into framebuffer space
					ImVec4 clip_rect;
					clip_rect.x = (drawCmd->ClipRect.x - clip_off.x) * clip_scale.x;
					clip_rect.y = (drawCmd->ClipRect.y - clip_off.y) * clip_scale.y;
					clip_rect.z = (drawCmd->ClipRect.z - clip_off.x) * clip_scale.x;
					clip_rect.w = (drawCmd->ClipRect.w - clip_off.y) * clip_scale.y;

					if (clip_rect.x < fb_width && clip_rect.y < fb_height && clip_rect.z >= 0.0f && clip_rect.w >= 0.0f)
					{
						UiDrawCmd &uiDrawCmd = uiDraw.drawCmds.emplace_back();

						// Negative offsets are illegal for vkCmdSetScissor
						if (clip_rect.x < 0.0f)
							clip_rect.x = 0.0f;
						if (clip_rect.y < 0.0f)
							clip_rect.y = 0.0f;

						// Apply scissor/clipping rectangle
						VkRect2D &scissor = uiDrawCmd.scissor;
						scissor.offset.x = (int32_t)(clip_rect.x);
						scissor.offset.y = (int32_t)(clip_rect.y);
						scissor.extent.width = (uint32_t)(clip_rect.z - clip_rect.x);
						scissor.extent.height = (uint32_t)(clip_rect.w - clip_rect.y);

						CGuiMaterial *material = reinterpret_cast<CGuiMaterial*>(drawCmd->TextureId);

						if (material)
							uiDrawCmd.descriptorSet = material->descriptorSets[currentFrame];

						uiDrawCmd.indexCount = drawCmd->ElemCount;
						uiDrawCmd.firstIndex = drawCmd->IdxOffset + idxOffset;
						uiDrawCmd.vertexOffset = drawCmd->VtxOffset + vtxOffset;
					}
				}
			}

			idxOffset += cmdList->IdxBuffer.Size;
			vtxOffset += cmdList->VtxBuffer.Size;
		}
	}
}

void CRendererVK::EndFrame()
{
	if (isReadyToDraw)
	{
		auto &commandBuffer = commandBuffers[imageIndex];

		vkCmdEndRenderPass(commandBuffer);

		if (vkEndCommandBuffer(commandBuffer) != VK_SUCCESS)
			throw std::runtime_error("[Vulkan]Failed to end command buffer");

		VkSemaphore waitSemaphores[] = { renderWindow->imageAvailableSemaphores[currentFrame] };
		VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT };

		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = waitSemaphores;
		submitInfo.pWaitDstStageMask = waitStages;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &commandBuffers[imageIndex];

		VkSemaphore signalSemaphores[] = { renderWindow->renderFinishedSemaphores[currentFrame] };
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = signalSemaphores;

		vkResetFences(graphics->device, 1, &renderWindow->inFlightFences[currentFrame]);

		if (vkQueueSubmit(graphics->graphicsQueue, 1, &submitInfo, renderWindow->inFlightFences[currentFrame]) != VK_SUCCESS)
		{
			Log::Print(ESeverity::Warning, "Queue submit failed!\n");
			return;
		}

		VkPresentInfoKHR presentInfo = {};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = signalSemaphores;
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = &renderWindow->swapChainKHR;
		presentInfo.pImageIndices = &imageIndex;
		presentInfo.pResults = nullptr; // Optional

		if (vkQueuePresentKHR(graphics->presentQueue, &presentInfo) != VK_SUCCESS)
			Log::Print(ESeverity::Warning, "Queue present failed!\n");

		currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
		isReadyToDraw = false;
	}
}

void CRendererVK::ClearDrawLists()
{
	sceneDraws.clear();
	uiDraws.clear();
}
