#pragma once

#include "shadervk.h"
#include <vector>
#include <unordered_map>
#include <string>
#include <memory>
#include <filesystem>

#include "shaderbuffer.h"
#include "graphicsvk_definitions.h"
#include "renderwindow.h"

class CGraphicsVK;

class CShaderSystemVK
{
public:
	//CShaderVK *GetShader(const std::string &name) { return shaderMap[ name ].get(); }

	void OnWindowResize();

	void LoadShaders();
	void UnloadShaders();

	void RebuildPipelines();

	VkShaderModule CreateShaderModule(const std::filesystem::path shaderPath);

	template <typename T>
	CShaderVK *CreateShader(const std::string &name)
	{
		auto pair = shaderMap.emplace(std::make_pair(name, std::make_unique<T>()));

		CShaderVK *shader = pair.first->second.get();
		shader->shaderName = pair.first->first;
		shader->graphics = graphics;
		shader->shaderSystem = this;

		shader->LoadShaderModules();
		shader->CreateDescriptorSetLayout();
		shader->CreateGraphicsPipelineLayout();
		shader->CreateGraphicsPipeline();

		shader->InitShader();
		return shader;
	}

	template <typename T>
	CShaderBuffer *CreateUniformBuffer()
	{
		return CreateShaderBuffer_Internal(sizeof(T), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER);
	}

	CShaderBuffer *CreateStorageBuffer(VkDeviceSize bufferSize)
	{
		return CreateShaderBuffer_Internal(bufferSize, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER);
	}

	void ResizeStorageBuffer(CShaderBuffer *shaderBuffer, VkDeviceSize bufferSize);
	void DeleteShaderBuffer(CShaderBuffer *shaderBuffer);

	CGraphicsVK *graphics = nullptr;

	std::unordered_map<std::string, std::unique_ptr<CShaderVK>> shaderMap;
	std::vector<std::unique_ptr<CShaderBuffer>> shaderBuffers;

	struct Shaders
	{
		CShaderVK *skybox = nullptr;
		CShaderVK *staticMesh = nullptr;
		CShaderVK *staticMeshTransparent = nullptr;
		CShaderVK *transparentComposition = nullptr;
		CShaderVK *billboard = nullptr;
		CShaderVK *billboardTransparent = nullptr;
		CShaderVK *ui = nullptr;
	} shaders;

private:
	CShaderBuffer *CreateShaderBuffer_Internal(VkDeviceSize bufferSize, VkBufferUsageFlags bufferUsage, VkDescriptorType descriptorType);
};