#pragma once

#include <vector>
#include <memory>

#include "model.h"
#include "meshvk.h"

class CModelVK : public IModel
{
public:
	static CModelVK *ToModelVK(IModel *model)
	{
#ifdef _DEBUG
		return dynamic_cast<CModelVK*>(model);
#else
		return static_cast<CModelVK*>(model);
#endif
	}

	std::vector<std::unique_ptr<CMeshVK>> meshes;
};