#pragma once

#include <array>
#include <vector>
#include <memory>
#include <string>
#include <filesystem>
#include <unordered_map>
#include <volk.h>

#include "sprite.h"
#include "graphicsvk_definitions.h"
#include "texturevk.h"
#include "vertexbuffervk.h"
#include "indexbuffervk.h"
#include "materialinstance.h"

class CGraphicsVK;
class CTextureCacheVK;
class CMaterialCacheVK;

class CBillboardSpriteVK : public IBillboardSprite
{
public:

	void SetColor(const glm::vec4 &color) { this->color = color;  }
	glm::vec4 GetColor() const { return color; }

	// TODO: Create our own upcast function
	static CBillboardSpriteVK *ToBillboardSpriteVK(IBillboardSprite *sprite)
	{
#ifdef _DEBUG
		return dynamic_cast<CBillboardSpriteVK *>(sprite);
#else
		return static_cast<CBillboardSpriteVK *>(sprite);
#endif
	}

	CVertexBuffer *vertexBuffer = nullptr;
	CIndexBuffer *indexBuffer = nullptr;
	std::unique_ptr<CMaterialInstanceVK> materialInstance;

	glm::vec4 color = { 1.0f, 1.0f, 1.0f, 1.0f };
};

class CSpriteLoaderVK
{
public:
	IBillboardSprite *LoadBillboardSprite(const std::filesystem::path &spritePath);
	void UnloadBillboardSprite(IBillboardSprite *sprite);

	void UnloadResources();

	CGraphicsVK *graphics = nullptr;
	CTextureCacheVK *textureCache = nullptr;
	CMaterialCacheVK *materialCache = nullptr;

	std::vector<std::unique_ptr<CBillboardSpriteVK>> billboardSprites;
};