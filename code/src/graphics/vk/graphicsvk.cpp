#define VMA_IMPLEMENTATION // for vk_mem_alloc.h

#include "graphicsvk.h"
#include "renderwindow.h"
#include "log.h"
#include "ui/the_imgui.h"
#include "materialinstance.h"

#include <algorithm>
#include <array>
#include <map>
#include <set>
#include <SDL2/SDL_vulkan.h>

#if VK_DEBUG
static VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT, VkDebugUtilsMessageTypeFlagsEXT, const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData, void *)
{
	Log::Print(ESeverity::Warning, "Validation layer: {}\n", pCallbackData->pMessage);
	return VK_FALSE;
}
#endif // VK_DEBUG

struct SwapChainSupportDetails
{
	VkSurfaceCapabilitiesKHR capabilities = {};
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};

SwapChainSupportDetails QuerySwapChainSupport(CRenderWindow *renderWindow, VkPhysicalDevice device)
{
	SwapChainSupportDetails details;
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, renderWindow->surface, &details.capabilities);

	uint32_t formatCount = 0;
	vkGetPhysicalDeviceSurfaceFormatsKHR(device, renderWindow->surface, &formatCount, nullptr);

	if (formatCount != 0)
	{
		details.formats.resize(formatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, renderWindow->surface, &formatCount, details.formats.data());
	}

	uint32_t presentModeCount = 0;
	vkGetPhysicalDeviceSurfacePresentModesKHR(device, renderWindow->surface, &presentModeCount, nullptr);

	if (presentModeCount != 0)
	{
		details.presentModes.resize( presentModeCount );
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, renderWindow->surface, &presentModeCount, details.presentModes.data());
	}

	return details;
}


#if VK_DEBUG
VkResult CGraphicsVK::CreateDebugUtilsMessengerEXT(const VkDebugUtilsMessengerCreateInfoEXT * pCreateInfo, const VkAllocationCallbacks *pAllocator)
{
	auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");

	if (func)
		return func(instance, pCreateInfo, pAllocator, &debugCallback);

	return VK_ERROR_EXTENSION_NOT_PRESENT;
}

void CGraphicsVK::DestroyDebugUtilsMessengerEXT(VkAllocationCallbacks *pAllocator)
{
	auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");

	if (func != nullptr)
		func(instance, debugCallback, pAllocator);
}
#endif // VK_DEBUG

bool CGraphicsVK::IsPhysicalDeviceSuitable(CRenderWindow *renderWindow, VkPhysicalDevice device)
{
	auto checkDeviceExtensionSupport = [ device, this ]() -> bool
	{
		uint32_t extensionCount = 0;
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

		std::vector<VkExtensionProperties> availableExtensions( extensionCount );
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

		std::set<std::string> requiredDeviceExtensionSet(deviceExtensions.begin(), deviceExtensions.end());

		for (const auto &extension : availableExtensions)
			requiredDeviceExtensionSet.erase(extension.extensionName);

		return (requiredDeviceExtensionSet.empty());
	};

	auto checkSwapChainSupport = [ & ](const SwapChainSupportDetails &swapChainSupport)
	{
		return (!swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty());
	};

	// TODO: Allow custom callbacks for checking device features and extensions


	return (FindQueueFamilies(renderWindow, device).IsComplete() &&
		checkDeviceExtensionSupport() &&
		checkSwapChainSupport(QuerySwapChainSupport(renderWindow, device)));
}

CGraphicsVK::~CGraphicsVK()
{
	if (isInitialized)
		input->UnregisterWindowEventListener(this);

	if (device != VK_NULL_HANDLE)
		vkDeviceWaitIdle(device);

	textureCache->UnloadTextures();
	delete textureCache; // TODO: Make this better
	textureCache = nullptr;

	// It's safe to destroy these buffers immediately now the device is idle
	for (auto &destroy : bufferDestroyList)
		vmaDestroyBuffer(allocator, destroy.buffer, destroy.allocation);

	for (auto &destroy : textureDestroyList)
	{
		if (destroy.sampler != VK_NULL_HANDLE)
			vkDestroySampler(device, destroy.sampler, nullptr);

		if (destroy.imageView != VK_NULL_HANDLE)
			vkDestroyImageView(device, destroy.imageView, nullptr);

		if (destroy.image != VK_NULL_HANDLE)
			vmaDestroyImage(allocator, destroy.image, destroy.allocation);
	}

	for (auto &destroy : descriptorPoolDestroyList)
	{
		if (destroy.descriptorPool != VK_NULL_HANDLE)
			vkDestroyDescriptorPool(device, destroy.descriptorPool, nullptr);
	}

	bufferDestroyList.clear();
	textureDestroyList.clear();
	descriptorPoolDestroyList.clear();

	if (descriptorSetLayouts.depthOnly != VK_NULL_HANDLE)
	{
		vkDestroyDescriptorSetLayout(device, descriptorSetLayouts.depthOnly, nullptr);
		descriptorSetLayouts.depthOnly = VK_NULL_HANDLE;
	}

	if (descriptorSetLayouts.accumRevealage != VK_NULL_HANDLE)
	{
		vkDestroyDescriptorSetLayout(device, descriptorSetLayouts.accumRevealage, nullptr);
		descriptorSetLayouts.accumRevealage = VK_NULL_HANDLE;
	}

	shaderSystem->UnloadShaders();

	if (renderPass != VK_NULL_HANDLE)
	{
		vkDestroyRenderPass(device, renderPass, nullptr);
		renderPass = VK_NULL_HANDLE;
	}

	if (commandPool != VK_NULL_HANDLE)
	{
		vkDestroyCommandPool(device, commandPool, nullptr);
		commandPool = VK_NULL_HANDLE;
	}

	if (computeCommandPool != VK_NULL_HANDLE)
	{
		vkDestroyCommandPool(device, computeCommandPool, nullptr);
		computeCommandPool = VK_NULL_HANDLE;
	}

	if (allocator != VK_NULL_HANDLE)
	{
		vmaDestroyAllocator(allocator);
		allocator = VK_NULL_HANDLE;
	}

#if VK_DEBUG
	if (debugCallback != VK_NULL_HANDLE)
	{
		DestroyDebugUtilsMessengerEXT(nullptr);
		debugCallback = VK_NULL_HANDLE;
	}
#endif

	if (device != VK_NULL_HANDLE)
	{
		vkDestroyDevice(device, nullptr);
		device = VK_NULL_HANDLE;

		// These are cleaned up when the device is destroyed
		graphicsQueue = VK_NULL_HANDLE;
		presentQueue = VK_NULL_HANDLE;
	}

	// Physical devices are cleaned up when VkInstance is destroyed
	physicalDevice = VK_NULL_HANDLE;

	if (instance != VK_NULL_HANDLE)
	{
		vkDestroyInstance(instance, nullptr);
		instance = VK_NULL_HANDLE;
	}

	requiredExtensions.clear();
}

void CGraphicsVK::Init(CRenderWindow *renderWindow)
{
	if (volkInitialize() != VK_SUCCESS)
		Log::FatalError("Failed to initialize volk\n");

	input->RegisterWindowEventListener(this);

	LoadExtensions(renderWindow->windowHandle);
	CheckValidationLayerSupport();
	CreateInstance();
	SetupDebugCallback();
}

void CGraphicsVK::ToggleMSAA()
{
	vkDeviceWaitIdle(device);

	currentMSAASamples = (currentMSAASamples == VK_SAMPLE_COUNT_1_BIT) ? maxMSAASamples : VK_SAMPLE_COUNT_1_BIT;

	if (IsMSAAEnabled())
		Log::Print("MSAA On\n");
	else
		Log::Print("MSAA Off\n");

	if (renderPass != VK_NULL_HANDLE)
	{
		vkDestroyRenderPass(device, renderPass, nullptr);
		renderPass = VK_NULL_HANDLE;
	}

	if (!IsMSAAEnabled())
		CreateRenderPass();
	else
		CreateRenderPassMSAA();

	for (auto &[id, windowHandle] : renderWindows)
	{
		CRenderWindow *renderWindow = windowHandle.get();

		DestroySwapChain(renderWindow);
		CreateSwapChain(renderWindow);
		CreateSwapChainImageViews(renderWindow);

		if (IsMSAAEnabled())
			CreateColorResources(renderWindow);

		CreateAccumResources(renderWindow);
		CreateRevealageResources(renderWindow);
		CreateDepthResources(renderWindow);
		CreateFramebuffers(renderWindow);
		CreateFramebufferDescriptorSets(renderWindow);

		shaderSystem->RebuildPipelines();
	}
}

void CGraphicsVK::Update(uint64_t)
{
	for (auto it = bufferDestroyList.begin(); it != bufferDestroyList.end();)
	{
		if (--it->destroyFrame == 0)
		{
			if (it->buffer != VK_NULL_HANDLE)
				vmaDestroyBuffer(allocator, it->buffer, it->allocation);

			it = bufferDestroyList.erase(it);
		}
		else
			++it;
	}

	for (auto it = textureDestroyList.begin(); it != textureDestroyList.end();)
	{
		if (--it->destroyFrame == 0)
		{
			if (it->sampler != VK_NULL_HANDLE)
				vkDestroySampler(device, it->sampler, nullptr);

			if (it->imageView != VK_NULL_HANDLE)
				vkDestroyImageView(device, it->imageView, nullptr);

			if (it->image != VK_NULL_HANDLE)
				vmaDestroyImage(allocator, it->image, it->allocation);

			it = textureDestroyList.erase(it);
		}
		else
			++it;
	}

	for (auto it = descriptorPoolDestroyList.begin(); it != descriptorPoolDestroyList.end();)
	{
		if (--it->destroyFrame == 0)
		{
			if (it->descriptorPool != VK_NULL_HANDLE)
				vkDestroyDescriptorPool(device, it->descriptorPool, nullptr);

			it = descriptorPoolDestroyList.erase(it);
		}
		else
			++it;
	}
}

void CGraphicsVK::OnWindowResize(CRenderWindow *renderWindow)
{
	vkDeviceWaitIdle(device);

	DestroySwapChain(renderWindow);
	CreateSwapChain(renderWindow);
	CreateSwapChainImageViews(renderWindow);

	if (IsMSAAEnabled())
		CreateColorResources(renderWindow);

	CreateAccumResources(renderWindow);
	CreateRevealageResources(renderWindow);
	CreateDepthResources(renderWindow);
	CreateFramebuffers(renderWindow);
	CreateFramebufferDescriptorSets(renderWindow);

	shaderSystem->OnWindowResize();

	for (auto resizeCallback : renderWindow->windowResizeCallbacks)
		resizeCallback->OnWindowResize(renderWindow, renderWindow->GetWidth(), renderWindow->GetHeight());
}

void CGraphicsVK::OnWindowEvent(const SDL_WindowEvent &windowEvent)
{
	auto window_it = renderWindows.find(windowEvent.windowID);
	if (window_it == renderWindows.end())
		return;
			
	CRenderWindow *renderWindow = window_it->second.get();
			
	switch (windowEvent.event)
	{
		case SDL_WINDOWEVENT_CLOSE:
		{
			for (auto &onClosed : renderWindow->onClosedCallbacks)
				onClosed(renderWindow);

			break;
		}
		case SDL_WINDOWEVENT_MINIMIZED:
		{
			renderWindow->isMinimized = true;
			break;
		}
		case SDL_WINDOWEVENT_RESTORED:
		case SDL_WINDOWEVENT_MAXIMIZED:
		{
			renderWindow->isMinimized = false;
			break;
		}
		case SDL_WINDOWEVENT_RESIZED:
		{
			Log::Print("WindowID: {}\n", windowEvent.windowID);
			OnWindowResize(renderWindow);
			break;
		}
	}
}

IWindow *CGraphicsVK::CreateWindow(const std::string_view &title, int width, int height)
{
	std::unique_ptr<CRenderWindow> renderWindow = std::make_unique<CRenderWindow>();

	uint32_t windowFlags = SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE;

	IWindow *baseWindow = renderWindow.get();
	renderWindow->windowHandle = SDL_CreateWindow(title.data(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, windowFlags);
	renderWindow->windowID = SDL_GetWindowID(renderWindow->windowHandle);

	SetupWindow(renderWindow.get());

	renderWindows[ renderWindow->windowID ] = std::move(renderWindow);
	isInitialized = true;

	return baseWindow;
}

IWindow *CGraphicsVK::CreateWindowFullscreen(const std::string_view &title)
{
	std::unique_ptr<CRenderWindow> renderWindow = std::make_unique<CRenderWindow>();

	uint32_t windowFlags = SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN | SDL_WINDOW_FULLSCREEN_DESKTOP | SDL_WINDOW_BORDERLESS;

	IWindow *baseWindow = renderWindow.get();
	renderWindow->windowHandle = SDL_CreateWindow(title.data(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 0, 0, windowFlags);
	renderWindow->windowID = SDL_GetWindowID(renderWindow->windowHandle);

	SetupWindow(renderWindow.get());

	renderWindows[ renderWindow->windowID ] = std::move(renderWindow);
	isInitialized = true;

	return baseWindow;
}

void CGraphicsVK::SetupWindow(CRenderWindow *renderWindow)
{
	if (!isInitialized)
		Init(renderWindow);

	CreateSurface(renderWindow);

	if (!isInitialized)
	{
		PickPhysicalDevice(renderWindow);
		CreateLogicalDevice();
		CreateVmaAllocator();
		GetPhysicalDeviceLimits();
	}

	if (!isInitialized)
		CreateCommandPools();

	CreateSwapChain(renderWindow);
	CreateSwapChainImageViews(renderWindow);

	if (!isInitialized)
	{
		FindDepthFormat();

		if (IsMSAAEnabled())
			CreateRenderPassMSAA();
		else
			CreateRenderPass();
	}

	if (IsMSAAEnabled())
		CreateColorResources(renderWindow);

	CreateAccumResources(renderWindow);
	CreateRevealageResources(renderWindow);
	CreateDepthResources(renderWindow);
	CreateFramebuffers(renderWindow);
	CreateSyncObjects(renderWindow);

	if (!isInitialized)
	{
		// These must be created before loading shaders
		CreateFramebufferDescriptorSetLayouts();

		shaderSystem = std::make_unique<CShaderSystemVK>();
		shaderSystem->graphics = this;
		shaderSystem->LoadShaders();

		textureCache = new CTextureCacheVK;

		textureCache->graphics = this;
		textureCache->owner = this;
		textureCache->LoadFallbackTextures();
	}

	CreateFramebufferDescriptorSets(renderWindow);

	SetupImGuiContext(renderWindow);
}

void CGraphicsVK::DeleteWindow(IWindow *window)
{
	CRenderWindow *renderWindow = CRenderWindow::ToRenderWindow(window);

	if (!renderWindow)
		return;

	if (device != VK_NULL_HANDLE)
		vkDeviceWaitIdle(device);

	DestroyDynamicVertexBuffer(renderWindow->guiVertexBuffer);
	DestroyDynamicIndexBuffer(renderWindow->guiIndexBuffer);

	renderWindow->guiVertexBuffer = nullptr;
	renderWindow->guiIndexBuffer = nullptr;

	for (auto &material : renderWindow->guiMaterials)
		DestroyGuiMaterial(renderWindow, material.get());

	renderWindow->fontMaterial = nullptr;
	renderWindow->guiMaterials.clear();

	DestroySwapChain(renderWindow);

	// Destroy sync objects
	for (std::size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
	{
		if (renderWindow->renderFinishedSemaphores[ i ] != VK_NULL_HANDLE) 
		{
			vkDestroySemaphore(device, renderWindow->renderFinishedSemaphores[ i ], nullptr);
			renderWindow->renderFinishedSemaphores[ i ] = VK_NULL_HANDLE;
		}

		if (renderWindow->imageAvailableSemaphores[ i ] != VK_NULL_HANDLE)
		{
			vkDestroySemaphore(device, renderWindow->imageAvailableSemaphores[ i ], nullptr);
			renderWindow->imageAvailableSemaphores[ i ] = VK_NULL_HANDLE;
		}

		if (renderWindow->inFlightFences[ i ] != VK_NULL_HANDLE)
		{
			vkDestroyFence(device, renderWindow->inFlightFences[ i ], nullptr);
			renderWindow->inFlightFences[ i ] = VK_NULL_HANDLE;
		}
	}

	if (renderWindow->surface != VK_NULL_HANDLE)
		vkDestroySurfaceKHR(instance, renderWindow->surface, nullptr);

	SDL_DestroyWindow(renderWindow->windowHandle);
	renderWindows.erase(renderWindow->windowID);
}

IRenderer *CGraphicsVK::CreateRenderer(IWindow *window)
{
	CRenderWindow *renderWindow = CRenderWindow::ToRenderWindow(window);

	if (!renderWindow)
		throw std::runtime_error("Tried to create renderer for a NULL window");

	CRendererVK *renderer = renderers.emplace_back(std::make_unique<CRendererVK>()).get();

	renderer->graphics = this;
	renderer->renderWindow = renderWindow;

	renderer->commandBuffers.resize(renderWindow->numSwapChainImages);

	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = renderWindow->numSwapChainImages;

	if (vkAllocateCommandBuffers(device, &allocInfo, renderer->commandBuffers.data()) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to allocate command buffers");

	return renderer;
}

void CGraphicsVK::DeleteRenderer(IRenderer *baseRenderer)
{
	auto renderer_it = std::find_if(renderers.begin(), renderers.end(), [&baseRenderer](const std::unique_ptr<CRendererVK> &renderer){ return (baseRenderer == renderer.get());});

	if (renderer_it != renderers.end())
	{
		CRendererVK *renderer = renderer_it->get();
		vkDeviceWaitIdle(device);

		if (renderer->commandBuffers.size() > 0)
		{
			vkFreeCommandBuffers(device, commandPool, static_cast<uint32_t>(renderer->commandBuffers.size()), renderer->commandBuffers.data());
			renderer->commandBuffers.clear();
		}

		renderers.erase(renderer_it);
	}
}

void CGraphicsVK::DestroySwapChain(CRenderWindow *renderWindow)
{
	DestroyFramebufferAttachmentDescriptor(renderWindow->transparentCompositionSubpass.attachments);
	DestroyFramebufferAttachmentDescriptor(renderWindow->transparentSubpass.attachments);

	DestroyFramebufferAttachment(renderWindow->attachments.color);

	DestroyFramebufferAttachment(renderWindow->attachments.accum);
	DestroyFramebufferAttachment(renderWindow->attachments.revealage);

	DestroyFramebufferAttachment(renderWindow->attachments.accumResolve);
	DestroyFramebufferAttachment(renderWindow->attachments.revealageResolve);

	DestroyFramebufferAttachment(renderWindow->attachments.depth);
	DestroyFramebufferAttachment(renderWindow->attachments.depthResolve);

	for (uint32_t i = 0; i < renderWindow->numSwapChainImages; ++i)
	{
		if (renderWindow->swapChainFramebuffers && renderWindow->swapChainFramebuffers[ i ] != VK_NULL_HANDLE)
			vkDestroyFramebuffer(device, renderWindow->swapChainFramebuffers[ i ], nullptr);

		if (renderWindow->swapChainImageViews && renderWindow->swapChainImageViews[ i ] != VK_NULL_HANDLE)
			vkDestroyImageView(device, renderWindow->swapChainImageViews[ i ], nullptr);
	}

	renderWindow->swapChainFramebuffers.reset();
	renderWindow->swapChainImageViews.reset();
	renderWindow->swapChainImages.reset();

	if (renderWindow->swapChainKHR != VK_NULL_HANDLE)
	{
		vkDestroySwapchainKHR(device, renderWindow->swapChainKHR, nullptr);
		renderWindow->swapChainKHR = VK_NULL_HANDLE;
	}
}

void CGraphicsVK::LoadExtensions(SDL_Window *window)
{
	unsigned int extensionCount = 0;
	SDL_Vulkan_GetInstanceExtensions(window, &extensionCount, nullptr);

	if (extensionCount == 0)
		throw std::runtime_error("[Vulkan]Failed to load required extensions");

	requiredExtensions.resize(extensionCount);
	SDL_Vulkan_GetInstanceExtensions(window, &extensionCount, requiredExtensions.data());

#if VK_DEBUG
	requiredExtensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
	requiredExtensions.push_back(VK_EXT_VALIDATION_FEATURES_EXTENSION_NAME);
#endif

	Log::Print("Required Extensions:\n");

	for (auto &extension : requiredExtensions)
		Log::Print("\t{}\n", extension);
}

void CGraphicsVK::CheckValidationLayerSupport()
{
#if VK_DEBUG
	uint32_t layerCount = 0;
	vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

	std::vector<VkLayerProperties> availableLayers(static_cast<std::size_t>(layerCount));
	vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

	for (auto layerName : validationLayers)
	{
		bool bLayerFound = false;

		for (const auto &layerProperties : availableLayers)
		{
			if (std::strcmp(layerName, layerProperties.layerName) == 0)
			{
				bLayerFound = true;
				break;
			}
		}

		if (!bLayerFound)
			throw std::runtime_error("[Vulkan]Failed to acquire validation layers");
	}
#endif
}

void CGraphicsVK::CreateInstance()
{	
#if VK_DEBUG
	//VkValidationFeatureEnableEXT validationEnableEXT = {};

	VkValidationFeaturesEXT validationEXT = {};
	validationEXT.sType = VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT;
	validationEXT.enabledValidationFeatureCount = 0;
	validationEXT.pEnabledValidationFeatures = nullptr;
#endif

	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = "Thermite";
	appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.pEngineName = "Thermite";
	appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.apiVersion = VK_API_VERSION_1_2;

	VkInstanceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;
	createInfo.enabledExtensionCount = static_cast<uint32_t>(requiredExtensions.size());
	createInfo.ppEnabledExtensionNames = requiredExtensions.data();

#if VK_DEBUG
	createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
	createInfo.ppEnabledLayerNames = validationLayers.data();
	createInfo.pNext = &validationEXT;
#else // !VK_DEBUG
	createInfo.enabledLayerCount = 0;
#endif // VK_DEBUG

	if (vkCreateInstance(&createInfo, nullptr, &instance) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create instance");
	
	volkLoadInstance(instance);

	// Query available extensions
	uint32_t extensionCount = 0;
	vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);

	std::vector< VkExtensionProperties > extensionProperties{ static_cast<size_t>(extensionCount) };
	vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensionProperties.data());

	Log::Print( "Available Extensions:\n" );

	for (const auto &extensionProperty : extensionProperties)
		Log::Print("\t{}\n", extensionProperty.extensionName);
}

void CGraphicsVK::SetupDebugCallback()
{
#if VK_DEBUG
	VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
	createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
	createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
	createInfo.pfnUserCallback = DebugCallback;
	createInfo.pUserData = nullptr;

	if (CreateDebugUtilsMessengerEXT( &createInfo, nullptr ) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to setup debug callback");
#endif
}

void CGraphicsVK::CreateSurface(CRenderWindow *renderWindow)
{
	if (SDL_Vulkan_CreateSurface(renderWindow->windowHandle, instance, &renderWindow->surface) != SDL_TRUE)
		throw std::runtime_error(fmt::format( "[Vulkan]Failed to create window surface: {}", SDL_GetError()));
}

void CGraphicsVK::PickPhysicalDevice(CRenderWindow *renderWindow)
{
	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);

	if (deviceCount == 0)
		throw std::runtime_error("[Vulkan]Failed to find physical device");

	std::vector<VkPhysicalDevice> devices(static_cast< size_t >(deviceCount));
	vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());

	// TODO: Allow for supplying custom physical device rating callback
	auto rateSuitability = [](VkPhysicalDevice device)
	{
		uint32_t iScore = 1;

		VkPhysicalDeviceProperties deviceProperties;
		vkGetPhysicalDeviceProperties(device, &deviceProperties);

		VkPhysicalDeviceFeatures deviceFeatures;
		vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

		if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
			iScore += 1000;
		
		iScore += deviceProperties.limits.maxImageDimension2D;

		return iScore;
	};

	// Ordered map that automatically sorts device candidates by increasing score
	std::multimap<uint32_t, VkPhysicalDevice> candidates;

	for (VkPhysicalDevice &device : devices)
	{
		if (!IsPhysicalDeviceSuitable(renderWindow, device))
			continue;

		uint32_t score = rateSuitability(device);
		candidates.insert(std::make_pair(score, device));
	}

	if (!candidates.empty() && candidates.rbegin()->first > 0)
		physicalDevice = candidates.rbegin()->second;

	if (!physicalDevice)
		throw std::runtime_error("[Vulkan]Failed to find physical device");

	queueFamilyIndices = FindQueueFamilies(renderWindow, physicalDevice);
}

void CGraphicsVK::GetPhysicalDeviceLimits()
{
	VkPhysicalDeviceProperties physicalDeviceProperties;
	vkGetPhysicalDeviceProperties(physicalDevice, &physicalDeviceProperties);

	VkSampleCountFlags counts = physicalDeviceProperties.limits.framebufferColorSampleCounts &
			physicalDeviceProperties.limits.framebufferDepthSampleCounts;

	if (counts & VK_SAMPLE_COUNT_64_BIT) maxMSAASamples = VK_SAMPLE_COUNT_64_BIT;
	else if (counts & VK_SAMPLE_COUNT_32_BIT) maxMSAASamples = VK_SAMPLE_COUNT_32_BIT;
	else if (counts & VK_SAMPLE_COUNT_16_BIT) maxMSAASamples = VK_SAMPLE_COUNT_16_BIT;
	else if (counts & VK_SAMPLE_COUNT_8_BIT) maxMSAASamples = VK_SAMPLE_COUNT_8_BIT;
	else if (counts & VK_SAMPLE_COUNT_4_BIT) maxMSAASamples = VK_SAMPLE_COUNT_4_BIT;
	else if (counts & VK_SAMPLE_COUNT_2_BIT) maxMSAASamples = VK_SAMPLE_COUNT_2_BIT;

	currentMSAASamples = maxMSAASamples;
}

void CGraphicsVK::CreateLogicalDevice()
{
	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;

	std::set<uint32_t> uniqueQueueFamilies =
	{
		queueFamilyIndices.graphicsFamily.value(),
		queueFamilyIndices.presentFamily.value(),
		queueFamilyIndices.computeFamily.value()
	};

	const float queuePriority = 1.0f;
	for (auto &queueFamily : uniqueQueueFamilies)
	{
		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = queueFamily;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;

		queueCreateInfos.push_back(queueCreateInfo);
	}

	// TODO: Revisit this in the future for requesting device features
	VkPhysicalDeviceFeatures deviceFeatures = {};
	deviceFeatures.geometryShader = VK_TRUE;
	deviceFeatures.multiDrawIndirect = VK_TRUE;
	deviceFeatures.samplerAnisotropy = VK_TRUE;
	deviceFeatures.textureCompressionBC = VK_TRUE;
	deviceFeatures.vertexPipelineStoresAndAtomics = VK_TRUE;
	deviceFeatures.sampleRateShading = VK_TRUE;
	deviceFeatures.depthClamp = VK_TRUE;
	deviceFeatures.independentBlend = VK_TRUE;

	VkPhysicalDeviceFeatures availableFeatures = {};
	vkGetPhysicalDeviceFeatures(physicalDevice, &availableFeatures);

	constexpr int featureCount = sizeof(VkPhysicalDeviceFeatures) / sizeof(VkBool32);
	VkBool32 *requested = reinterpret_cast<VkBool32*>(&deviceFeatures);
	VkBool32 *avail = reinterpret_cast<VkBool32*>(&availableFeatures);
	for (int i = 0; i < featureCount; ++i)
	{
		if (!avail[i] && requested[i])
			throw std::runtime_error("Required physical device features unavailable!");
	}

	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
	createInfo.pQueueCreateInfos = queueCreateInfos.data();
	createInfo.pEnabledFeatures = &deviceFeatures;

	createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
	createInfo.ppEnabledExtensionNames = deviceExtensions.data();

#if VK_DEBUG
	createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
	createInfo.ppEnabledLayerNames = validationLayers.data();
#else // !VK_DEBUG
	createInfo.enabledLayerCount = 0;
#endif // VK_DEBUG

	if (vkCreateDevice(physicalDevice, &createInfo, nullptr, &device) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create logical device");
	
	volkLoadDevice(device);

	vkGetDeviceQueue(device, queueFamilyIndices.graphicsFamily.value(), 0, &graphicsQueue);
	vkGetDeviceQueue(device, queueFamilyIndices.presentFamily.value(), 0, &presentQueue);
	vkGetDeviceQueue(device, queueFamilyIndices.computeFamily.value(), 0, &computeQueue);
}

void CGraphicsVK::CreateVmaAllocator()
{
	const VmaVulkanFunctions vulkanFunctions = {
		.vkGetPhysicalDeviceProperties = vkGetPhysicalDeviceProperties,
		.vkGetPhysicalDeviceMemoryProperties = vkGetPhysicalDeviceMemoryProperties,
		.vkAllocateMemory = vkAllocateMemory,
		.vkFreeMemory = vkFreeMemory,
		.vkMapMemory = vkMapMemory,
		.vkUnmapMemory = vkUnmapMemory,
		.vkFlushMappedMemoryRanges = vkFlushMappedMemoryRanges,
		.vkInvalidateMappedMemoryRanges = vkInvalidateMappedMemoryRanges,
		.vkBindBufferMemory = vkBindBufferMemory,
		.vkBindImageMemory = vkBindImageMemory,
		.vkGetBufferMemoryRequirements = vkGetBufferMemoryRequirements,
		.vkGetImageMemoryRequirements = vkGetImageMemoryRequirements,
		.vkCreateBuffer = vkCreateBuffer,
		.vkDestroyBuffer = vkDestroyBuffer,
		.vkCreateImage = vkCreateImage,
		.vkDestroyImage = vkDestroyImage,
		.vkCmdCopyBuffer = vkCmdCopyBuffer,
#if VMA_DEDICATED_ALLOCATION || VMA_VULKAN_VERSION >= 1001000
		.vkGetBufferMemoryRequirements2KHR = vkGetBufferMemoryRequirements2KHR,
		.vkGetImageMemoryRequirements2KHR = vkGetImageMemoryRequirements2KHR,
#endif
#if VMA_BIND_MEMORY2 || VMA_VULKAN_VERSION >= 1001000
		.vkBindBufferMemory2KHR = vkBindBufferMemory2KHR,
		.vkBindImageMemory2KHR = vkBindImageMemory2KHR,
#endif
#if VMA_MEMORY_BUDGET || VMA_VULKAN_VERSION >= 1001000
		.vkGetPhysicalDeviceMemoryProperties2KHR = vkGetPhysicalDeviceMemoryProperties2KHR
#endif
	};

	VmaAllocatorCreateInfo allocatorInfo = {
		.physicalDevice = physicalDevice,
		.device = device,
		.pVulkanFunctions = &vulkanFunctions,
		.instance = instance
	};

	vmaCreateAllocator(&allocatorInfo, &allocator);
}

void CGraphicsVK::CreateCommandPools()
{
	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily.value();
	poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT; // Optional

	if (vkCreateCommandPool(device, &poolInfo, nullptr, &commandPool) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create command pool");

	poolInfo.queueFamilyIndex = queueFamilyIndices.computeFamily.value();

	if (vkCreateCommandPool(device, &poolInfo, nullptr, &computeCommandPool) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create command pool");
}

void CGraphicsVK::CreateSwapChain(CRenderWindow *renderWindow)
{
	int width = 0;
	int height = 0;

	SDL_Vulkan_GetDrawableSize(renderWindow->windowHandle, &width, &height);
	Log::Print("width: {}, height: {}\n", width, height);

	auto chooseSwapSurfaceFormat = [](const std::vector< VkSurfaceFormatKHR > &availableFormats)
	{
		if (availableFormats.size() == 1 && availableFormats[ 0 ].format == VK_FORMAT_UNDEFINED)
			return VkSurfaceFormatKHR{ VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };

		for (const auto &format : availableFormats)
		{
			if (format.format == VK_FORMAT_B8G8R8A8_UNORM && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
				return format;
		}

		return availableFormats[ 0 ];
	};

	// TODO: Revisit this in the future for VSync options
	auto chooseSwapPresentMode = [](const std::vector< VkPresentModeKHR > &availablePresentModes)
	{
		auto selectedMode = VK_PRESENT_MODE_FIFO_KHR;

		for (const auto &presentMode : availablePresentModes)
		{
			if (presentMode == VK_PRESENT_MODE_MAILBOX_KHR)
				return presentMode;
			else if (presentMode == VK_PRESENT_MODE_IMMEDIATE_KHR)
				selectedMode = presentMode;
		}

		return selectedMode;
	};

	auto chooseSwapExtent = [ &width, &height ](const VkSurfaceCapabilitiesKHR &capabilities)
	{
		if (width == 0 || height == 0)
			throw std::runtime_error("[Vulkan] Window created with size of 0!");

		VkExtent2D actualExtent = { static_cast<uint32_t>(width), static_cast<uint32_t>(height) };

		actualExtent.width = std::clamp(actualExtent.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
		actualExtent.height = std::clamp(actualExtent.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);

		if (actualExtent.width == 0 || actualExtent.height == 0)
			throw std::runtime_error("[Vulkan]Swap chain capabilities clamped extent to 0!");

		return actualExtent;
	};

	auto swapChainSupport = QuerySwapChainSupport(renderWindow, physicalDevice);

	VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
	VkPresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
	VkExtent2D extent = chooseSwapExtent(swapChainSupport.capabilities);

	if (!isInitialized)
		swapChainImageFormat = surfaceFormat.format;

	assert(swapChainImageFormat == surfaceFormat.format);

	uint32_t minImageCount = swapChainSupport.capabilities.maxImageCount > 0 ?
		std::min(swapChainSupport.capabilities.minImageCount + 1, swapChainSupport.capabilities.maxImageCount) :
		swapChainSupport.capabilities.minImageCount + 1;

	VkSwapchainCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = renderWindow->surface;

	createInfo.minImageCount = minImageCount;
	createInfo.imageFormat = surfaceFormat.format;
	createInfo.imageColorSpace = surfaceFormat.colorSpace;
	createInfo.imageExtent = extent;
	createInfo.imageArrayLayers = 1;
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

	const uint32_t indices[] =
	{
		queueFamilyIndices.graphicsFamily.value(),
		queueFamilyIndices.presentFamily.value()
	};

	if (indices[ 0 ] != indices[ 1 ])
	{
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices = indices;
	}
	else
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;

	createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR; // TODO: Revisit this for window blending
	createInfo.presentMode = presentMode;
	createInfo.clipped = VK_TRUE;
	createInfo.oldSwapchain = VK_NULL_HANDLE;

	if (vkCreateSwapchainKHR(device, &createInfo, nullptr, &renderWindow->swapChainKHR) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create swap chain");

	// Get the actual number of swap chain images we will use
	vkGetSwapchainImagesKHR(device, renderWindow->swapChainKHR, &renderWindow->numSwapChainImages, nullptr);

	// Allocate our elements dependant on the number of swap chain images
	renderWindow->swapChainImages = std::make_unique<VkImage[]>(renderWindow->numSwapChainImages);
	renderWindow->swapChainImageViews = std::make_unique<VkImageView[]>(renderWindow->numSwapChainImages);
	renderWindow->swapChainFramebuffers = std::make_unique<VkFramebuffer[]>(renderWindow->numSwapChainImages);

	// Initialize everything to VK_NULL_HANDLE
	for (uint32_t i = 0; i < renderWindow->numSwapChainImages; ++i)
	{
		renderWindow->swapChainImages[ i ] = VK_NULL_HANDLE;
		renderWindow->swapChainImageViews[ i ] = VK_NULL_HANDLE;
		renderWindow->swapChainFramebuffers[ i ] = VK_NULL_HANDLE;
	}

	vkGetSwapchainImagesKHR(device, renderWindow->swapChainKHR, &renderWindow->numSwapChainImages, &renderWindow->swapChainImages[ 0 ]);

	renderWindow->swapChainExtent = extent;
	renderWindow->horizontalAspectRatio = (float)extent.width / (float)extent.height;
	renderWindow->verticalAspectRatio = (float)extent.height / (float)extent.width;

	renderWindow->orthoProjection = glm::ortho(0.0f, (float)extent.width, 0.0f, (float)extent.height);
}

void CGraphicsVK::CreateSwapChainImageViews(CRenderWindow *renderWindow)
{
	for (uint32_t i = 0; i < renderWindow->numSwapChainImages; ++i)
		renderWindow->swapChainImageViews[ i ] = CreateImageView2D(renderWindow->swapChainImages[ i ], swapChainImageFormat, VK_IMAGE_ASPECT_COLOR_BIT, 1);
}

void CGraphicsVK::FindDepthFormat()
{
	auto findSupportedFormat = [this](const std::vector< VkFormat > &candidates, VkImageTiling tiling, VkFormatFeatureFlags features)
	{
		for (const auto &format : candidates)
		{
			VkFormatProperties props = {};
			vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &props);

			if ( tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features ) == features)
				return format;
			else if ( tiling == VK_IMAGE_TILING_OPTIMAL && ( props.optimalTilingFeatures & features ) == features )
				return format;
		}

		return VK_FORMAT_UNDEFINED;
	};

	depthFormat = findSupportedFormat(
		{ VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT },
		VK_IMAGE_TILING_OPTIMAL,
		VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
	);

	if (depthFormat == VK_FORMAT_UNDEFINED) {
		throw std::runtime_error("[Vulkan]Failed to find supported depth format");
	}
}

void CGraphicsVK::CreateRenderPass()
{
	std::array<VkAttachmentDescription, 4> attachments = {};

	auto &colorAttachment = attachments[0];
	colorAttachment.format = swapChainImageFormat;
	colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
	colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	auto &accumAttachment = attachments[1];
	accumAttachment.format = VK_FORMAT_R16G16B16A16_SFLOAT;
	accumAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
	accumAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	accumAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	accumAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	accumAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	accumAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	accumAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	auto &revealageAttachment = attachments[2];
	revealageAttachment.format = VK_FORMAT_R16_SFLOAT;
	revealageAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
	revealageAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	revealageAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	revealageAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	revealageAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	revealageAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	revealageAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	auto &depthAttachment = attachments[3];
	depthAttachment.format = depthFormat;
	depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
	depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkAttachmentReference colorAttachmentRef = { 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };
	VkAttachmentReference accumAttachmentRef = { 1, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };
	VkAttachmentReference revealageAttachmentRef = { 2, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL };

	VkAttachmentReference depthAttachmentRef = { 3, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL };
	VkAttachmentReference depthAttachmentReadOnlyRef = { 3, VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL };

	VkAttachmentReference depthInputRef = { 3, VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL };

	VkAttachmentReference colorInputReferences[2] = {};
	colorInputReferences[0] = { 1, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL };
	colorInputReferences[1] = { 2, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL };

	std::array<VkSubpassDescription, 4> subpassDescriptions = {};

	uint32_t opaquepassPreserve[] = { 1, 2 };

	VkSubpassDescription &opaquepassDescription = subpassDescriptions[0];
	opaquepassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	opaquepassDescription.colorAttachmentCount = 1;
	opaquepassDescription.pColorAttachments = &colorAttachmentRef;
	opaquepassDescription.pDepthStencilAttachment = &depthAttachmentRef;
	opaquepassDescription.preserveAttachmentCount = 2;
	opaquepassDescription.pPreserveAttachments = opaquepassPreserve;

	VkAttachmentReference transparentpassRefs[] = { colorAttachmentRef, accumAttachmentRef, revealageAttachmentRef };

	VkSubpassDescription &transparentpassDescription = subpassDescriptions[1];
	transparentpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	transparentpassDescription.colorAttachmentCount = 3;
	transparentpassDescription.pColorAttachments = transparentpassRefs;
	transparentpassDescription.pDepthStencilAttachment = &depthAttachmentReadOnlyRef;
	transparentpassDescription.inputAttachmentCount = 1;
	transparentpassDescription.pInputAttachments = &depthInputRef;

	VkSubpassDescription &compositionpassDescription = subpassDescriptions[2];
	compositionpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	compositionpassDescription.colorAttachmentCount = 1;
	compositionpassDescription.pColorAttachments = &colorAttachmentRef;
	compositionpassDescription.pDepthStencilAttachment = &depthAttachmentReadOnlyRef;
	compositionpassDescription.inputAttachmentCount = 2;
	compositionpassDescription.pInputAttachments = colorInputReferences;

	VkSubpassDescription &uipassDescription = subpassDescriptions[3];
	uipassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	uipassDescription.colorAttachmentCount = 1;
	uipassDescription.pColorAttachments = &colorAttachmentRef;
	uipassDescription.pDepthStencilAttachment = &depthAttachmentReadOnlyRef;

	std::array<VkSubpassDependency, 5> subpassDependencies = {};

	VkSubpassDependency &opaquepassDependency = subpassDependencies[0];
	opaquepassDependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	opaquepassDependency.dstSubpass = 0;
	opaquepassDependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	opaquepassDependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	opaquepassDependency.srcAccessMask = 0;
	opaquepassDependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	opaquepassDependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	VkSubpassDependency &transparentpassDependency = subpassDependencies[1];
	transparentpassDependency.srcSubpass = 0;
	transparentpassDependency.dstSubpass = 1;
	transparentpassDependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	transparentpassDependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	transparentpassDependency.srcAccessMask = 0;
	transparentpassDependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	transparentpassDependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	VkSubpassDependency &transparentpassSelfDependency = subpassDependencies[2];
	transparentpassSelfDependency.srcSubpass = 1;
	transparentpassSelfDependency.dstSubpass = 1;
	transparentpassSelfDependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	transparentpassSelfDependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	transparentpassSelfDependency.srcAccessMask = 0;
	transparentpassSelfDependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	transparentpassSelfDependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	VkSubpassDependency &compositionpassDependency = subpassDependencies[3];
	compositionpassDependency.srcSubpass = 1;
	compositionpassDependency.dstSubpass = 2;
	compositionpassDependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	compositionpassDependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	compositionpassDependency.srcAccessMask = 0;
	compositionpassDependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	compositionpassDependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	VkSubpassDependency &uipassDependency = subpassDependencies[4];
	uipassDependency.srcSubpass = 2;
	uipassDependency.dstSubpass = 3;
	uipassDependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	uipassDependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	uipassDependency.srcAccessMask = 0;
	uipassDependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	uipassDependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	VkRenderPassCreateInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = (uint32_t)attachments.size();
	renderPassInfo.pAttachments = attachments.data();
	renderPassInfo.subpassCount = (uint32_t)subpassDescriptions.size();
	renderPassInfo.pSubpasses = subpassDescriptions.data();
	renderPassInfo.dependencyCount = (uint32_t)subpassDependencies.size();
	renderPassInfo.pDependencies = subpassDependencies.data();

	if (vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderPass) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create render pass");
}

void CGraphicsVK::CreateRenderPassMSAA()
{
	// NOTE: We're using the '2' variants of Vulkan's structures here.
	// This is so we can setup a resolve attachment for the depth buffer.
	// This is only available in Vulkan 1.2, but is available as an extension in earlier versions.
	// We may need to do some tweaking here if in the future we want to run on mobile with older versions of Vulkan.

	std::array<VkAttachmentDescription2, 8> attachments = {};

	for (auto &attachment : attachments)
	{
		attachment.sType = VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2;
	}

	auto &colorAttachment = attachments[0];
	colorAttachment.format = swapChainImageFormat;
	colorAttachment.samples = currentMSAASamples;
	colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colorAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	auto &accumAttachment = attachments[1];
	accumAttachment.format = VK_FORMAT_R16G16B16A16_SFLOAT;
	accumAttachment.samples = currentMSAASamples;
	accumAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	accumAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	accumAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	accumAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	accumAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	accumAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	auto &revealageAttachment = attachments[2];
	revealageAttachment.format = VK_FORMAT_R16_SFLOAT;
	revealageAttachment.samples = currentMSAASamples;
	revealageAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	revealageAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	revealageAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	revealageAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	revealageAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	revealageAttachment.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	auto &depthAttachment = attachments[3];
	depthAttachment.format = depthFormat;
	depthAttachment.samples = currentMSAASamples;
	depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	auto &colorAttachmentResolve = attachments[4];
	colorAttachmentResolve.format = swapChainImageFormat;
	colorAttachmentResolve.samples = VK_SAMPLE_COUNT_1_BIT;
	colorAttachmentResolve.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachmentResolve.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachmentResolve.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachmentResolve.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachmentResolve.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colorAttachmentResolve.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	auto &accumAttachmentResolve = attachments[5];
	accumAttachmentResolve.format = VK_FORMAT_R16G16B16A16_SFLOAT;
	accumAttachmentResolve.samples = VK_SAMPLE_COUNT_1_BIT;
	accumAttachmentResolve.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	accumAttachmentResolve.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	accumAttachmentResolve.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	accumAttachmentResolve.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	accumAttachmentResolve.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	accumAttachmentResolve.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	auto &revealageAttachmentResolve = attachments[6];
	revealageAttachmentResolve.format = VK_FORMAT_R16_SFLOAT;
	revealageAttachmentResolve.samples = VK_SAMPLE_COUNT_1_BIT;
	revealageAttachmentResolve.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	revealageAttachmentResolve.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	revealageAttachmentResolve.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	revealageAttachmentResolve.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	revealageAttachmentResolve.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	revealageAttachmentResolve.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	auto &depthAttachmentResolve = attachments[7];
	depthAttachmentResolve.format = depthFormat;
	depthAttachmentResolve.samples = VK_SAMPLE_COUNT_1_BIT;
	depthAttachmentResolve.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	depthAttachmentResolve.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	depthAttachmentResolve.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	depthAttachmentResolve.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	depthAttachmentResolve.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	depthAttachmentResolve.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkAttachmentReference2 colorAttachmentRef = {};
	colorAttachmentRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
	colorAttachmentRef.attachment = 0;
	colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkAttachmentReference2 accumAttachmentRef = {};
	accumAttachmentRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
	accumAttachmentRef.attachment = 1;
	accumAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkAttachmentReference2 revealageAttachmentRef = {};
	revealageAttachmentRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
	revealageAttachmentRef.attachment = 2;
	revealageAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkAttachmentReference2 depthAttachmentRef = {};
	depthAttachmentRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
	depthAttachmentRef.attachment = 3;
	depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkAttachmentReference2 depthAttachmentReadOnlyRef = {};
	depthAttachmentReadOnlyRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
	depthAttachmentReadOnlyRef.attachment = 3;
	depthAttachmentReadOnlyRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

	VkAttachmentReference2 depthInputRef = {};
	depthInputRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
	depthInputRef.attachment = 7;
	depthInputRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
	depthInputRef.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

	VkAttachmentReference2 colorInputReferences[2] = {};
	colorInputReferences[0].sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
	colorInputReferences[0].attachment = 5;
	colorInputReferences[0].layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	colorInputReferences[0].aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

	colorInputReferences[1].sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
	colorInputReferences[1].attachment = 6;
	colorInputReferences[1].layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	colorInputReferences[1].aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

	VkAttachmentReference2 colorAttachmentResolveRef = {};
	colorAttachmentResolveRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
	colorAttachmentResolveRef.attachment = 4;
	colorAttachmentResolveRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkAttachmentReference2 accumAttachmentResolveRef = {};
	accumAttachmentResolveRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
	accumAttachmentResolveRef.attachment = 5;
	accumAttachmentResolveRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkAttachmentReference2 revealageAttachmentResolveRef = {};
	revealageAttachmentResolveRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
	revealageAttachmentResolveRef.attachment = 6;
	revealageAttachmentResolveRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkAttachmentReference2 depthAttachmentResolveRef = {};
	depthAttachmentResolveRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
	depthAttachmentResolveRef.attachment = 7;
	depthAttachmentResolveRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkAttachmentReference2 depthAttachmentReadOnlyResolveRef = {};
	depthAttachmentReadOnlyResolveRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
	depthAttachmentReadOnlyResolveRef.attachment = 7;
	depthAttachmentReadOnlyResolveRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

	VkSubpassDescriptionDepthStencilResolve depthResolveDescription = {};
	depthResolveDescription.sType = VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_DEPTH_STENCIL_RESOLVE;
	depthResolveDescription.depthResolveMode = VK_RESOLVE_MODE_AVERAGE_BIT;
	depthResolveDescription.stencilResolveMode = VK_RESOLVE_MODE_NONE;
	depthResolveDescription.pDepthStencilResolveAttachment = &depthAttachmentResolveRef;

	VkSubpassDescriptionDepthStencilResolve depthReadOnlyResolveDescription = depthResolveDescription;
	depthReadOnlyResolveDescription.pDepthStencilResolveAttachment = &depthAttachmentReadOnlyResolveRef;

	std::array<VkSubpassDescription2, 4> subpassDescriptions = {};

	for (auto &description : subpassDescriptions)
		description.sType = VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_2;

	uint32_t opaquepassPreserve[] = { 1, 2 };

	VkSubpassDescription2 &opaquepassDescription = subpassDescriptions[0];
	opaquepassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	opaquepassDescription.colorAttachmentCount = 1;
	opaquepassDescription.pColorAttachments = &colorAttachmentRef;
	opaquepassDescription.pResolveAttachments = &colorAttachmentResolveRef;
	opaquepassDescription.pNext = &depthResolveDescription;
	opaquepassDescription.pDepthStencilAttachment = &depthAttachmentRef;
	opaquepassDescription.preserveAttachmentCount = 2;
	opaquepassDescription.pPreserveAttachments = opaquepassPreserve;

	VkAttachmentReference2 transparentpassRefs[] = { colorAttachmentRef, accumAttachmentRef, revealageAttachmentRef };
	VkAttachmentReference2 transparentpassResolveRefs[] = { colorAttachmentResolveRef, accumAttachmentResolveRef, revealageAttachmentResolveRef };

	VkSubpassDescription2 &transparentpassDescription = subpassDescriptions[1];
	transparentpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	transparentpassDescription.colorAttachmentCount = 3;
	transparentpassDescription.pColorAttachments = transparentpassRefs;
	transparentpassDescription.pDepthStencilAttachment = &depthAttachmentReadOnlyRef;
	transparentpassDescription.inputAttachmentCount = 1;
	transparentpassDescription.pInputAttachments = &depthInputRef;
	transparentpassDescription.pResolveAttachments = transparentpassResolveRefs;
	transparentpassDescription.pNext = &depthReadOnlyResolveDescription;

	VkSubpassDescription2 &compositionpassDescription = subpassDescriptions[2];
	compositionpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	compositionpassDescription.colorAttachmentCount = 1;
	compositionpassDescription.pColorAttachments = &colorAttachmentRef;
	compositionpassDescription.pDepthStencilAttachment = &depthAttachmentReadOnlyRef;
	compositionpassDescription.inputAttachmentCount = 2;
	compositionpassDescription.pInputAttachments = colorInputReferences;
	compositionpassDescription.pNext = &depthReadOnlyResolveDescription;

	VkSubpassDescription2 &uipassDescription = subpassDescriptions[3];
	uipassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	uipassDescription.colorAttachmentCount = 1;
	uipassDescription.pColorAttachments = &colorAttachmentRef;
	uipassDescription.pResolveAttachments = &colorAttachmentResolveRef;
	uipassDescription.pNext = &depthReadOnlyResolveDescription;
	uipassDescription.pDepthStencilAttachment = &depthAttachmentReadOnlyRef;

	std::array<VkSubpassDependency2, 5> subpassDependencies = {};

	for (auto &dependency : subpassDependencies)
		dependency.sType = VK_STRUCTURE_TYPE_SUBPASS_DEPENDENCY_2;

	VkSubpassDependency2 &opaquepassDependency = subpassDependencies[0];
	opaquepassDependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	opaquepassDependency.dstSubpass = 0;
	opaquepassDependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	opaquepassDependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	opaquepassDependency.srcAccessMask = 0;
	opaquepassDependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	opaquepassDependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	VkSubpassDependency2 &transparentpassDependency = subpassDependencies[1];
	transparentpassDependency.srcSubpass = 0;
	transparentpassDependency.dstSubpass = 1;
	transparentpassDependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	transparentpassDependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	transparentpassDependency.srcAccessMask = 0;
	transparentpassDependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	transparentpassDependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	VkSubpassDependency2 &transparentpassSelfDependency = subpassDependencies[2];
	transparentpassSelfDependency.srcSubpass = 1;
	transparentpassSelfDependency.dstSubpass = 1;
	transparentpassSelfDependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	transparentpassSelfDependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	transparentpassSelfDependency.srcAccessMask = 0;
	transparentpassSelfDependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	transparentpassSelfDependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	VkSubpassDependency2 &compositionpassDependency = subpassDependencies[3];
	compositionpassDependency.srcSubpass = 1;
	compositionpassDependency.dstSubpass = 2;
	compositionpassDependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	compositionpassDependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	compositionpassDependency.srcAccessMask = 0;
	compositionpassDependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	compositionpassDependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	VkSubpassDependency2 &uipassDependency = subpassDependencies[4];
	uipassDependency.srcSubpass = 2;
	uipassDependency.dstSubpass = 3;
	uipassDependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	uipassDependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	uipassDependency.srcAccessMask = 0;
	uipassDependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	uipassDependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	VkRenderPassCreateInfo2 renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
	renderPassInfo.attachmentCount = (uint32_t)attachments.size();
	renderPassInfo.pAttachments = attachments.data();
	renderPassInfo.subpassCount = (uint32_t)subpassDescriptions.size();
	renderPassInfo.pSubpasses = subpassDescriptions.data();
	renderPassInfo.dependencyCount = (uint32_t)subpassDependencies.size();
	renderPassInfo.pDependencies = subpassDependencies.data();

	if (vkCreateRenderPass2(device, &renderPassInfo, nullptr, &renderPass) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create render pass");
}

void CGraphicsVK::CreateColorResources(CRenderWindow *renderWindow)
{
	const VkFormat format = swapChainImageFormat;
	const VkImageUsageFlags imageUsage = VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	const VkImageAspectFlags aspectFlags = VK_IMAGE_ASPECT_COLOR_BIT;

	renderWindow->attachments.color = CreateFramebufferAttachment(renderWindow, format, imageUsage, aspectFlags, currentMSAASamples);
}

void CGraphicsVK::CreateAccumResources(CRenderWindow *renderWindow)
{
	const VkFormat format = VK_FORMAT_R16G16B16A16_SFLOAT;
	const VkImageUsageFlags imageUsage = VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT;
	const VkImageAspectFlags aspectFlags = VK_IMAGE_ASPECT_COLOR_BIT;

	renderWindow->attachments.accum = CreateFramebufferAttachment(renderWindow, format, imageUsage, aspectFlags, currentMSAASamples);

	if (IsMSAAEnabled())
		renderWindow->attachments.accumResolve = CreateFramebufferAttachment(renderWindow, format, imageUsage, aspectFlags, VK_SAMPLE_COUNT_1_BIT);
}

void CGraphicsVK::CreateRevealageResources(CRenderWindow *renderWindow)
{
	const VkFormat format = VK_FORMAT_R16_SFLOAT;
	const VkImageUsageFlags imageUsage = VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT;
	const VkImageAspectFlags aspectFlags = VK_IMAGE_ASPECT_COLOR_BIT;

	renderWindow->attachments.revealage = CreateFramebufferAttachment(renderWindow, format, imageUsage, aspectFlags, currentMSAASamples);

	if (IsMSAAEnabled())
		renderWindow->attachments.revealageResolve = CreateFramebufferAttachment(renderWindow, format, imageUsage, aspectFlags, VK_SAMPLE_COUNT_1_BIT);
}

void CGraphicsVK::CreateDepthResources(CRenderWindow *renderWindow)
{
	const VkFormat format = depthFormat;
	const VkImageUsageFlags imageUsage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT;
	const VkImageAspectFlags aspectFlags = VK_IMAGE_ASPECT_DEPTH_BIT;

	renderWindow->attachments.depth = CreateFramebufferAttachment(renderWindow, format, imageUsage, aspectFlags, currentMSAASamples);
	TransitionImageLayoutImmediate(renderWindow->attachments.depth.image, format, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, 1);

	renderWindow->attachments.depth.imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;

	if (IsMSAAEnabled())
		renderWindow->attachments.depthResolve = CreateFramebufferAttachment(renderWindow, format, imageUsage, aspectFlags, VK_SAMPLE_COUNT_1_BIT);;

	renderWindow->attachments.depthResolve.imageLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
}

void CGraphicsVK::CreateFramebuffers(CRenderWindow *renderWindow)
{
	if (currentMSAASamples == VK_SAMPLE_COUNT_1_BIT)
	{
		for (uint32_t i = 0; i < renderWindow->numSwapChainImages; ++i)
		{
			std::array<VkImageView, 4> attachments = {
				renderWindow->swapChainImageViews[i],
				renderWindow->attachments.accum.imageView,
				renderWindow->attachments.revealage.imageView,
				renderWindow->attachments.depth.imageView,
			};

			VkFramebufferCreateInfo framebufferInfo = {};
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.renderPass = renderPass;
			framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
			framebufferInfo.pAttachments = attachments.data();
			framebufferInfo.width = renderWindow->swapChainExtent.width;
			framebufferInfo.height = renderWindow->swapChainExtent.height;
			framebufferInfo.layers = 1;

			if (vkCreateFramebuffer(device, &framebufferInfo, nullptr, &renderWindow->swapChainFramebuffers[i]) != VK_SUCCESS)
				throw std::runtime_error("[Vulkan]Failed to create framebuffers");
		}
	}
	else
	{
		for (uint32_t i = 0; i < renderWindow->numSwapChainImages; ++i)
		{
			std::array<VkImageView, 8> attachments = {
				renderWindow->attachments.color.imageView,
				renderWindow->attachments.accum.imageView,
				renderWindow->attachments.revealage.imageView,
				renderWindow->attachments.depth.imageView,
				renderWindow->swapChainImageViews[i],
				renderWindow->attachments.accumResolve.imageView,
				renderWindow->attachments.revealageResolve.imageView,
				renderWindow->attachments.depthResolve.imageView
			};

			VkFramebufferCreateInfo framebufferInfo = {};
			framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			framebufferInfo.renderPass = renderPass;
			framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
			framebufferInfo.pAttachments = attachments.data();
			framebufferInfo.width = renderWindow->swapChainExtent.width;
			framebufferInfo.height = renderWindow->swapChainExtent.height;
			framebufferInfo.layers = 1;

			if (vkCreateFramebuffer(device, &framebufferInfo, nullptr, &renderWindow->swapChainFramebuffers[i]) != VK_SUCCESS)
				throw std::runtime_error("[Vulkan]Failed to create framebuffers");
		}
	}
}

void CGraphicsVK::CreateSyncObjects(CRenderWindow *renderWindow)
{
	renderWindow->imagesInFlight.resize(renderWindow->numSwapChainImages, VK_NULL_HANDLE);

	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkFenceCreateInfo fenceInfo = {};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	for ( std::size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i )
	{
		if (vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderWindow->imageAvailableSemaphores[ i ]) != VK_SUCCESS)
			throw std::runtime_error("[Vulkan]Failed to create sync objects");

		if (vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderWindow->renderFinishedSemaphores[ i ]) != VK_SUCCESS)
			throw std::runtime_error("[Vulkan]Failed to create sync objects");

		if (vkCreateFence(device, &fenceInfo, nullptr, &renderWindow->inFlightFences[ i ]) != VK_SUCCESS)
			throw std::runtime_error("[Vulkan]Failed to create sync objects");
	}
}

void CGraphicsVK::CreateFramebufferDescriptorSetLayouts()
{
	std::vector<VkDescriptorSetLayoutBinding> depthOnlyBindings =
	{
		{
			.binding = 0,
			.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,
			.descriptorCount = 1,
			.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
			.pImmutableSamplers = nullptr
		}
	};

	std::vector<VkDescriptorSetLayoutBinding> accumRevealageBindings =
	{
		{
			.binding = 0,
			.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,
			.descriptorCount = 1,
			.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
			.pImmutableSamplers = nullptr
		},
		{
			.binding = 1,
			.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,
			.descriptorCount = 1,
			.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT,
			.pImmutableSamplers = nullptr
		}
	};

	VkDescriptorSetLayoutCreateInfo layoutInfo = {};
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = static_cast<uint32_t>(depthOnlyBindings.size());
	layoutInfo.pBindings = depthOnlyBindings.data();

	if (vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &descriptorSetLayouts.depthOnly) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create descriptor set layout");

	layoutInfo.bindingCount = static_cast<uint32_t>(accumRevealageBindings.size());
	layoutInfo.pBindings = accumRevealageBindings.data();

	if (vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &descriptorSetLayouts.accumRevealage) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create descriptor set layout");
}

void CGraphicsVK::CreateFramebufferDescriptorSets(CRenderWindow *renderWindow)
{
	if (currentMSAASamples == VK_SAMPLE_COUNT_1_BIT)
	{
		renderWindow->transparentCompositionSubpass.attachments = CreateFramebufferAttachmentDescriptor(
			{
				&renderWindow->attachments.accum,
				&renderWindow->attachments.revealage
			},

			descriptorSetLayouts.accumRevealage
		);

		renderWindow->transparentSubpass.attachments = CreateFramebufferAttachmentDescriptor(
			{
				&renderWindow->attachments.depth
			},

			descriptorSetLayouts.depthOnly
		);
	}
	else
	{
		renderWindow->transparentCompositionSubpass.attachments = CreateFramebufferAttachmentDescriptor(
			{
				&renderWindow->attachments.accumResolve,
				&renderWindow->attachments.revealageResolve
			},

			descriptorSetLayouts.accumRevealage
		);

		renderWindow->transparentSubpass.attachments = CreateFramebufferAttachmentDescriptor(
			{
				&renderWindow->attachments.depthResolve
			},

			descriptorSetLayouts.depthOnly
		);
	}
}

void CGraphicsVK::SetupImGuiContext(CRenderWindow *renderWindow)
{
	ThermiteImGui::SetCurrentContext(renderWindow->guiContext);

	ImGui::StyleColorsDark();
	ImGuiIO &io = ImGui::GetIO();

	// NOTE: The input service relies on this userdata
	io.UserData = renderWindow->windowHandle;

	io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors; // We can honor GetMouseCursor() values (optional)
	//io.BackendFlags |= ImGuiBackendFlags_HasSetMousePos; // We can honor io.WantSetMousePos requests (optional, rarely used)

	// NOTE: These are purely informational and entirely optional
	io.BackendPlatformName = "imgui_impl_thermite";
	io.BackendRendererName = "imgui_impl_vulkan";

	// Keyboard mapping. ImGui will use those indices to peek into the io.KeysDown[] array.
	io.KeyMap[ImGuiKey_Tab] = SDL_SCANCODE_TAB;
	io.KeyMap[ImGuiKey_LeftArrow] = SDL_SCANCODE_LEFT;
	io.KeyMap[ImGuiKey_RightArrow] = SDL_SCANCODE_RIGHT;
	io.KeyMap[ImGuiKey_UpArrow] = SDL_SCANCODE_UP;
	io.KeyMap[ImGuiKey_DownArrow] = SDL_SCANCODE_DOWN;
	io.KeyMap[ImGuiKey_PageUp] = SDL_SCANCODE_PAGEUP;
	io.KeyMap[ImGuiKey_PageDown] = SDL_SCANCODE_PAGEDOWN;
	io.KeyMap[ImGuiKey_Home] = SDL_SCANCODE_HOME;
	io.KeyMap[ImGuiKey_End] = SDL_SCANCODE_END;
	io.KeyMap[ImGuiKey_Insert] = SDL_SCANCODE_INSERT;
	io.KeyMap[ImGuiKey_Delete] = SDL_SCANCODE_DELETE;
	io.KeyMap[ImGuiKey_Backspace] = SDL_SCANCODE_BACKSPACE;
	io.KeyMap[ImGuiKey_Space] = SDL_SCANCODE_SPACE;
	io.KeyMap[ImGuiKey_Enter] = SDL_SCANCODE_RETURN;
	io.KeyMap[ImGuiKey_Escape] = SDL_SCANCODE_ESCAPE;
	io.KeyMap[ImGuiKey_KeyPadEnter] = SDL_SCANCODE_KP_ENTER;
	io.KeyMap[ImGuiKey_A] = SDL_SCANCODE_A;
	io.KeyMap[ImGuiKey_C] = SDL_SCANCODE_C;
	io.KeyMap[ImGuiKey_V] = SDL_SCANCODE_V;
	io.KeyMap[ImGuiKey_X] = SDL_SCANCODE_X;
	io.KeyMap[ImGuiKey_Y] = SDL_SCANCODE_Y;
	io.KeyMap[ImGuiKey_Z] = SDL_SCANCODE_Z;

	input->SetupImGuiContext(renderWindow->guiContext);

	io.Fonts->AddFontDefault();
	unsigned char *pixels = nullptr;
	int width = 0;
	int height = 0;

	io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);

	renderWindow->fontMaterial = CreateGuiMaterial(renderWindow, pixels, width, height);
	io.Fonts->TexID = renderWindow->fontMaterial;
}

FramebufferAttachment CGraphicsVK::CreateFramebufferAttachment(CRenderWindow *renderWindow, VkFormat format, VkImageUsageFlags imageUsage, VkImageAspectFlags aspectFlags, VkSampleCountFlagBits numSamples)
{
	FramebufferAttachment attachment;

	attachment.image = CreateImage2D(
		renderWindow->swapChainExtent.width, renderWindow->swapChainExtent.height,
		1,
		numSamples,
		format,
		VK_IMAGE_TILING_OPTIMAL,
		imageUsage,
		VMA_MEMORY_USAGE_GPU_ONLY,
		attachment.imageAllocation
	);

	attachment.imageView = CreateImageView2D(attachment.image, format, aspectFlags, 1);

	return attachment;
}

void CGraphicsVK::DestroyFramebufferAttachment(FramebufferAttachment &attachment)
{
	if (attachment.imageView != VK_NULL_HANDLE)
	{
		vkDestroyImageView(device, attachment.imageView, nullptr);
		attachment.imageView = VK_NULL_HANDLE;
	}

	if (attachment.image != VK_NULL_HANDLE)
	{
		vmaDestroyImage(allocator, attachment.image, attachment.imageAllocation);
		attachment.image = VK_NULL_HANDLE;
		attachment.imageAllocation = VK_NULL_HANDLE;
	}
}

FramebufferAttachmentDescriptor CGraphicsVK::CreateFramebufferAttachmentDescriptor(std::vector<FramebufferAttachment*> attachments, VkDescriptorSetLayout descriptorSetLayout)
{
	FramebufferAttachmentDescriptor attachmentDescriptor;

	std::vector<VkDescriptorPoolSize> poolSizes;
	std::vector<VkDescriptorSetLayoutBinding> bindings(attachments.size());

	for (std::size_t i = 0; i < bindings.size(); ++i)
	{
		VkDescriptorSetLayoutBinding &binding = bindings[i];
		binding.binding = static_cast<uint32_t>(i);
		binding.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
		binding.descriptorCount = 1;
		binding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
		binding.pImmutableSamplers = nullptr;
	}

	for (auto &binding : bindings)
	{
		VkDescriptorPoolSize poolSize = {};
		poolSize.type = binding.descriptorType;
		poolSize.descriptorCount = MAX_FRAMES_IN_FLIGHT;

		poolSizes.emplace_back(poolSize);
	}

	VkDescriptorPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = MAX_FRAMES_IN_FLIGHT;

	if (vkCreateDescriptorPool(device, &poolInfo, nullptr, &attachmentDescriptor.descriptorPool) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create descriptor pool");

	std::array<VkDescriptorSetLayout, MAX_FRAMES_IN_FLIGHT> layouts;
	layouts.fill(descriptorSetLayout);

	VkDescriptorSetAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = attachmentDescriptor.descriptorPool;
	allocInfo.descriptorSetCount = MAX_FRAMES_IN_FLIGHT;
	allocInfo.pSetLayouts = layouts.data();

	if (vkAllocateDescriptorSets(device, &allocInfo, attachmentDescriptor.descriptorSets.data()) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to allocate descriptor sets");

	for (uint32_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
	{
		for (std::size_t attachmentIndex = 0; attachmentIndex < attachments.size(); ++attachmentIndex)
		{
			FramebufferAttachment *attachment = attachments[attachmentIndex];

			VkDescriptorImageInfo imageInfo = {};
			imageInfo.imageLayout = attachment->imageLayout;
			imageInfo.imageView = attachment->imageView;

			VkWriteDescriptorSet descriptorWrite = {};
			descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrite.dstSet = attachmentDescriptor.descriptorSets[i];
			descriptorWrite.dstBinding = static_cast<uint32_t>(attachmentIndex);
			descriptorWrite.dstArrayElement = 0;
			descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
			descriptorWrite.descriptorCount = 1;
			descriptorWrite.pImageInfo = &imageInfo;

			vkUpdateDescriptorSets(device, 1, &descriptorWrite, 0, nullptr);
		}
	}

	return attachmentDescriptor;
}

void CGraphicsVK::DestroyFramebufferAttachmentDescriptor(FramebufferAttachmentDescriptor &attachmentDescriptor)
{
	vkDestroyDescriptorPool(device, attachmentDescriptor.descriptorPool, nullptr);
	attachmentDescriptor.descriptorPool = VK_NULL_HANDLE;
	attachmentDescriptor.descriptorSets.fill(VK_NULL_HANDLE);
}

VkCommandBuffer CGraphicsVK::BeginSingleTimeCommands()
{
	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandPool = commandPool;
	allocInfo.commandBufferCount = 1;

	VkCommandBuffer commandBuffer = VK_NULL_HANDLE;
	if (vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to allocate single use command buffer");

	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	vkBeginCommandBuffer(commandBuffer, &beginInfo);

	return commandBuffer;
}

void CGraphicsVK::EndSingleTimeCommands(VkCommandBuffer &commandBuffer)
{
	vkEndCommandBuffer(commandBuffer);

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;

	vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
	vkQueueWaitIdle(graphicsQueue);

	vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
}

VkImage CGraphicsVK::CreateImage(uint32_t arrayLayers, VkImageType imageType, uint32_t width, uint32_t height, uint32_t mipLevels, VkSampleCountFlagBits numSamples, VkFormat format, VkImageTiling tiling, VkImageUsageFlags imageUsage, VmaMemoryUsage memoryUsage, VmaAllocation &allocation)
{
	VkImageCreateInfo imageInfo = {};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = imageType;
	imageInfo.extent.width = width;
	imageInfo.extent.height = height;
	imageInfo.extent.depth = 1;
	imageInfo.mipLevels = mipLevels;
	imageInfo.samples = numSamples;
	imageInfo.arrayLayers = arrayLayers;

	// TODO: Better way to do this?
	if (imageInfo.arrayLayers == 6)
		imageInfo.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;

	imageInfo.format = format;
	imageInfo.tiling = tiling;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageInfo.usage = imageUsage;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VmaAllocationCreateInfo allocInfo = {};
	allocInfo.usage = memoryUsage;

	VkImage image = VK_NULL_HANDLE;

	if (vmaCreateImage(allocator, &imageInfo, &allocInfo, &image, &allocation, nullptr) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create image");

	return image;
}

VkImage CGraphicsVK::CreateImage2D(uint32_t width, uint32_t height, uint32_t mipLevels, VkSampleCountFlagBits numSamples, VkFormat format, VkImageTiling tiling, VkImageUsageFlags imageUsage, VmaMemoryUsage memoryUsage, VmaAllocation &allocation)
{
	return CreateImage(1, VK_IMAGE_TYPE_2D, width, height, mipLevels, numSamples, format, tiling, imageUsage, memoryUsage, allocation);
}

VkImageView CGraphicsVK::CreateImageView(uint32_t layerCount, VkImageViewType imageViewType, VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t mipLevels)
{
	VkImageViewCreateInfo viewInfo = {};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.image = image;
	viewInfo.viewType = imageViewType;
	viewInfo.format = format;
	viewInfo.subresourceRange.aspectMask = aspectFlags;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = mipLevels;
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.layerCount = layerCount;

	VkImageView imageView = VK_NULL_HANDLE;
	if ( vkCreateImageView(device, &viewInfo, nullptr, &imageView) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create image view");

	return imageView;
}

VkImageView CGraphicsVK::CreateImageView2D(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t mipLevels)
{
	VkImageViewCreateInfo viewInfo = {};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.image = image;
	viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	viewInfo.format = format;
	viewInfo.subresourceRange.aspectMask = aspectFlags;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = mipLevels;
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.layerCount = 1;

	VkImageView imageView = VK_NULL_HANDLE;
	if ( vkCreateImageView(device, &viewInfo, nullptr, &imageView) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create image view");

	return imageView;
}

void CGraphicsVK::CopyBufferToImage(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height, uint32_t faceCount /*= 1*/)
{
	std::vector<VkBufferImageCopy> bufferCopyRegions;
	uint32_t offset = 0;

	for (uint32_t face = 0; face < faceCount; ++face)
	{
		VkBufferImageCopy region = {};
		region.bufferOffset = offset;
		region.bufferRowLength = 0;
		region.bufferImageHeight = 0;
		region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		region.imageSubresource.mipLevel = 0;
		region.imageSubresource.baseArrayLayer = face;
		region.imageSubresource.layerCount = 1;
		region.imageOffset = { 0, 0, 0 };
		region.imageExtent = { width, height, 1 };
		bufferCopyRegions.push_back( region );

		// HACKHACK: FIX
		// TODO: Assuming RGBA
		offset += (width * height * 4);
	}

	VkCommandBuffer commandBuffer = BeginSingleTimeCommands();
		vkCmdCopyBufferToImage(commandBuffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, static_cast<uint32_t>(bufferCopyRegions.size()), bufferCopyRegions.data());
	EndSingleTimeCommands(commandBuffer);
}

void CGraphicsVK::TransitionImageLayout(VkCommandBuffer commandBuffer, VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout, uint32_t mipLevels, uint32_t layerCount /*= 1*/)
{
	VkImageMemoryBarrier barrier = {};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.oldLayout = oldLayout;
	barrier.newLayout = newLayout;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.image = image;
			
	if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
	{
		barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
		auto hasStencilComponent = [](VkFormat format) { return ( format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT ); };
				
		if ( hasStencilComponent( format ) )
			barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
	}
	else {
		barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	}

	barrier.subresourceRange.baseMipLevel = 0;
	barrier.subresourceRange.levelCount = mipLevels;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = layerCount;
	barrier.srcAccessMask = 0; // TODO
	barrier.dstAccessMask = 0; // TODO

	VkPipelineStageFlags sourceStage, destinationStage;

	if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
	{
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

		sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	}
	else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
	{
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

		sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	}
	else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
	{
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

		sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
	}
	else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)
	{
		sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		destinationStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
	}
	else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_PRESENT_SRC_KHR)
	{
		sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		destinationStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
	}
	else if (oldLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_PRESENT_SRC_KHR)
	{
		barrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;

		sourceStage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
		destinationStage = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
	}
	else
		throw std::runtime_error("[Vulkan]Unsupported layout transition");

	vkCmdPipelineBarrier(
		commandBuffer,
		sourceStage, destinationStage,
		0,
		0, nullptr, 
		0, nullptr, 
		1, &barrier);
}

void CGraphicsVK::TransitionImageLayoutExplicit(VkCommandBuffer commandBuffer, const VkImageMemoryBarrier &barrier, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask)
{
	vkCmdPipelineBarrier(
		commandBuffer,
		srcStageMask, dstStageMask,
		0,
		0, nullptr, 
		0, nullptr, 
		1, &barrier);
}

void CGraphicsVK::TransitionImageLayoutExplicitImmediate(const VkImageMemoryBarrier &barrier, VkPipelineStageFlags srcStageMask, VkPipelineStageFlags dstStageMask)
{
	VkCommandBuffer commandBuffer = BeginSingleTimeCommands();
	TransitionImageLayoutExplicit(commandBuffer, barrier, srcStageMask, dstStageMask);
	EndSingleTimeCommands(commandBuffer);
}

void CGraphicsVK::TransitionImageLayoutImmediate(VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout, uint32_t mipLevels, uint32_t layerCount /*= 1*/)
{
	VkCommandBuffer commandBuffer = BeginSingleTimeCommands();
	TransitionImageLayout(commandBuffer, image, format, oldLayout, newLayout, mipLevels, layerCount);
	EndSingleTimeCommands(commandBuffer);
}

void CGraphicsVK::CreateBuffer(VkDeviceSize size, VkBufferUsageFlags bufferUsage, VmaMemoryUsage memoryUsage, VkBuffer &buffer, VmaAllocation &allocation)
{
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size;
	bufferInfo.usage = bufferUsage;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VmaAllocationCreateInfo allocInfo = {};
	allocInfo.usage = memoryUsage;

	if (vmaCreateBuffer(allocator, &bufferInfo, &allocInfo, &buffer, &allocation, nullptr) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create buffer");
}

void CGraphicsVK::DeleteBuffer(VkBuffer buffer, VmaAllocation allocation)
{
	BufferDestroy &destroy = bufferDestroyList.emplace_back();
	destroy.buffer = buffer;
	destroy.allocation = allocation;
}

void CGraphicsVK::DeleteBufferImmediate(VkBuffer buffer, VmaAllocation allocation)
{
	vmaDestroyBuffer(allocator, buffer, allocation);
}

void CGraphicsVK::CopyBuffer(const VkBuffer &srcBuffer, VkBuffer &dstBuffer, VkDeviceSize size)
{
	VkCommandBuffer commandBuffer = BeginSingleTimeCommands();
		VkBufferCopy copyRegion = {};
		copyRegion.size = size;
		vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);
		EndSingleTimeCommands(commandBuffer);
}

void CGraphicsVK::GenerateMipMaps(VkImage image, VkFormat format, uint32_t width, uint32_t height, uint32_t mipLevels)
{
	// Check if the image format supports linear blitting
	VkFormatProperties formatProperties = {};
	vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &formatProperties);

	if (!(formatProperties.optimalTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT))
		throw std::runtime_error("[Vulkan]Failed to find linear blit tiling feature");

	VkCommandBuffer commandBuffer = BeginSingleTimeCommands();

	VkImageMemoryBarrier barrier = {};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.image = image;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = 1;
	barrier.subresourceRange.levelCount = 1;

	int32_t mipWidth = (int32_t)width;
	int32_t mipHeight = (int32_t)height;

	for (uint32_t i = 1; i < mipLevels; ++i)
	{
		barrier.subresourceRange.baseMipLevel = i - 1;
		barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

		vkCmdPipelineBarrier(
			commandBuffer,
			VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
			0, nullptr,
			0, nullptr,
			1, &barrier
		);

		VkImageBlit blit = {};
		blit.srcOffsets[0] = { 0, 0, 0 };
		blit.srcOffsets[1] = { mipWidth, mipHeight, 1 };
		blit.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		blit.srcSubresource.mipLevel = i - 1;
		blit.srcSubresource.baseArrayLayer = 0;
		blit.srcSubresource.layerCount = 1;
		blit.dstOffsets[0] = { 0, 0, 0 };
		blit.dstOffsets[1] = { mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1 };
		blit.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		blit.dstSubresource.mipLevel = i;
		blit.dstSubresource.baseArrayLayer = 0;
		blit.dstSubresource.layerCount = 1;

		vkCmdBlitImage(
			commandBuffer,
			image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
			image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			1, &blit,
			VK_FILTER_LINEAR
		);

		barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
		barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

		vkCmdPipelineBarrier(
			commandBuffer,
			VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
			0, nullptr,
			0, nullptr,
			1, &barrier
		);

		if (mipWidth > 1)
			mipWidth /= 2;

		if (mipHeight > 1)
			mipHeight /= 2;
	}

	barrier.subresourceRange.baseMipLevel = mipLevels - 1;
	barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
	barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

	vkCmdPipelineBarrier(
		commandBuffer,
		VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0,
		0, nullptr,
		0, nullptr,
		1, &barrier
	);

	EndSingleTimeCommands(commandBuffer);
}

void CGraphicsVK::UpdateDirtyBuffers(uint32_t frame)
{
	for (auto &[buffer, bufferHandle] : dynamicBuffers)
	{
		if (buffer->dirty[frame])
		{
			std::memcpy(buffer->mapped[frame], buffer->data.data(), buffer->data.size());
			buffer->dirty[frame] = false;
		}
	}
}

void CGraphicsVK::DestroyStaticBuffer(CStaticBufferVK *buffer)
{
	if (!buffer)
		return;

	auto it = staticBuffers.find(buffer);

	if (it == staticBuffers.end())
		throw std::runtime_error("[Vulkan]Invalid buffer deletion");

	BufferDestroy &destroy = bufferDestroyList.emplace_back();
	destroy.buffer = buffer->buffer;
	destroy.allocation = buffer->allocation;

	staticBuffers.erase(it);
}

void CGraphicsVK::DestroyDynamicBuffer(CDynamicBufferVK *dynamicBuffer)
{
	if (!dynamicBuffer)
		return;

	auto it = dynamicBuffers.find(dynamicBuffer);

	if (it == dynamicBuffers.end())
		throw std::runtime_error("[Vulkan]Invalid dynamic buffer deletion");

	for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
	{
		vmaUnmapMemory(allocator, dynamicBuffer->allocation[i]);

		BufferDestroy &destroy = bufferDestroyList.emplace_back();
		destroy.buffer = dynamicBuffer->buffer[i];
		destroy.allocation = dynamicBuffer->allocation[i];
	}

	dynamicBuffers.erase(it);
}

void CGraphicsVK::DestroyDescriptorPool(VkDescriptorPool descriptorPool)
{
	DescriptorPoolDestroy &destroy = descriptorPoolDestroyList.emplace_back();
	destroy.descriptorPool = descriptorPool;
}

CDynamicVertexBuffer *CGraphicsVK::CreateDynamicVertexBuffer(VkDeviceSize vertexSize, VkDeviceSize vertexCount, std::optional<const void*> vertexData)
{
	std::unique_ptr<CDynamicBufferVK> bufferHandle = std::make_unique<CDynamicVertexBuffer>();
	CDynamicVertexBuffer *vertexBuffer = static_cast<CDynamicVertexBuffer*>(bufferHandle.get());

	const VkDeviceSize vertexBufferSize = vertexSize * vertexCount;
	vertexBuffer->vertexSize = static_cast<uint32_t>(vertexSize);
	vertexBuffer->vertexCount = static_cast<uint32_t>(vertexCount);

	vertexBuffer->data.resize(vertexBufferSize);

	if (vertexData.value_or(nullptr))
		std::memcpy(vertexBuffer->GetForModify(), vertexData.value(), vertexBufferSize);

	for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
	{
		CreateBuffer(vertexBufferSize, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU, vertexBuffer->buffer[i], vertexBuffer->allocation[i]);
		vmaMapMemory(allocator, vertexBuffer->allocation[i], &vertexBuffer->mapped[i]);

		if (vertexBuffer->dirty[i])
		{
			std::memcpy(vertexBuffer->mapped[i], vertexBuffer->data.data(), vertexBuffer->data.size());
			vertexBuffer->dirty[i] = false;
		}
	}

	dynamicBuffers[vertexBuffer] = std::move(bufferHandle);
	return vertexBuffer;
}

CVertexBuffer *CGraphicsVK::CreateVertexBuffer(VkDeviceSize vertexSize, VkDeviceSize vertexCount, const void *vertexData)
{
	if (!vertexData)
		throw std::runtime_error("[Vulkan]Tried to create static vertex buffer with no data!");

	std::unique_ptr<CStaticBufferVK> bufferHandle = std::make_unique<CVertexBuffer>();
	CVertexBuffer *vertexBuffer = static_cast<CVertexBuffer*>(bufferHandle.get());

	const VkDeviceSize vertexBufferSize = vertexSize * vertexCount;
	vertexBuffer->vertexSize = static_cast<uint32_t>(vertexSize);
	vertexBuffer->vertexCount = static_cast<uint32_t>(vertexCount);

	VkBuffer stagingBuffer = VK_NULL_HANDLE;
	VmaAllocation stagingBufferAllocation = VK_NULL_HANDLE;

	CreateBuffer(vertexBufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_ONLY, stagingBuffer, stagingBufferAllocation);

	void *stageData = nullptr;
	vmaMapMemory(allocator, stagingBufferAllocation, &stageData);
		std::memcpy(stageData, vertexData, static_cast<size_t>(vertexBufferSize));
	vmaUnmapMemory(allocator, stagingBufferAllocation);

	CreateBuffer(vertexBufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VMA_MEMORY_USAGE_GPU_ONLY, vertexBuffer->buffer, vertexBuffer->allocation);
	CopyBuffer(stagingBuffer, vertexBuffer->buffer, vertexBufferSize);

	DeleteBufferImmediate(stagingBuffer, stagingBufferAllocation);

	staticBuffers[vertexBuffer] = std::move(bufferHandle);
	return vertexBuffer;
}

CDynamicIndexBuffer *CGraphicsVK::CreateDynamicIndexBuffer(VkDeviceSize indexSize, VkDeviceSize indexCount, std::optional<const void*> indexData)
{
	std::unique_ptr<CDynamicBufferVK> bufferHandle = std::make_unique<CDynamicIndexBuffer>();
	CDynamicIndexBuffer *indexBuffer = static_cast<CDynamicIndexBuffer*>(bufferHandle.get());

	const VkDeviceSize indexBufferSize = indexSize * indexCount;
	indexBuffer->indexCount = static_cast<uint32_t>(indexCount);
	indexBuffer->data.resize(indexBufferSize);

	if (indexData.value_or(nullptr))
		std::memcpy(indexBuffer->GetForModify(), indexData.value(), indexBufferSize);

	for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
	{
		CreateBuffer(indexBufferSize, VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU, indexBuffer->buffer[i], indexBuffer->allocation[i]);
		vmaMapMemory(allocator, indexBuffer->allocation[i], &indexBuffer->mapped[i]);

		if (indexBuffer->dirty[i])
		{
			std::memcpy(indexBuffer->mapped[i], indexBuffer->data.data(), indexBuffer->data.size());
			indexBuffer->dirty[i] = false;
		}
	}

	dynamicBuffers[indexBuffer] = std::move(bufferHandle);
	return indexBuffer;
}

CIndexBuffer *CGraphicsVK::CreateIndexBuffer(VkDeviceSize indexSize, VkDeviceSize indexCount, const void *indexData)
{
	if (!indexData)
		throw std::runtime_error("[Vulkan]Tried to create static index buffer with no data!");

	std::unique_ptr<CStaticBufferVK> bufferHandle = std::make_unique<CIndexBuffer>();
	CIndexBuffer *indexBuffer = static_cast<CIndexBuffer*>(bufferHandle.get());

	const VkDeviceSize indexBufferSize = indexSize * indexCount;
	indexBuffer->indexCount = static_cast<uint32_t>(indexCount);

	VkBuffer stagingBuffer = VK_NULL_HANDLE;
	VmaAllocation stagingBufferAllocation = VK_NULL_HANDLE;

	CreateBuffer(indexBufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_ONLY, stagingBuffer, stagingBufferAllocation);

	void *stageData = nullptr;
	vmaMapMemory(allocator, stagingBufferAllocation, &stageData);
		std::memcpy(stageData, indexData, static_cast<size_t>(indexBufferSize));
	vmaUnmapMemory(allocator, stagingBufferAllocation);

	CreateBuffer(indexBufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VMA_MEMORY_USAGE_GPU_ONLY, indexBuffer->buffer, indexBuffer->allocation);
	CopyBuffer(stagingBuffer, indexBuffer->buffer, indexBufferSize);

	DeleteBufferImmediate(stagingBuffer, stagingBufferAllocation);

	staticBuffers[indexBuffer] = std::move(bufferHandle);
	return indexBuffer;
}

CTextureVK *CGraphicsVK::CreateTexture(bool generateMipMaps, uint32_t width, uint32_t height, const void *pixelData, VkFormat format, VkImageType imageType, VkImageViewType imageViewType, VkSamplerCreateInfo samplerInfo)
{
	if (!pixelData)
		throw std::runtime_error("[Vulkan]Tried to create texture with NULL pixel data");

	// TODO: Support other format :trollface:
	if (format != VK_FORMAT_R8G8B8A8_UNORM)
		throw std::runtime_error("[Vulkan]Unsupported format!");

	uint32_t arrayLayers = 0;
	switch (imageViewType)
	{
		case VK_IMAGE_VIEW_TYPE_2D:
			arrayLayers = 1;
			break;
		case VK_IMAGE_VIEW_TYPE_CUBE:
			arrayLayers = 6;
			break;
		default:
			throw std::runtime_error("[Vulkan]Unsupported image type!");
	}

	std::unique_ptr<CTextureVK> textureHandle = std::make_unique<CTextureVK>();
	CTextureVK *texture = textureHandle.get();

	if (generateMipMaps)
		texture->mipLevels = static_cast<uint32_t>(std::floor(std::log2(std::max(width, height)))) + 1;

	texture->width = width;
	texture->height = height;

	const VkDeviceSize imageSize = texture->width * texture->height * 4 * arrayLayers;

	VkBuffer stagingBuffer = VK_NULL_HANDLE;
	VmaAllocation stagingBufferAllocation = VK_NULL_HANDLE;

	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = imageSize;
	bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VmaAllocationCreateInfo allocInfo = {};
	allocInfo.usage = VMA_MEMORY_USAGE_CPU_ONLY;

	if (vmaCreateBuffer(allocator, &bufferInfo, &allocInfo, &stagingBuffer, &stagingBufferAllocation, nullptr) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create buffer");

	void *data = nullptr;
	vmaMapMemory(allocator, stagingBufferAllocation, &data);
		std::memcpy(data, pixelData, imageSize);
	vmaUnmapMemory(allocator, stagingBufferAllocation);

	texture->image = CreateImage(
				arrayLayers,
				imageType,
				width, height,
				texture->mipLevels,
				VK_SAMPLE_COUNT_1_BIT,
				format,
				VK_IMAGE_TILING_OPTIMAL,
				VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
				VMA_MEMORY_USAGE_GPU_ONLY,
				texture->allocation);

	VkImageMemoryBarrier barrier = {};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
	barrier.image = texture->image;
	barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	barrier.subresourceRange.levelCount = texture->mipLevels;
	barrier.subresourceRange.layerCount = arrayLayers;

	TransitionImageLayoutExplicitImmediate(barrier, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT);
	CopyBufferToImage(stagingBuffer, texture->image, width, height, arrayLayers);

	if (generateMipMaps)
		GenerateMipMaps(texture->image, format, texture->width, texture->height, texture->mipLevels);
	else
	{
		barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
		barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
		TransitionImageLayoutExplicitImmediate(barrier, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
	}

	vmaDestroyBuffer(allocator, stagingBuffer, stagingBufferAllocation);

	texture->imageView = CreateImageView(arrayLayers, imageViewType, texture->image, format, VK_IMAGE_ASPECT_COLOR_BIT, texture->mipLevels);

	// Create Texture Sampler
	// TODO: We override certain settings here, do this better?
	samplerInfo.minLod = 0.0f;
	samplerInfo.maxLod = static_cast<float>(texture->mipLevels);

	if (vkCreateSampler(device, &samplerInfo, nullptr, &texture->sampler) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create sampler");

	staticTextures[texture] = std::move(textureHandle);
	return texture;
}

void CGraphicsVK::DestroyTexture(CTextureVK *texture)
{
	auto it = staticTextures.find(texture);

	if (it == staticTextures.end())
		throw std::runtime_error("[Vulkan]Invalid texture deletion");

	TextureDestroy &destroy = textureDestroyList.emplace_back();
	destroy.image = texture->image;
	destroy.allocation = texture->allocation;
	destroy.imageView = texture->imageView;
	destroy.sampler = texture->sampler;

	staticTextures.erase(it);
}

CGuiMaterial *CGraphicsVK::CreateGuiMaterial(CRenderWindow *renderWindow, const void *image, int width, int height)
{
	CShaderVK *guiShader = shaderSystem->shaders.ui;
	CGuiMaterial *material = renderWindow->guiMaterials.emplace_back(std::make_unique<CGuiMaterial>()).get();

	VkDescriptorPoolSize poolSize = {};
	poolSize.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	poolSize.descriptorCount = MAX_FRAMES_IN_FLIGHT;

	VkDescriptorPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = 1;
	poolInfo.pPoolSizes = &poolSize;
	poolInfo.maxSets = MAX_FRAMES_IN_FLIGHT;

	if (vkCreateDescriptorPool(device, &poolInfo, nullptr, &material->descriptorPool) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create descriptor pool");

	const std::vector<VkDescriptorSetLayout> layouts(MAX_FRAMES_IN_FLIGHT, guiShader->descriptorSetLayout);

	VkDescriptorSetAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = material->descriptorPool;
	allocInfo.descriptorSetCount = MAX_FRAMES_IN_FLIGHT;
	allocInfo.pSetLayouts = layouts.data();

	if (vkAllocateDescriptorSets(device, &allocInfo, material->descriptorSets.data()) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to allocate descriptor sets");

	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.minFilter = VK_FILTER_LINEAR;

	material->texture = CreateTexture(false, (uint32_t)width, (uint32_t)height, image, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_TYPE_2D, VK_IMAGE_VIEW_TYPE_2D, samplerInfo);

	for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
	{
		VkDescriptorImageInfo imageInfo = {};
		imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		imageInfo.imageView = material->texture->imageView;
		imageInfo.sampler = material->texture->sampler;

		VkWriteDescriptorSet descriptorWrite = {};
		descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptorWrite.dstSet = material->descriptorSets[i];
		descriptorWrite.dstBinding = 0;
		descriptorWrite.dstArrayElement = 0;
		descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		descriptorWrite.descriptorCount = 1;
		descriptorWrite.pImageInfo = &imageInfo;

		vkUpdateDescriptorSets(device, 1, &descriptorWrite, 0, nullptr);
	}

	return material;
}

void CGraphicsVK::DestroyGuiMaterial(CRenderWindow *renderWindow, CGuiMaterial *material)
{
	material->descriptorSets.fill(VK_NULL_HANDLE);

	if (material->descriptorPool != VK_NULL_HANDLE)
	{
		DestroyDescriptorPool(material->descriptorPool);
		material->descriptorPool = VK_NULL_HANDLE;
	}

	DestroyTexture(material->texture);

	for (auto it = renderWindow->guiMaterials.begin(); it != renderWindow->guiMaterials.end(); ++it)
	{
		if (it->get() == material)
		{
			renderWindow->guiMaterials.erase(it);
			break;
		}
	}
}

void CGraphicsVK::SetupMaterialInstance(CMaterialInstanceVK *materialInstance)
{
	CShaderVK *shader = materialInstance->material->GetShader();
	std::vector<VkDescriptorPoolSize> poolSizes;

	auto bindings = shader->GetDescriptorSetLayoutBindings();
	for (auto &binding : bindings)
	{
		VkDescriptorPoolSize poolSize = {};
		poolSize.type = binding.descriptorType;
		poolSize.descriptorCount = MAX_FRAMES_IN_FLIGHT;

		poolSizes.emplace_back(poolSize);
	}

	VkDescriptorPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = MAX_FRAMES_IN_FLIGHT;

	if (vkCreateDescriptorPool(device, &poolInfo, nullptr, &materialInstance->descriptorPool) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create descriptor pool");

	const std::vector<VkDescriptorSetLayout> layouts(MAX_FRAMES_IN_FLIGHT, shader->descriptorSetLayout);

	VkDescriptorSetAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = materialInstance->descriptorPool;
	allocInfo.descriptorSetCount = MAX_FRAMES_IN_FLIGHT;
	allocInfo.pSetLayouts = layouts.data();

	if (vkAllocateDescriptorSets(device, &allocInfo, materialInstance->descriptorSets.data()) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to allocate descriptor sets");

	for (uint32_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
		materialInstance->UpdateDirtyDescriptorSets(i);
}