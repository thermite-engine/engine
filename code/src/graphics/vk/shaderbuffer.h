#pragma once

#include <volk.h>
#include <vk_mem_alloc.h>
#include <cstring>
#include <array>
#include <unordered_set>

#include "graphicsvk_definitions.h"

class CGraphicsVK;
class CMaterialInstanceVK;

class CShaderBuffer
{
public:

	CShaderBuffer()
	{
		buffer.fill(VK_NULL_HANDLE);
		bufferAllocation.fill(VK_NULL_HANDLE);
		mapped.fill(nullptr);
	}

	void AddMaterialInstance(CMaterialInstanceVK *instance);
	void RemoveMaterialInstance(CMaterialInstanceVK *instance);
	void MarkMaterialInstancesDirty();
	void UpdateBuffer(uint32_t imageIndex, const void *src, std::size_t size);

	CGraphicsVK *graphics = nullptr;

	std::array<VkBuffer, MAX_FRAMES_IN_FLIGHT> buffer;
	std::array<VmaAllocation, MAX_FRAMES_IN_FLIGHT> bufferAllocation;
	VkDeviceSize bufferSize = 0;

	std::array<void*, MAX_FRAMES_IN_FLIGHT> mapped;
	std::unordered_set<CMaterialInstanceVK*> materialInstances;

	VkDescriptorType descriptorType = {};
};