#include "shadervk.h"
#include "shadersystem.h"
#include "renderervk_impl.h"
#include "graphicsvk.h"

#include <stdexcept>

void CShaderVK::CreateDescriptorSetLayout()
{
	auto bindings = GetDescriptorSetLayoutBindings();

	if (bindings.size() > 0)
	{
		VkDescriptorSetLayoutCreateInfo layoutInfo = {};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
		layoutInfo.pBindings = bindings.data();

		if (vkCreateDescriptorSetLayout(graphics->device, &layoutInfo, nullptr, &descriptorSetLayout) != VK_SUCCESS)
			throw std::runtime_error("[Vulkan]Failed to create descriptor set layout");
	}
}