#pragma once

#include "texture.h"

#include <volk.h>
#include <vk_mem_alloc.h>

class CTextureVK : public ITexture
{
public:
	static CTextureVK *ToTextureVK(ITexture *texture)
	{
#ifdef _DEBUG
		return dynamic_cast<CTextureVK*>(texture);
#else
		return static_cast<CTextureVK*>(texture);
#endif
	}

	uint32_t GetWidth() const override { return width; }
	uint32_t GetHeight() const override { return height;  }

	bool IsOpaque() const override { return opaque; }

	bool opaque = true;

	VkImage image = VK_NULL_HANDLE;
	VmaAllocation allocation = VK_NULL_HANDLE;
	VkImageView imageView = VK_NULL_HANDLE;
	VkSampler sampler = VK_NULL_HANDLE;

	uint32_t mipLevels = 1;

	uint32_t width = 0;
	uint32_t height = 0;
};