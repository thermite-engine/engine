#pragma once

#include <cstring>
#include <cassert>
#include <vector>
#include <array>
#include <volk.h>
#include <vk_mem_alloc.h>

#include "graphicsvk_definitions.h"

class CDynamicBufferVK
{
public:
	CDynamicBufferVK()
	{
		buffer.fill(VK_NULL_HANDLE);
		allocation.fill(VK_NULL_HANDLE);
		mapped.fill(nullptr);
		dirty.fill(false);
	}

	virtual ~CDynamicBufferVK() = default;

	void MarkDirty()
	{
		dirty.fill(true);
	}

	void *GetForModify()
	{
		MarkDirty();
		return data.data();
	}

	void Modify(const void *src, std::size_t size, std::size_t offset = 0)
	{
		assert(offset + size <= data.size());
		std::memcpy(&data[offset], src, size);
		MarkDirty();
	}

	std::array<VkBuffer, MAX_FRAMES_IN_FLIGHT> buffer;
	std::array<VmaAllocation, MAX_FRAMES_IN_FLIGHT> allocation;
	std::array<void*, MAX_FRAMES_IN_FLIGHT> mapped;
	std::array<bool, MAX_FRAMES_IN_FLIGHT> dirty;

	// We maintain an intermediate buffer CPU-side that we can write to whenever and commit later
	// This will mean multiple potentially big copies, so use 'mapped' when you can.
	// Unfortunately this may be the only way to have the same buffer work for multiple windows.
	std::vector<char*> data;
};


class CStaticBufferVK
{
public:
	virtual ~CStaticBufferVK() = default;

	VkBuffer buffer = VK_NULL_HANDLE;
	VmaAllocation allocation = VK_NULL_HANDLE;
};