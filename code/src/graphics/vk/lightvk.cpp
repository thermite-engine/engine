#include "lightvk.h"
#include "shadersystem.h"

#include <cassert>
#include <limits>

void CPointLightVK::SetPosition(const glm::vec3 &position)
{
	this->position = position;
	MarkDirty();
}

void CPointLightVK::SetColor(const glm::vec3 &color)
{
	this->color = color;
	MarkDirty();
}

void CLightStateVK::CreateBuffers()
{
	lightInfoBuffer = shaderSystem->CreateUniformBuffer<LightInfo>();
	pointLightBuffer = shaderSystem->CreateStorageBuffer(sizeof(PointLight) * 100); // Allocate enough for 100 point lights to start
}

void CLightStateVK::DeleteBuffers()
{
	shaderSystem->DeleteShaderBuffer(lightInfoBuffer);
	shaderSystem->DeleteShaderBuffer(pointLightBuffer);

	lightInfoBuffer = nullptr;
	pointLightBuffer = nullptr;
}

void CLightStateVK::UpdateBuffers(uint32_t frame)
{
	if (dirty[frame])
	{
		LightInfo lightInfo = {
			(glm::u32)pointLights.size(),
			{ ambientLightColor.x, ambientLightColor.y, ambientLightColor.z }
		};

		std::memcpy(lightInfoBuffer->mapped[frame], &lightInfo, sizeof(lightInfo));
		dirty[frame] = false;
	}

	const std::size_t instanceCount = pointLightBuffer->bufferSize / sizeof(PointLight);
	if (instanceCount < pointLights.size())
	{
		// Grow by 100 point lights if we run out of space
		shaderSystem->ResizeStorageBuffer(pointLightBuffer, sizeof(PointLight) * (pointLights.size() + 100));
	}

	PointLight *lights = reinterpret_cast<PointLight*>(pointLightBuffer->mapped[frame]);

	for (auto &pl : pointLights)
	{
		if (pl->dirty[frame])
		{
			lights[pl->lightIndex].position = { pl->position.x, pl->position.y, pl->position.z };
			lights[pl->lightIndex].color = { pl->color.x, pl->color.y, pl->color.z };

			pl->dirty[frame] = false;
		}
	}
}

IPointLight *CLightStateVK::CreatePointLight()
{
	// TODO: Uh... This is really dumb honestly. Please fix.
	assert(pointLights.size() <= std::numeric_limits<uint32_t>::max());

	CPointLightVK *pointLight = pointLights.emplace_back(std::make_unique<CPointLightVK>()).get();
	pointLight->lightIndex = (uint32_t)pointLights.size() - 1;

	pointLight->MarkDirty();

	return pointLight;
}

void CLightStateVK::DeletePointLight(IPointLight *pointLight)
{
	auto light_it = std::find_if(pointLights.begin(), pointLights.end(), [&pointLight](const std::unique_ptr<CPointLightVK> &pointLightHandle) { return pointLight == pointLightHandle.get(); });

	if (light_it != pointLights.end())
	{
		pointLights.erase(light_it);
		for (size_t i = 0; i < pointLights.size(); ++i)
			pointLights[i]->lightIndex = (uint32_t)i;
	}
}

void CLightStateVK::SetAmbientLightColor(const glm::vec3 &color)
{
	ambientLightColor = color;
	MarkDirty();
}