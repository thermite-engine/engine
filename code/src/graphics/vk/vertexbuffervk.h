#pragma once
#include <array>
#include <vector>
#include <volk.h>
#include <vk_mem_alloc.h>

#include "buffervk.h"
#include "graphicsvk_definitions.h"

template <typename T>
class CVertexBufferBase : public T
{
public:
	uint32_t vertexSize = 0;
	uint32_t vertexCount = 0;
};

class CDynamicVertexBuffer : public CVertexBufferBase<CDynamicBufferVK>{};
class CVertexBuffer : public CVertexBufferBase<CStaticBufferVK>{};
