#pragma once

#include <array>
#include <volk.h>

#include "graphicsvk_definitions.h"

class CTextureVK;
class CShaderVK;

class CGuiMaterial
{
public:
	CTextureVK *texture = nullptr;
	VkDescriptorPool descriptorPool = VK_NULL_HANDLE;
	std::array<VkDescriptorSet, MAX_FRAMES_IN_FLIGHT> descriptorSets = { VK_NULL_HANDLE };
};
