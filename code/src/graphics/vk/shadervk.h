#pragma once
#include <string>
#include <string_view>
#include <volk.h>

#include "view.h"
#include "components/transform.h"
#include "vertexvk.h"
#include "texturecache.h"
#include "lightvk.h"

class CShaderSystemVK;
class CMaterialInstanceVK;
class CTextureCacheVK;
class CSceneVK;
class CMeshVK;
class CGraphicsVK;

struct alignas(16) ModelView_Projection
{
	alignas(16) glm::mat4 modelView;
	alignas(16) glm::mat4 projection;
};

struct alignas(16) MVPModel
{
	alignas(16) glm::mat4 mvp;
	alignas(16) glm::mat4 model;
};

struct alignas(16) ViewPos
{
	alignas(16) glm::vec3 pos = { 0.0f, 0.0f, 0.0f };
};

class CShaderVK
{
public:
	virtual ~CShaderVK() = default;

	virtual void InitShader() = 0;
	virtual void InitMaterialInstance(CTextureCacheVK *textureCache, CLightStateVK *lightState, CMaterialInstanceVK *instance) {}
	virtual void ShutdownMaterialInstance(CMaterialInstanceVK *instance) {}

	virtual void UpdateBuffers(CMaterialInstanceVK *materialInstance, const std::vector<Transform> &instances, const CView3D &view3D, uint32_t frame) {}

	virtual std::vector<VkDescriptorSetLayoutBinding> GetDescriptorSetLayoutBindings() const = 0;

	virtual void LoadShaderModules() = 0;
	virtual void CreateGraphicsPipelineLayout() = 0;
	virtual void CreateGraphicsPipeline() = 0;

	void CreateDescriptorSetLayout();
	
	CGraphicsVK *graphics = nullptr;
	CShaderSystemVK *shaderSystem = nullptr;
	VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;
	VkPipelineLayout pipelineLayout = VK_NULL_HANDLE;
	VkPipeline pipeline = VK_NULL_HANDLE;
	std::vector<VkShaderModule> shaderModules;

	std::string_view shaderName;
};