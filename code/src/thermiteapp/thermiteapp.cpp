#include "thermiteapp/thermiteapp.h"

#include <SDL2/SDL.h>
#include <stdexcept>

#ifdef _WIN32
#include <Windows.h>
#endif

CThermiteApp::CThermiteApp()
{
	if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER) < 0)
		throw std::runtime_error(SDL_GetError());

#ifdef _WIN32
	// This lets us have color text :D
	auto consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);

	if (consoleHandle)
	{
		DWORD flags = {};
		GetConsoleMode(consoleHandle, &flags);
		SetConsoleMode(consoleHandle, flags | ENABLE_VIRTUAL_TERMINAL_PROCESSING);
	}
#endif
}

CThermiteApp::~CThermiteApp()
{
	SDL_Quit();
}