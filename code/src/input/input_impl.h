#pragma once

#include <array>
#include <unordered_map>
#include <unordered_set>

#include "input.h"
#include "graphics/graphics.h"
#include "log/log.h"

struct ButtonBinding
{
	std::unordered_set<SDL_Keycode> keys;
	std::unordered_set<uint8_t> mouseButtons;
	std::unordered_set<uint8_t> controllerButtons;
};

struct ActionSet
{
	ActionSetState state;
	uint64_t numButtons;

	std::unordered_map<uint64_t, ButtonBinding> bindings;
};

struct MouseButton
{
	bool isPressed = false;
	bool wasPressed = false;
	int clicks = 0;
};

struct Key
{
	bool isPressed = false;
	bool wasPressed = false;
};

class CInput : public IInput, public CService<>
{
public:
	//using CService<>::CService;
	CInput(CServer *o);
	~CInput();

	void SetupImGuiContext(ImGuiContext *ctx) override;
	void HandleUI(ImGuiContext *ctx) override;

	int GetClicks(uint8_t mouseButton) const override;

	int GetMouseWheelX() const override { return mouseWheelX; }
	int GetMouseWheelY() const override { return mouseWheelY; }

	int GetMouseX() const override { return mouseX; }
	int GetMouseY() const override { return mouseY; }

	void GetMousePos(int &x, int &y) const override;

	void RegisterTextInputInterface(ITextInput *textInput) override;
	void UnregisterTextInputInterface(ITextInput *textInput) override;

	void RegisterWindowEventListener(IWindowEventListener *listener) override;
	void UnregisterWindowEventListener(IWindowEventListener *listener) override;

	bool IsMouseButtonPressed(uint8_t mouseButton) const override;
	bool IsMouseButtonReleased(uint8_t mouseButton) const override;
	bool IsMouseButtonJustPressed(uint8_t mouseButton) const override;
	bool IsMouseButtonJustReleased(uint8_t mouseButton) const override;

	bool IsKeyPressed(SDL_Keycode key) const override;
	bool IsKeyReleased(SDL_Keycode key) const override;
	bool IsKeyJustPressed(SDL_Keycode key) const override;
	bool IsKeyJustReleased(SDL_Keycode key) const override;

	int GetMouseDeltaX() const override { return mouseDeltaX; }
	int GetMouseDeltaY() const override { return mouseDeltaY; }

	bool IsPressed(const Button &button) const override
	{
		return (actionSets.at(button.actionSet).state.currentButtonState & button.buttonMask) == button.buttonMask;
	}

	bool IsReleased(const Button &button) const override
	{
		return (actionSets.at(button.actionSet).state.currentButtonState & button.buttonMask) == 0;
	}

	bool IsJustPressed(const Button &button) const override
	{
		return ((actionSets.at(button.actionSet).state.currentButtonState & button.buttonMask) == button.buttonMask)
			&& ((actionSets.at(button.actionSet).state.oldButtonState & button.buttonMask) == 0);
	}

	bool IsJustReleased(const Button &button) const override
	{
		return ((actionSets.at(button.actionSet).state.currentButtonState & button.buttonMask) == 0)
			&& ((actionSets.at(button.actionSet).state.oldButtonState & button.buttonMask) == button.buttonMask);
	}

	ActionSetID CreateActionSet(const std::string &name) override;
	Button CreateButton(ActionSetID actionSet, const std::string &name) override;

	void SetActiveActionSet(ActionSetID actionSet) override
	{
		actionSets.at(activeActionSet).state.currentButtonState = 0;
		activeActionSet = actionSet;
	}

	ActionSetID GetActiveActionSet() const override { return activeActionSet; }

	uint64_t GetNumActionSets() const override { return static_cast<uint64_t>(actionSets.size()); }
	uint64_t GetNumButtonsForActionSet(ActionSetID actionSet) const override { return actionSets.at(actionSet).numButtons; }

	ActionSetState GetActionSetState(ActionSetID actionSet) const override { return actionSets.at(actionSet).state; }
	void GetActionSetStates(std::vector<ActionSetState> &states) const override
	{
		states.resize(actionSets.size());

		for (std::size_t i = 0; i < actionSets.size(); ++i)
			states[i] = actionSets[i].state;
	}

	const Button& BindKey(const Button &button, SDL_Keycode key) override;
	const Button& UnbindKey(const Button &button, SDL_Keycode key) override;

	const Button& BindMouseButton(const Button &button, uint8_t mouseButton) override;
	const Button& UnbindMouseButton(const Button &button, uint8_t mouseButton) override;

	void Update(uint64_t) override;

private:

	void ClearState();

	ActionSetID activeActionSet = 0;

	std::vector<ITextInput*> textInputInterfaces;
	std::vector<IWindowEventListener*> windowEventListeners;

	std::vector<ActionSet> actionSets;
	std::unordered_map<std::string, ActionSetID> actionSetNameMap;

	std::unordered_map<std::string, Button> buttonNameMap;

	std::unordered_map<SDL_Keycode, Key> keyboardState = {};
	std::unordered_map<uint8_t, MouseButton> mouseState = {};
	std::unordered_map<int, std::unordered_map<uint8_t, bool>> gameControllersState = {};

	std::unordered_map<int, SDL_GameController*> gameControllers = {};

	// TODO: I really don't want this here, but... I don't know where else to put this
	SDL_Cursor *mouseCursors[ImGuiMouseCursor_COUNT] = {};
	std::unique_ptr<char[], std::function<void(void*)>> clipboardText;

	std::array<bool, SDL_NUM_SCANCODES> keyboardScancodeState;

	// NOTE: This is hacky storage for ImGui
	std::string textInputBuffer;

	int mouseDeltaX = 0;
	int mouseDeltaY = 0;

	int mouseX = 0;
	int mouseY = 0;

	int mouseWheelX = 0;
	int mouseWheelY = 0;
};