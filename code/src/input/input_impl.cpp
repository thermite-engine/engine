#include "input_impl.h"
#include "log/log.h"

#include "imgui.h"
#include "ui/the_imgui.h"

CInput::CInput(CServer *o) : CService<>(o, "input")
{
	keyboardScancodeState.fill(false);

	// Load mouse cursors
	mouseCursors[ImGuiMouseCursor_Arrow] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_ARROW);
	mouseCursors[ImGuiMouseCursor_TextInput] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_IBEAM);
	mouseCursors[ImGuiMouseCursor_ResizeAll] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZEALL);
	mouseCursors[ImGuiMouseCursor_ResizeNS] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENS);
	mouseCursors[ImGuiMouseCursor_ResizeEW] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZEWE);
	mouseCursors[ImGuiMouseCursor_ResizeNESW] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENESW);
	mouseCursors[ImGuiMouseCursor_ResizeNWSE] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_SIZENWSE);
	mouseCursors[ImGuiMouseCursor_Hand] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_HAND);
	mouseCursors[ImGuiMouseCursor_NotAllowed] = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_NO);
}

CInput::~CInput()
{
	for (auto &[index, gamecontroller] : gameControllers)
	{
		if (gamecontroller)
			SDL_GameControllerClose(gamecontroller);
	}

	for (ImGuiMouseCursor i = 0; i < ImGuiMouseCursor_COUNT; ++i)
	{
		SDL_FreeCursor(mouseCursors[i]);
		mouseCursors[i] = nullptr;
	}
}

void CInput::SetupImGuiContext(ImGuiContext *ctx)
{
	ThermiteImGui::SetCurrentContext(ctx);

	ImGuiIO &io = ImGui::GetIO();
	io.ClipboardUserData = this;

	// TODO: Better wrappers for these
	io.SetClipboardTextFn = [](void*, const char *text) -> void
	{
		SDL_SetClipboardText(text);
	};

	io.GetClipboardTextFn = [](void *userData) -> const char*
	{
		CInput *input = reinterpret_cast<CInput*>(userData);
		input->clipboardText = {SDL_GetClipboardText(), SDL_free};

		return input->clipboardText.get();
	};
}

void CInput::HandleUI(ImGuiContext *ctx)
{
	ThermiteImGui::SetCurrentContext(ctx);
	ImGuiIO &io = ImGui::GetIO();

	SDL_Window *window = reinterpret_cast<SDL_Window*>(io.UserData);
	if (window && SDL_GetWindowFlags(window) & SDL_WINDOW_INPUT_FOCUS)
	{
		// TODO: Kind of hoping for the best here, verify this works OK
		io.MouseWheelH = (float)mouseWheelX;
		io.MouseWheel = (float)mouseWheelY;

		auto imguiInput = [&io, this](SDL_Scancode code)
		{
			io.KeysDown[code] = keyboardScancodeState[code];
		};

		imguiInput(SDL_SCANCODE_TAB);
		imguiInput(SDL_SCANCODE_LEFT);
		imguiInput(SDL_SCANCODE_RIGHT);
		imguiInput(SDL_SCANCODE_UP);
		imguiInput(SDL_SCANCODE_DOWN);
		imguiInput(SDL_SCANCODE_PAGEUP);
		imguiInput(SDL_SCANCODE_PAGEDOWN);
		imguiInput(SDL_SCANCODE_HOME);
		imguiInput(SDL_SCANCODE_END);
		imguiInput(SDL_SCANCODE_INSERT);
		imguiInput(SDL_SCANCODE_DELETE);
		imguiInput(SDL_SCANCODE_BACKSPACE);
		imguiInput(SDL_SCANCODE_SPACE);
		imguiInput(SDL_SCANCODE_RETURN);
		imguiInput(SDL_SCANCODE_ESCAPE);
		imguiInput(SDL_SCANCODE_KP_ENTER);
		imguiInput(SDL_SCANCODE_A);
		imguiInput(SDL_SCANCODE_C);
		imguiInput(SDL_SCANCODE_V);
		imguiInput(SDL_SCANCODE_X);
		imguiInput(SDL_SCANCODE_Y);
		imguiInput(SDL_SCANCODE_Z);

		io.KeyShift = ((SDL_GetModState() & KMOD_SHIFT) != 0);
		io.KeyCtrl = ((SDL_GetModState() & KMOD_CTRL) != 0);
		io.KeyAlt = ((SDL_GetModState() & KMOD_ALT) != 0);
#ifdef _WIN32
		io.KeySuper = false;
#else
		io.KeySuper = ((SDL_GetModState() & KMOD_GUI) != 0);
#endif

		if (!textInputBuffer.empty())
		{
			io.AddInputCharactersUTF8(textInputBuffer.c_str());
			textInputBuffer.clear();
		}

		// Update mouse cursor as necessary
		if (!(io.ConfigFlags & ImGuiConfigFlags_NoMouseCursorChange))
		{

			ImGuiMouseCursor imgui_cursor = ImGui::GetMouseCursor();
			if (io.MouseDrawCursor || imgui_cursor == ImGuiMouseCursor_None)
			{
				// Hide OS mouse cursor if imgui is drawing it or if it wants no cursor
				SDL_ShowCursor(SDL_FALSE);
			}
			else
			{
				// Show OS mouse cursor
				SDL_SetCursor(mouseCursors[imgui_cursor] ? mouseCursors[imgui_cursor] : mouseCursors[ImGuiMouseCursor_Arrow]);
				SDL_ShowCursor(SDL_TRUE);
			}
		}

		// Update mouse pos and buttons
		// Set OS mouse position if requested (rarely used, only when ImGuiConfigFlags_NavEnableSetMousePos is enabled by user)
		if (io.WantSetMousePos)
			SDL_WarpMouseInWindow(window, (int)io.MousePos.x, (int)io.MousePos.y);
		else
			io.MousePos = ImVec2(-FLT_MAX, -FLT_MAX);

		// TODO: HACKHACK: We're basically ignoring mouse input if the mouse is in relative mode
		const bool relative = SDL_GetRelativeMouseMode();
		if (!relative)
		{
			io.MouseDown[ImGuiMouseButton_Left] = IsMouseButtonPressed(SDL_BUTTON_LEFT);
			io.MouseDown[ImGuiMouseButton_Right] = IsMouseButtonPressed(SDL_BUTTON_RIGHT);
			io.MouseDown[ImGuiMouseButton_Middle] = IsMouseButtonPressed(SDL_BUTTON_MIDDLE);
			io.MousePos = ImVec2((float)mouseX, (float)mouseY);
		}
	}
}

int CInput::GetClicks(uint8_t mouseButton) const
{
	if (auto it = mouseState.find(mouseButton); it != mouseState.cend())
		return it->second.clicks;

	return 0;
}

void CInput::GetMousePos(int &x, int &y) const
{
	x = mouseX;
	y = mouseY;
}

void CInput::RegisterTextInputInterface(ITextInput *textInput)
{
	textInputInterfaces.push_back(textInput);
}

void CInput::UnregisterTextInputInterface(ITextInput *textInput)
{
	std::erase(textInputInterfaces, textInput);
}

void CInput::RegisterWindowEventListener(IWindowEventListener *listener)
{
	windowEventListeners.push_back(listener);
}

void CInput::UnregisterWindowEventListener(IWindowEventListener *listener)
{
	std::erase(windowEventListeners, listener);
}

bool CInput::IsMouseButtonPressed(uint8_t mouseButton) const
{
	if (auto it = mouseState.find(mouseButton); it != mouseState.cend())
		return it->second.isPressed;

	return false;
}

bool CInput::IsMouseButtonReleased(uint8_t mouseButton) const
{
	if (auto it = mouseState.find(mouseButton); it != mouseState.cend())
		return !it->second.isPressed;

	return true;
}

bool CInput::IsMouseButtonJustPressed(uint8_t mouseButton) const
{
	if (auto it = mouseState.find(mouseButton); it != mouseState.cend())
		return (it->second.isPressed && !it->second.wasPressed);

	return false;
}

bool CInput::IsMouseButtonJustReleased(uint8_t mouseButton) const
{
	if (auto it = mouseState.find(mouseButton); it != mouseState.cend())
		return (!it->second.isPressed && it->second.wasPressed);

	return false;
}

bool CInput::IsKeyPressed(SDL_Keycode key) const
{
	if (auto it = keyboardState.find(key); it != keyboardState.cend())
		return it->second.isPressed;

	return false;
}

bool CInput::IsKeyReleased(SDL_Keycode key) const
{
	if (auto it = keyboardState.find(key); it != keyboardState.cend())
		return !it->second.isPressed;

	return true;
}

bool CInput::IsKeyJustPressed(SDL_Keycode key) const
{
	if (auto it = keyboardState.find(key); it != keyboardState.cend())
		return (it->second.isPressed && !it->second.wasPressed);

	return false;
}

bool CInput::IsKeyJustReleased(SDL_Keycode key) const
{
	if (auto it = keyboardState.find(key); it != keyboardState.cend())
		return (!it->second.isPressed && it->second.wasPressed);

	return false;
}

ActionSetID CInput::CreateActionSet(const std::string &name)
{
	const ActionSetID actionSet = actionSets.size();
	actionSets.emplace_back(ActionSet{});

	actionSetNameMap[name] = actionSet;

	return actionSet;
}

Button CInput::CreateButton(ActionSetID actionSet, const std::string &name)
{
	if (buttonNameMap.find(name) != buttonNameMap.end())
		Log::FatalError("Tried to create button that already exists.\n");

	ActionSet &set = actionSets.at(actionSet);
	if (set.numButtons == 64)
	{
		// We store button states in 64-bit button masks so the maximum number of buttons is 64
		// Hopefully you don't need more than this, otherwise you're screwed.
		Log::FatalError("Exceeded maximum allowed buttons per action set! (64)");
	}

	Button button = {
		.actionSet = actionSet,
		.buttonMask = (1ull << set.numButtons)
	};

	buttonNameMap[name] = button;
	++set.numButtons;

	return button;
}

const Button &CInput::BindKey(const Button &button, SDL_Keycode key)
{
	ActionSet &set = actionSets.at(button.actionSet);
	set.bindings[button.buttonMask].keys.insert(key);

	return button;
}

const Button &CInput::UnbindKey(const Button &button, SDL_Keycode key)
{
	ActionSet &set = actionSets.at(button.actionSet);
	set.bindings[button.buttonMask].keys.erase(key);

	return button;
}

const Button &CInput::BindMouseButton(const Button &button, uint8_t mouseButton)
{
	ActionSet &set = actionSets.at(button.actionSet);
	set.bindings[button.buttonMask].mouseButtons.insert(mouseButton);

	return button;
}

const Button &CInput::UnbindMouseButton(const Button &button, uint8_t mouseButton)
{
	ActionSet &set = actionSets.at(button.actionSet);
	set.bindings[button.buttonMask].mouseButtons.erase(mouseButton);

	return button;
}

void CInput::Update(uint64_t)
{
	ClearState();

	SDL_Event event = {};
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
			case SDL_TEXTINPUT:
			{
				for (auto textInput : textInputInterfaces)
					textInput->OnTextInput(event.text.text);

				textInputBuffer += event.text.text;

				break;
			}
			case SDL_CONTROLLERDEVICEADDED:
			{
				Log::Print("SDL_CONTROLLERDEVICEADDED: {}\n", event.cdevice.which);
				SDL_GameController *gamecontroller = SDL_GameControllerOpen(event.cdevice.which);

				if (!gamecontroller)
					Log::Print("Failed to add game controller: {}\n", SDL_GetError());
				else
					gameControllers[event.cdevice.which] = gamecontroller;

				Log::Print("Adding controller: {}\n", SDL_GameControllerName(gamecontroller));

				break;
			}
			case SDL_CONTROLLERDEVICEREMOVED:
			{
				Log::Print("SDL_CONTROLLERDEVICEREMOVED: {}\n", event.cdevice.which);
				if (auto it = gameControllers.find(event.cdevice.which); it != gameControllers.end())
				{
					SDL_GameControllerClose(it->second);
					gameControllers.erase(it);
				}

				break;
			}
			case SDL_WINDOWEVENT:
			{
				for (auto listener : windowEventListeners)
					listener->OnWindowEvent(event.window);
				break;
			}
			case SDL_KEYDOWN:
			case SDL_KEYUP:
			{
				const SDL_Keycode keyCode = event.key.keysym.sym;
				keyboardState[keyCode].isPressed = (event.key.state == SDL_PRESSED);
				keyboardScancodeState[event.key.keysym.scancode] = (event.key.state == SDL_PRESSED);
				break;
			}
			case SDL_MOUSEBUTTONDOWN:
			{
				const uint8_t mouseButton = event.button.button;
				auto &buttonState = mouseState[mouseButton];
				buttonState.isPressed = true;
				buttonState.clicks = event.button.clicks;
				break;
			}
			case SDL_MOUSEBUTTONUP:
			{
				const uint8_t mouseButton = event.button.button;
				mouseState[mouseButton].isPressed = false;
				break;
			}
			case SDL_MOUSEMOTION:
			{
				mouseX = event.motion.x;
				mouseY = event.motion.y;
				mouseDeltaX += event.motion.xrel;
				mouseDeltaY += event.motion.yrel;
				break;
			}
			case SDL_MOUSEWHEEL:
			{
				mouseWheelX += event.wheel.x;
				mouseWheelY += event.wheel.y;

				break;
			}
			case SDL_CONTROLLERBUTTONDOWN:
			{
				Log::Print("SDL_CONTROLLERBUTTONDOWN: {}\n", event.cbutton.button);
				gameControllersState[event.cbutton.which][event.cbutton.button] = true;
				break;
			}
			case SDL_CONTROLLERBUTTONUP:
			{
				Log::Print("SDL_CONTROLLERBUTTONUP: {}\n", event.cbutton.button);
				gameControllersState[event.cbutton.which][event.cbutton.button] = false;
				break;
			}
		}
	}
}

void CInput::ClearState()
{
	for (auto &[code, mouseButton] : mouseState)
	{
		mouseButton.wasPressed = mouseButton.isPressed;
		mouseButton.clicks = 0;
	}

	for (auto &[keyCode, key] : keyboardState)
		key.wasPressed = key.isPressed;
	
	for (ActionSet &set : actionSets)
	{
		set.state.oldButtonState = set.state.currentButtonState;
		set.state.currentButtonState = 0;
	}

	// Now check the active set for any buttons that are still held
	ActionSet &activeSet = actionSets.at(activeActionSet);
	for (uint64_t i = 0; i < activeSet.numButtons; ++i)
	{
		const uint64_t buttonMask = (1ull << i);
		ButtonBinding &bindings = activeSet.bindings[buttonMask];

		for (const auto &key : bindings.keys)
		{
			if (keyboardState[key].isPressed)
			{
				activeSet.state.currentButtonState |= buttonMask;
				break;
			}
		}

		if ((activeSet.state.currentButtonState & buttonMask) == 0)
		{
			// Not pressed by key, check mouse
			for (const auto &mouseButton : bindings.mouseButtons)
			{
				if (mouseState[mouseButton].isPressed)
				{
					activeSet.state.currentButtonState |= buttonMask;
					break;
				}
			}
		}

		// TODO: Check controller
	}

	mouseDeltaX = 0;
	mouseDeltaY = 0;
	mouseWheelX = 0;
	mouseWheelY = 0;

	// TODO: This is used for ImGui text input, but there's a chance we're clearing the buffer before it's actually consumed
	textInputBuffer.clear();
}