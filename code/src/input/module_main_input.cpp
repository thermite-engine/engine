#include "input_impl.h"
#include "log/log.h"
#include "ui/the_imgui.h"

extern "C" THR_EXPORT void ModuleInit(CServer::CServerInstance *instance)
{
	CServer::Init(instance);
	Log::Init(instance->Get<ILogger>(), "Input", fmt::color::lawn_green);

	instance->MakeService<CInput>(instance->GetServer());
	ThermiteImGui::Init(instance->GetServer());
}

extern "C" THR_EXPORT void ModuleShutdown()
{
	ThermiteImGui::Shutdown();
	Log::Print("Shutting down...\n");
}