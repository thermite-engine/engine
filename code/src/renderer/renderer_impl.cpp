#include "renderer_impl.h"

IWindow *CRenderer::CreateWindow(const std::string_view &title, int width, int height)
{
	CWindow *window = new CWindow();
	SDL_Window *handle = SDL_CreateWindow(title.data(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN);
	window->handle = handle;
	uint32_t id = SDL_GetWindowID(handle);
	windows.insert_or_assign(id, window);
	return window;
}

void CRenderer::DeleteWindow(IWindow *window)
{
	if (!window)
		return;
	
	CWindow *impl_window = reinterpret_cast<CWindow *>(window);
	SDL_Window *handle = impl_window->handle;
	uint32_t id = SDL_GetWindowID(handle);
	SDL_DestroyWindow(handle);
	
	windows.erase(id);
	
	delete impl_window;
}

void CRenderer::HandleWindowEvent(const SDL_WindowEvent &windowEvent)
{
	std::unordered_map<uint32_t, CWindow *>::const_iterator window_it = windows.find(windowEvent.windowID);
	if (window_it == windows.cend())
		return;
			
	CWindow *window = window_it->second;
			
	switch (windowEvent.event)
	{
		case SDL_WINDOWEVENT_CLOSE:
		{
			window->onClosed(window);
			break;
		}
	}
}

void CRenderer::Update(std::size_t)
{
	
}