#pragma once

#include "network.h"
#include "packet_types.h"
#include "log/log.h"
#include "timer.h"
#include "network_definitions.h"
#include "networkchannels.h"
#include "networkserver.h"
#include "networkclient.h"

#include <enet/enet.h>
#include <thread>
#include <set>
#include <unordered_map>

class CNetwork : public INetwork, public CService<>
{
public:
	using CService<>::CService;
	~CNetwork();

	INetworkServer *CreateServer(uint16_t portNum) override;
	void ConnectToServer(const std::string &ip, uint16_t portNum) override;

	bool IsHosting() const override { return (server.host != nullptr); }
	bool IsConnected() const override { return (client.host != nullptr); };

	void Update(uint64_t) override;


	CNetworkServer server;
	CNetworkClient client;

	IInput *input = Get<IInput>();
};