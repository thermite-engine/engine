#pragma once

#include "network.h"
#include "network_definitions.h"
#include "packetbuffer.h"
#include "timer.h"
#include "input/input.h"

#include <enet/enet.h>

class CNetworkClient
{
public:
	void AccumulateClientInputDeltas();
	void ResetClientInput();
	void SampleClientInput();

	void Run();
	void Destroy();

	CPacketBufferWriter &GetWriterInstance()
	{
		// Doesn't free allocated memory so we don't have to re-allocate on new writes
		writerInstance.buffer.clear();
		return writerInstance;
	}

	CPacketBufferWriter writerInstance;

	IInput *input = nullptr;

	ENetAddress address = {};
	ENetHost *host = nullptr;
	ENetPeer *peer = nullptr;

	bool connectionPending = false;
	bool isConnected = false;
	bool isFullyConnected = false;

	struct {
		CIntervalTimer<std::chrono::seconds, 5> connectionTimeout;
		CIntervalTimer<std::chrono::milliseconds, UPDATE_RATE_MS> nextUpdate;
		CIntervalTimer<std::chrono::milliseconds, INPUT_SAMPLE_RATE_MS> nextInputUpdate;
		CIntervalTimer<std::chrono::seconds, 1> bandwidthMeasure;
	} timers;

	bool InputChanged() const
	{
		if (prevStates != inputBuffer)
			return true;

		if (mouseDeltaX != 0 || mouseDeltaY != 0)
			return true;
		
		if ((prevMouseDeltaX != mouseDeltaX) || (prevMouseDeltaY != mouseDeltaY))
			return true;


		return false;
	}

	std::vector<ActionSetState> prevStates;
	std::vector<ActionSetState> inputBuffer;

	int32_t prevMouseDeltaX = 0;
	int32_t prevMouseDeltaY = 0;
	int32_t mouseDeltaX = 0;
	int32_t mouseDeltaY = 0;

	uint64_t numActionSets = 0;

	// Received from server
	uint32_t maxClients = 0;
	uint32_t clientID = 0;
};