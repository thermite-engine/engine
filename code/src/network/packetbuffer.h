#pragma once

#include <cstdint>
#include <vector>
#include <string>
#include <string_view>
#include <cassert>
#include <limits>
#include <type_traits>
#include <enet/enet.h>

#include "log/log.h"

class CPacketBufferWriter;
class CPacketBufferReader;

template <typename T>
struct PacketBuffer
{
	void operator()(CPacketBufferWriter &writer, const T &in) = delete;
	void operator()(CPacketBufferReader &reader, T &out) = delete;
};

class CPacketBufferReader
{
public:
	template <typename T>
	CPacketBufferReader &ReadPacket(T &out)
	{
		PacketBuffer<T>()(*this, out);
		return *this;
	}

	template <typename T>
	CPacketBufferReader &ReadVector(std::vector<T> &out)
	{
		static_assert(std::is_trivially_copyable_v<T>, "Cannot read non-trivial type");

		uint32_t length = 0;
		ReadUint32(length);

		if (length > 0)
		{
			out.resize(length);
			ReadMultiple(out.data(), out.size());
		}

		return *this;
	}

	template<typename T>
	CPacketBufferReader &Read(T &out)
	{
		static_assert(std::is_trivially_copyable_v<T>, "Cannot read non-trivial type");

		if (sizeof(T) + bufferPos > bufferLen) {
			Log::FatalError("Buffer read exceeded buffer length\n");
		}

		out = reinterpret_cast<const T&>(buffer[bufferPos]);
		bufferPos += sizeof(T);

		return *this;
	}

	template <typename T>
	CPacketBufferReader &ReadMultiple(T *out, std::size_t length)
	{
		static_assert(std::is_trivially_copyable_v<T>, "Cannot read non-trivial type");
		const std::size_t size = sizeof(T) * length;

		if (size + bufferPos > bufferLen) {
			Log::FatalError("Buffer read exceeded buffer length\n");
		}

		const T* begin = reinterpret_cast<const T*>(&buffer[bufferPos]);
		const T* end = begin + length;

		std::copy(begin, end, out);
		bufferPos += size;

		return *this;
	}

	CPacketBufferReader &ReadFloat32(float &out) { return Read(out); }
	CPacketBufferReader &ReadFloat64(double &out) { return Read(out); }

	CPacketBufferReader &ReadUint8(uint8_t &out) { return Read(out); }
	CPacketBufferReader &ReadUint16(uint16_t &out) { return Read(out); }
	CPacketBufferReader &ReadUint32(uint32_t &out) { return Read(out); }
	CPacketBufferReader &ReadUint64(uint64_t &out) { return Read(out); }

	CPacketBufferReader &ReadInt8(int8_t &out) { return Read(out); }
	CPacketBufferReader &ReadInt16(int16_t &out) { return Read(out); }
	CPacketBufferReader &ReadInt32(int32_t &out) { return Read(out); }
	CPacketBufferReader &ReadInt64(int64_t &out) { return Read(out); }

	CPacketBufferReader &ReadBytes(int8_t *out, std::size_t length) { return ReadMultiple<int8_t>(out, length); }
	CPacketBufferReader &ReadUBytes(uint8_t *out, std::size_t length) { return ReadMultiple<uint8_t>(out, length); }

	CPacketBufferReader &ReadString(std::string &out)
	{
		uint32_t length = 0;
		ReadUint32(length);

		out.resize(length);
		ReadBytes(reinterpret_cast<int8_t*>(out.data()), out.size());

		return *this;
	}

	const uint8_t* buffer = nullptr;
	std::size_t bufferLen = 0;

	std::size_t bufferPos = 0;
};

class CPacketBufferWriter
{
public:
	ENetPacket *CreatePacket(uint32_t flags)
	{
		return enet_packet_create(buffer.data(), buffer.size(), flags);
	}

	template <typename T>
	CPacketBufferWriter &WritePacket(const T &in)
	{
		PacketBuffer<T>()(*this, in);
		return *this;
	}

	template <typename T>
	CPacketBufferWriter &WriteVector(const std::vector<T> &in)
	{
		static_assert(std::is_trivially_copyable_v<T>, "Cannot write non-trivial type");
		assert(in.size() < std::numeric_limits<uint32_t>::max());

		// Always write the size even if we're empty so that reads will always work
		WriteUint32(static_cast<uint32_t>(in.size()));
		WriteMultiple(in.data(), in.size());

		return *this;
	}

	template <typename T>
	CPacketBufferWriter &Write(const T &in)
	{
		static_assert(std::is_trivially_copyable_v<T>, "Cannot write non-trivial type");

		const std::size_t pos = buffer.size();
		buffer.resize(buffer.size() + sizeof(T));

		T &out = reinterpret_cast<T&>(buffer[pos]);
		out = in;

		return *this;
	}

	template <typename T>
	CPacketBufferWriter &WriteMultiple(const T *in, std::size_t length)
	{
		static_assert(std::is_trivially_copyable_v<T>, "Cannot write non-trivial type");

		if (length != 0)
		{
			const std::size_t size = sizeof(T) * length;
			const std::size_t pos = buffer.size();

			buffer.resize(buffer.size() + size);
			const T *end = in + length;

			std::copy(in, end, reinterpret_cast<T*>(&buffer[pos]));
		}

		return *this;
	}

	CPacketBufferWriter &WriteFloat32(const float &in) { return Write(in); }
	CPacketBufferWriter &WriteFloat64(const double &in) { return Write(in); }

	CPacketBufferWriter &WriteUint8(const uint8_t &in) { return Write(in); }
	CPacketBufferWriter &WriteUint16(const uint16_t &in) { return Write(in); }
	CPacketBufferWriter &WriteUint32(const uint32_t &in) { return Write(in); }
	CPacketBufferWriter &WriteUint64(const uint64_t &in) { return Write(in); }

	CPacketBufferWriter &WriteInt8(const int8_t &in) { return Write(in); }
	CPacketBufferWriter &WriteInt16(const int16_t &in) { return Write(in); }
	CPacketBufferWriter &WriteInt32(const int32_t &in) { return Write(in); }
	CPacketBufferWriter &WriteInt64(const int64_t &in) { return Write(in); }

	
	CPacketBufferWriter &WriteBytes(const int8_t *in, std::size_t length) { return WriteMultiple<int8_t>(in, length); }
	CPacketBufferWriter &WriteUBytes(const uint8_t *in, std::size_t length) { return WriteMultiple<uint8_t>(in, length); }

	CPacketBufferWriter &WriteString(const std::string_view &in)
	{
		assert(in.size() < std::numeric_limits<uint32_t>::max());
		const uint32_t size = static_cast<uint32_t>(in.size());

		WriteUint32(size);
		WriteBytes(reinterpret_cast<const int8_t*>(in.data()), in.size());
		return *this;
	}

	std::vector<uint8_t> buffer;
};

template <typename T>
struct TrivialPacketBuffer
{
	void operator()(CPacketBufferWriter &writer, const T &in)
	{
		writer.Write(in);
	}

	void operator()(CPacketBufferReader &reader, T &out)
	{
		reader.Read(out);
	}
};