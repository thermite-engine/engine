#pragma once

constexpr auto UPDATE_RATE_MS = 15;
constexpr auto INPUT_SAMPLE_RATE_MS = 30;