#include "network_impl.h"
#include "log/log.h"
#include "packetbuffer.h"
#include "enet_wrappers.h"

using namespace std::chrono_literals;

CNetwork::~CNetwork()
{
	server.Destroy();
	client.Destroy();

	enet_deinitialize();
}

INetworkServer *CNetwork::CreateServer(uint16_t portNum)
{
	if (server.host)
	{
		Log::Print(ESeverity::Warning, "Already hosting server.\n");
		return &server;
	}

	server.address.host = ENET_HOST_ANY;
	server.address.port = portNum;

	server.host = enet_host_create(
		&server.address,
		server.GetMaxClients(),
		static_cast<std::size_t>(ENetworkChannel::NumChannels),
		0,
		0
	);

	if (!server.host) {
		Log::FatalError("Failed to create server.\n");
	}

	server.numActionSets = input->GetNumActionSets();

	return &server;
}

void CNetwork::ConnectToServer(const std::string &ip, uint16_t portNum)
{
	if (client.host)
	{
		Log::Print(ESeverity::Warning, "Already connected to server.\n");
		return;
	}

	client.host = enet_host_create(
		nullptr,
		1,
		static_cast<std::size_t>(ENetworkChannel::NumChannels),
		0,
		0
	);

	if (!client.host) {
		Log::FatalError("Failed to create client.\n");
	}

	enet_address_set_host(&client.address, ip.data());
	client.address.port = portNum;

	client.peer = enet_host_connect(
		client.host,
		&client.address,
		static_cast<std::size_t>(ENetworkChannel::NumChannels),
		0
	);

	if (!client.peer) {
		Log::FatalError("No available peers for initiating an ENet connection.\n");
	}

	using namespace std::chrono_literals;

	client.connectionPending = true;
	client.timers.connectionTimeout.Restart();

	client.input = input;
	client.numActionSets = input->GetNumActionSets();
}

void CNetwork::Update(uint64_t)
{
	server.Run();
	client.Run();
}