#pragma once

#include "network.h"
#include "network_definitions.h"

#include "packet_types.h"
#include "timer.h"

#include <vector>
#include <cstdint>
#include <unordered_map>
#include <set>
#include <enet/enet.h>

// Clients that have technically connected, but haven't requested to be put in server
struct PendingClient
{
	ENetPeer *peer = nullptr;
	CIntervalTimer<std::chrono::seconds, 5> timeout;
};

struct ConnectedClient
{
	ENetPeer *peer = nullptr;
	ClientInfo clientInfo;
};

class CNetworkServer : public INetworkServer
{
public:

	uint32_t AssignClient()
	{
		if (!retiredClientIDs.empty()) {
			return retiredClientIDs.extract(retiredClientIDs.begin()).value();
		}

		return lastClientID++;
	}
	
	uint32_t GetMaxClients() const override { return maxClients; }
	uint32_t GetNumConnectedClients() const override { return static_cast<uint32_t>(connectedClients.size()); }
	ClientInfo GetConnectedClient(uint32_t clientID) override { return connectedClients.at(clientID).clientInfo; }

	std::vector<ClientInfo> GetConnectedClients() override
	{
		std::vector<ClientInfo> clientInfos;
		clientInfos.reserve(connectedClients.size());

		for (auto &[peer, client] : connectedClients)
			clientInfos.emplace_back(client.clientInfo);

		return clientInfos;
	}

	void OnServerUpdate();
	void Run();
	void Destroy();

	struct
	{
		CIntervalTimer<std::chrono::milliseconds, UPDATE_RATE_MS> nextUpdate;
	} timers;


	CPacketBufferWriter &GetWriterInstance()
	{
		// Doesn't free allocated memory so we don't have to re-allocate on new writes
		writerInstance.buffer.clear();
		return writerInstance;
	}

	CPacketBufferWriter writerInstance;

	std::string serverName = "Unnamed Server";

	ENetAddress address = {};
	ENetHost *host = nullptr;

	uint32_t maxClients = 8;
	std::unordered_map<uint32_t, ConnectedClient> connectedClients;
	std::unordered_map<ENetPeer*, uint32_t> peerIDMap;

	std::vector<PendingClient> pendingClients;

	uint32_t lastClientID = 0;
	std::set<uint32_t> retiredClientIDs;
	
	uint64_t numActionSets = 0;
};