#include "networkserver.h"
#include "log/log.h"
#include "enet_wrappers.h"

void CNetworkServer::OnServerUpdate()
{
	std::vector<ENetPacket*> inputPackets;
	inputPackets.reserve(connectedClients.size());

	for (auto &[clientID, connectedClient] : connectedClients)
	{
		// TODO: We need to optimize this to only send changes
		InputChangePacket inputChange = {
			.clientInfo = { .clientID = clientID },
			.inputBuffer = connectedClient.clientInfo.inputState.actionSetStates,
			.mouseDeltaX = connectedClient.clientInfo.inputState.mouseDeltaX,
			.mouseDeltaY = connectedClient.clientInfo.inputState.mouseDeltaY
		};

		inputPackets.emplace_back(
			GetWriterInstance()
				.WritePacket(inputChange)
				.CreatePacket(ENET_PACKET_FLAG_RELIABLE)
		);
	}

	for (auto &packet : inputPackets)
	{
		for (auto &[clientID, connectedClient] : connectedClients)
		{
			PeerSend(
				connectedClient.peer,
				ENetworkChannel::Input,
				packet
			);
		}
	}
}

void CNetworkServer::Run()
{
	// Run the server
	if (host)
	{
		auto it = pendingClients.begin();
		while (it != pendingClients.end())
		{
			PendingClient &pendingClient = *it;
			if (pendingClient.timeout.IsReady())
			{
				enet_peer_disconnect(pendingClient.peer, 0);
				pendingClients.erase(it);
			}
			else
				++it;
		}

		if (timers.nextUpdate.IsReady())
		{
			OnServerUpdate();
			timers.nextUpdate.Restart();
		}

		ENetEvent event;
		while (enet_host_service(host, &event, 0) > 0)
		{
			switch (event.type)
			{
				case ENET_EVENT_TYPE_RECEIVE: {
					//Log::Print("[Server]Received packet on channel {}\n", (int)event.channelID);
					const ENetworkChannel channelID = static_cast<ENetworkChannel>(event.channelID);

					CPacketBufferReader reader = {
						.buffer = event.packet->data,
						.bufferLen = event.packet->dataLength
					};

					switch (channelID)
					{
						case ENetworkChannel::Request: {
							RequestPacket request;
							reader.ReadPacket(request);

							switch(request.type)
							{
								case ERequestType::ServerInfo: {
									PeerSend(
										event.peer,
										ENetworkChannel::ServerInfo,
										GetWriterInstance()
											.WritePacket<ServerInfoResponsePacket>({
												.serverName = serverName,
												.maxClients = maxClients,
												.numConnectedClients = GetNumConnectedClients()
											}).CreatePacket(ENET_PACKET_FLAG_RELIABLE)
									);
									break;
								}
								case ERequestType::ClientAssign: {
									pendingClients.erase(std::remove_if(pendingClients.begin(), pendingClients.end(), [&event](const PendingClient &pendingClient)
									{
										if (pendingClient.peer == event.peer) {
											return true;
										}

										return false;
									}));

									auto &[newClientID, connectedClient] = *connectedClients
										.emplace(std::make_pair(AssignClient(), ConnectedClient())).first;

									connectedClient.peer = event.peer;
									peerIDMap.emplace(std::make_pair(event.peer, newClientID));

									PeerSend(
										event.peer,
										ENetworkChannel::ClientInfoAssign,
										GetWriterInstance()
											.WritePacket<ClientInfoAssignResponsePacket>({
												.clientID = newClientID
											}).CreatePacket(ENET_PACKET_FLAG_RELIABLE)
									);
									break;
								}
								default: {
									break;
								}
							}
							break;
						}
						case ENetworkChannel::ClientInfoAssign: {
							break;
						}
						case ENetworkChannel::Input: {
							//Log::Print("[Server] Received input\n");

							InputChangePacket inputChange;
							reader.ReadPacket(inputChange);

							if (inputChange.inputBuffer.size() != numActionSets)
							{
								Log::Print(ESeverity::Warning, "Input buffer not the same for server/client\n");
								break;
							}

							ClientInputState &inputState = connectedClients.at(inputChange.clientInfo.clientID).clientInfo.inputState;

							inputState.actionSetStates = inputChange.inputBuffer;
							inputState.mouseDeltaX = inputChange.mouseDeltaX;
							inputState.mouseDeltaY = inputChange.mouseDeltaY;

							break;
						}
						default: {
							break;
						}
					}

					enet_packet_destroy(event.packet);
					break;
				}
				case ENET_EVENT_TYPE_CONNECT: {
					Log::Print("[Server] Pending client connected\n");

					PendingClient &pendingClient = pendingClients.emplace_back();
					pendingClient.peer = event.peer;
					pendingClient.timeout.Restart();

					break;
				}
				case ENET_EVENT_TYPE_DISCONNECT: {
					Log::Print("[Server] Client disconnected\n");

					auto it = peerIDMap.find(event.peer);

					if (it != peerIDMap.end())
					{
						const uint32_t peerID = it->second;

						peerIDMap.erase(it);
						connectedClients.erase(peerID);
						retiredClientIDs.insert(peerID);
					}
					else
					{
						// TODO: This may eventually be considered valid if we allow peers to connect to query the server
						Log::Print(ESeverity::Warning, "[Server] Unknown disconnected client!\n");
					}

					break;
				}
				default: {
					break;
				}
			}
		}
	}
}

void CNetworkServer::Destroy()
{
	if (host)
	{
		for (auto &[peer, connectedClient] : connectedClients)
			Log::Print("Client sent {} data\n", connectedClient.peer->host->totalReceivedData);

		enet_host_destroy(host);
		host = nullptr;
	}

	address = {};
}
