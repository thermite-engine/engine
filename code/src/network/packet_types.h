#pragma once

#include <cstdint>
#include <vector>
#include <string>

#include "input/input.h"
#include "packetbuffer.h"
#include "log/log.h"

enum class ERequestType : uint32_t
{
	ServerInfo,
	ClientAssign,
};

struct RequestPacket
{
	ERequestType type;
};

template <> struct PacketBuffer<RequestPacket> : TrivialPacketBuffer<RequestPacket>{};

struct ServerInfoResponsePacket
{
	std::string serverName;
	uint32_t maxClients;
	uint32_t numConnectedClients;
};

template <>
struct PacketBuffer<ServerInfoResponsePacket>
{
	void operator()(CPacketBufferWriter &writer, const ServerInfoResponsePacket &in)
	{
		writer
			.WriteString(in.serverName)
			.WriteUint32(in.maxClients)
			.WriteUint32(in.numConnectedClients);
	}

	void operator()(CPacketBufferReader &reader, ServerInfoResponsePacket &out)
	{
		reader
			.ReadString(out.serverName)
			.ReadUint32(out.maxClients)
			.ReadUint32(out.numConnectedClients);
	}
};

struct ClientInfoAssignResponsePacket
{
	uint32_t clientID;
};

template <> struct PacketBuffer<ClientInfoAssignResponsePacket> : TrivialPacketBuffer<ClientInfoAssignResponsePacket>{};

struct ClientInfoPacket
{
	uint32_t clientID;
};

template <> struct PacketBuffer<ClientInfoPacket> : TrivialPacketBuffer<ClientInfoPacket>{};

struct InputChangePacket
{
	ClientInfoPacket clientInfo;

	// Our input state changes
	std::vector<ActionSetState> inputBuffer;

	// These deltas we can probably just accumulate instead of buffering, we'll see I guess
	int32_t mouseDeltaX;
	int32_t mouseDeltaY;
};

template <>
struct PacketBuffer<InputChangePacket>
{
	void operator()(CPacketBufferWriter &writer, const InputChangePacket &in)
	{
		writer
			.WritePacket(in.clientInfo)
			.WriteVector(in.inputBuffer)
			.WriteInt32(in.mouseDeltaX)
			.WriteInt32(in.mouseDeltaY);
	}

	void operator()(CPacketBufferReader &reader, InputChangePacket &out)
	{
		reader
			.ReadPacket(out.clientInfo)
			.ReadVector(out.inputBuffer)
			.ReadInt32(out.mouseDeltaX)
			.ReadInt32(out.mouseDeltaY);
	}
};