#include "service.h"
#include "services/defines.h"
#include "log/log.h"
#include "enet/enet.h"
#include "network_impl.h"

extern "C" THR_EXPORT void ModuleInit(CServer::CServerInstance *instance)
{
    CServer::Init(instance);
    Log::Init(instance->Get<ILogger>(), "Network", fmt::color::brown);

    if (enet_initialize() != 0)
        Log::FatalError("An error occurred while initializing ENet.\n");

    instance->MakeService<CNetwork>(instance->GetServer());
}

extern "C" THR_EXPORT void ModuleShutdown()
{
    Log::Print("Shutting down...\n");
}