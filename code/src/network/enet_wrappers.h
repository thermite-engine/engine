#pragma once

#include <enet/enet.h>
#include "networkchannels.h"

// Just a little wrapper so I can stop casting channelID everywhere
static inline int PeerSend(ENetPeer *peer, ENetworkChannel channelID, ENetPacket *packet)
{
	return enet_peer_send(peer, static_cast<uint8_t>(channelID), packet);
}