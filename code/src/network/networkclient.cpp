#include "networkclient.h"
#include "enet_wrappers.h"
#include "packet_types.h"
#include "log/log.h"

void CNetworkClient::AccumulateClientInputDeltas()
{
	mouseDeltaX += input->GetMouseDeltaX();
	mouseDeltaY += input->GetMouseDeltaY();
}

void CNetworkClient::ResetClientInput()
{
	prevStates = inputBuffer;
	prevMouseDeltaX = mouseDeltaX;
	prevMouseDeltaY = mouseDeltaY;
	mouseDeltaX = 0;
	mouseDeltaY = 0;

	inputBuffer.clear();
}

void CNetworkClient::SampleClientInput()
{
	input->GetActionSetStates(inputBuffer);
}

void CNetworkClient::Run()
{
	// Run the client
	if (host)
	{
		if (timers.bandwidthMeasure.IsReady())
		{
			Log::Print("out: {} (b/s)\n", host->totalSentData);
			host->totalSentData = 0;
			timers.bandwidthMeasure.Restart();
		}

		ENetEvent event;
		while (enet_host_service(host, &event, 0) > 0)
		{
			switch (event.type)
			{
				case ENET_EVENT_TYPE_RECEIVE: {
					const ENetworkChannel channelID = static_cast<ENetworkChannel>(event.channelID);
					CPacketBufferReader reader = {
						.buffer = event.packet->data,
						.bufferLen = event.packet->dataLength
					};
					switch(channelID)
					{
						case ENetworkChannel::ServerInfo: {
							ServerInfoResponsePacket serverInfo;
							reader.ReadPacket(serverInfo);

							Log::Print("[Client] Got server info\n");
							Log::Print("[Client] Server name: {}\n", serverInfo.serverName);
							Log::Print("[Client] Max clients: {}\n", serverInfo.maxClients);
							Log::Print("[Client] Number of connected clients: {}\n", serverInfo.numConnectedClients);

							// TODO: Implement this
							const bool wantsToConnect = true;

							if (wantsToConnect)
							{
								maxClients = serverInfo.maxClients;
								PeerSend(
									event.peer,
									ENetworkChannel::Request,
									GetWriterInstance()
										.WritePacket<RequestPacket>({
											.type = ERequestType::ClientAssign
										}).CreatePacket(ENET_PACKET_FLAG_RELIABLE)
								);
							}
							break;
						}
						case ENetworkChannel::ClientInfoAssign: {
							ClientInfoAssignResponsePacket assign;

							reader.ReadPacket(assign);
							clientID = assign.clientID;
							
							Log::Print("[Client] Got ClientInfoAssignResponsePacket | clientID = {}\n", clientID);

							timers.nextUpdate.Restart();
							timers.nextInputUpdate.Restart();
							isFullyConnected = true;

							break;
						}
						case ENetworkChannel::Input: {
							break;
						}
						default: {
							break;
						}
					}
					enet_packet_destroy(event.packet);
					break;
				}
				case ENET_EVENT_TYPE_CONNECT: {
					if (!connectionPending) {
						Log::FatalError("[Client] Attempted connection with no pending connection.\n");
					}

					connectionPending = false;
					timers.connectionTimeout = {};

					isConnected = true;

					PeerSend(
						event.peer,
						ENetworkChannel::Request,
						GetWriterInstance()
							.WritePacket<RequestPacket>({
								.type = ERequestType::ServerInfo
							}).CreatePacket(ENET_PACKET_FLAG_RELIABLE)
					);

					Log::Print("[Client] Connection to server successful.\n");
					break;
				}
				default: {
					break;
				}
			}
		}

		if (connectionPending && timers.connectionTimeout.IsReady())
		{
			Log::Print("[Client] Failed to connect to server, connection timed out.\n");
			Destroy();
		}
	}

	if (isFullyConnected)
	{
		AccumulateClientInputDeltas();

		if (timers.nextInputUpdate.IsReady())
		{
			SampleClientInput();

			if (InputChanged())
			{
				InputChangePacket inputChange = {
					.clientInfo = { .clientID = clientID },
					.inputBuffer = inputBuffer,
					.mouseDeltaX = mouseDeltaX,
					.mouseDeltaY = mouseDeltaY
				};
				
				PeerSend(
					peer,
					ENetworkChannel::Input,
					GetWriterInstance()
						.WritePacket(inputChange)
						.CreatePacket(ENET_PACKET_FLAG_RELIABLE)
				);
			}

			ResetClientInput();

			timers.nextInputUpdate.Restart();
		}
	}
}

void CNetworkClient::Destroy()
{
	if (peer)
	{
		enet_peer_disconnect(peer, 0);
		enet_host_flush(host);

		peer = nullptr;
	}

	if (host)
	{
		enet_host_destroy(host);
		host = nullptr;
	}

	address = {};

	connectionPending = false;
	timers = {};

	maxClients = 0;
	clientID = 0;
}