#pragma once

#include <cstdint>

enum class ENetworkChannel : uint8_t
{
	Request,
	ServerInfo,
	ClientInfo,
	ClientInfoAssign,
	Input,
	FullUpdate,
	Snapshot,
	NumChannels
};