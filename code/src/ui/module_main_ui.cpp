#include "log.h"
#include "service.h"

#include "imgui.h"
#include "ui/the_imgui.h"
#include "ui/ui.h"

#include <SDL2/SDL.h>

class CImGuiService : public CService<>, public IImGuiService
{
public:
	using CService<>::CService;

	void RegisterContextSetter(IImGuiContextSetter *setter) override
	{
		setters.push_back(setter);
	}

	void UnregisterContextSetter(IImGuiContextSetter *setter) override
	{
		std::erase(setters, setter);
	}

	void SetCurrentContext(ImGuiContext *ctx) override
	{
		for (auto setter : setters)
			setter->SetCurrentContext(ctx);
	}

	std::vector<IImGuiContextSetter*> setters;
};

extern "C" THR_EXPORT void ModuleInit(CServer::CServerInstance *instance)
{
	CServer::Init(instance);
	Log::Init(instance->Get<ILogger>(), "UI");

	instance->MakeService<CImGuiService>(instance->GetServer(), "ImGui");

	ThermiteImGui::Init(instance->GetServer());
}

extern "C" THR_EXPORT void ModuleShutdown()
{
	ThermiteImGui::Shutdown();
	Log::Print("Shutting down...\n");
}