#ifdef __MINGW32__
#define SDL_MAIN_HANDLED
#endif

#include <iostream>
#include <thread>
#include "thermiteapp/thermiteapp.h"
#include "service.h"
#include "resourceloader.h"
#include "graphics.h"
#include "config.h"
#include "input/input.h"
#include "components/transform.h"
#include "log/log.h"
#include "ui/the_imgui.h"
#include "network/network.h"

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <algorithm>
#include <charconv>
#include <array>

class CMainService : public CService<IGraphics>, public IWindowResize
{
public:
	CMainService(CServer *s) : CService(s)
	{
		Log::Init(Get<ILogger>(), "Launcher", fmt::color::dark_turquoise);
		ThermiteImGui::Init(s);
		//SDL_SetRelativeMouseMode(SDL_TRUE);

		camera = entityManager.CreateEntity<Camera>();
		camera->transform = entityManager.CreateEntity<Transform>();

		view3D.camera = camera;

		window->RegisterOnClosed(std::bind(&CMainService::WindowClosed, this, std::placeholders::_1));
		window->RegisterWindowResize(this);
		renderer->SetClearColor(0.39f, 0.58f, 0.93f, 1.0f);

		ITextureCache *cache = scene->GetTextureCache();
		scene->SetSkyboxTexture(cache->LoadSkybox("textures/skybox/space_nebula_", ".png"));
		//scene->SetSkyboxTexture(cache->LoadSkybox("textures/skybox/interstellar_", ".tga"));

		camera->transform->position = { 0.0f, 0.0f, 3.0f };

		ILightState *lightState = scene->GetLightState();
		lightState->SetAmbientLightColor({ 0.2f, 0.2f, 0.2f });

		pl1 = lightState->CreatePointLight();
		pl2 = lightState->CreatePointLight();
		pl3 = lightState->CreatePointLight();

		pl1->SetColor({ 1.0f, 0.0f, 0.0f });
		pl1->SetPosition({ 1.5f, 0.0f, 0.0f });

		pl2->SetColor({ 0.383f, 0.025f, 0.419f });
		pl2->SetPosition({ -1.5f, 0.0f, 0.0f });

		pl3->SetColor({ 0.0f, 0.0f, 1.0f });
		pl3->SetPosition({ 0.0f, 1.5f, 0.0f });

		for (int i = 0; i < 10; ++i)
			sprites.emplace_back(scene->LoadBillboardSprite("textures/heart_sprite.png"));

		sprites[0]->SetColor({ 1.0f, 0.0f, 1.0f, 1.0f });
		sprites[1]->SetColor({ 1.0f, 0.0f, 0.0f, 0.4f });
		sprites[2]->SetColor({ 0.0f, 1.0f, 1.0f, 1.0f });

		//network->CreateServer(1234);
		//network->ConnectToServer("127.0.0.1", 1234);
	}

	void OnWindowResize(IWindow *resized, uint32_t width, uint32_t height) override
	{
		if (resized == window)
		{
			view3D.Set(0, 0, width, height, zNear, zFar, FOV);
		}
	}

	void WindowClosed(IWindow*)
	{
		KillAll();
	}

	void KillAll()
	{
		ThermiteImGui::Shutdown();

		renderer->DeleteScene(scene);
		graphics->DeleteRenderer(renderer);
		graphics->DeleteWindow(window);
		scene = nullptr;
		renderer = nullptr;
		window = nullptr;
		killed = true;
		Halt();
	}

	void UpdateCamera()
	{
		static const glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
		static const glm::vec3 forward = glm::vec3(0.0f, 0.0f, -1.0f);
		static const glm::vec3 right = glm::vec3(1.0f, 0.0f, 0.0f);

		glm::quat xRot = glm::angleAxis(glm::radians(mY), glm::normalize(glm::vec3(-1.0f, 0.0f, 0.0f)));
		glm::quat yRot = glm::angleAxis(glm::radians(mX), glm::normalize(glm::vec3(0.0f, 1.0f, 0.0f)));

		camera->transform->rotation = xRot * yRot;
		camera->viewMatrix = ToFirstPersonCameraTransformation(*camera->transform);

		camera->local_forward = forward * camera->transform->rotation;
		camera->local_back = -camera->local_forward;

		camera->forward = forward * yRot;
		camera->up = up;
		camera->right = right * yRot;

		camera->back = -camera->forward;
		camera->down = -camera->up;
		camera->left = -camera->right;
	}

	void InputCamera(float dt)
	{
		const float speed = input->IsPressed(IN_SPEED) ? 8.0f : 5.0f;

		glm::vec3 move = {};
		if (input->IsPressed(IN_FORWARD)) move += camera->local_forward;
		if (input->IsPressed(IN_BACK)) move += camera->local_back;
		if (input->IsPressed(IN_RIGHT)) move += camera->right;
		if (input->IsPressed(IN_LEFT)) move += camera->left;

		glm::normalize(move);
		camera->transform->position += move * speed * dt;

		if (input->IsPressed(IN_JUMP))
			camera->transform->position += glm::vec3(0.0f, 1.0f, 0.0f) * speed * dt;
		else if (input->IsPressed(IN_CROUCH))
			camera->transform->position += glm::vec3(0.0f, -1.0f, 0.0f) * speed * dt;

		if (input->IsPressed(IN_LOOKUP))
			mY += 180.0f * dt;
		else if (input->IsPressed(IN_LOOKDOWN))
			mY -= 180.0f *dt;

		if (input->IsPressed(IN_TURNRIGHT))
			mX += 180.0f * dt;
		else if (input->IsPressed(IN_TURNLEFT))
			mX -= 180.0f * dt;

		mX += (float)input->GetMouseDeltaX() * 0.1f;
		mY -= (float)input->GetMouseDeltaY() * 0.1f;

		auto constrain = [](float num) -> float
		{
			num = std::fmod(num, 360.0f);
			return (num < 0.0f) ? num += 360.0f : num;
		};

		mX = constrain(mX);
		mY = std::clamp(mY, -90.0f, 90.0f);

		if (input->IsJustPressed(IN_ZOOMIN))
			camera->transform->scale += 0.1f;
		else if (input->IsJustPressed(IN_ZOOMOUT))
			camera->transform->scale -= 0.1f;
	}

	void Update(uint64_t dt) override
	{
		if (killed)
			return;

		if (signalQuit)
		{
			KillAll();
			return;
		}

		if (input->IsJustPressed(IN_MENU))
			drawUI = !drawUI;

		SDL_SetRelativeMouseMode((SDL_bool)!drawUI);

		const float flDt = (float)dt / (float)1000000;
		elapsedTime += flDt;

		glm::vec3 lightPos = pl1->GetPosition();
		lightPos.z = (sinf(1.2f * elapsedTime) * 1.5f) + 1.5f;

		pl1->SetPosition(lightPos);

		window->NewGuiFrame(flDt);
		input->HandleUI(window->GetGuiContext());
		ThermiteImGui::SetCurrentContext(window->GetGuiContext());
		ImGui::NewFrame();
		{
			if (drawUI)
			{
				if (drawNetworkUI && !network->IsConnected())
				{
					ImGui::Begin("Network", &drawNetworkUI);
					static std::array<char, 16> ipbuf;
					static std::array<char, 6> portbuf = {'1', '2', '3', '4' };

					// IP field doesn't allow for domains for now.
					ImGui::InputText("IP", ipbuf.data(), ipbuf.size(), ImGuiInputTextFlags_CharsDecimal|ImGuiInputTextFlags_CharsNoBlank);
					ImGui::InputText("Port", portbuf.data(), portbuf.size(), ImGuiInputTextFlags_CharsDecimal|ImGuiInputTextFlags_CharsNoBlank);

					if (ImGui::Button("Create Server"))
					{
						network->CreateServer(1234);
						network->ConnectToServer("127.0.0.1", 1234);
					}

					ImGui::SameLine();

					if (ImGui::Button("Connect"))
					{
						uint16_t portNum = 0;
						std::from_chars(portbuf.data(), portbuf.data() + portbuf.size(), portNum);
						Log::Print("Connecting to {}:{}\n", ipbuf.data(), portNum);
						
						network->ConnectToServer(ipbuf.data(), portNum);
					}

					ImGui::End();
				}

				ImGui::Begin("Hello, world!", &drawUI); // Create a window called "Hello, world!" and append into it.

				float fieldOfView = view3D.fieldOfView;
				ImGui::SliderFloat("FOV", &fieldOfView, 10.0f, 170.0f);
				view3D.SetFOV(fieldOfView);

				if (ImGui::Button("Toggle MSAA"))
					graphics->ToggleMSAA();
				
				if (!network->IsConnected())
				{
					if (ImGui::Button("Network"))
						drawNetworkUI = true;
				}

				if (ImGui::Button("Quit"))
					signalQuit = true;

				ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
				ImGui::End();
			}
		}

		ImGui::Render();
		Transform modelTransform = {};
		modelTransform.position.y = -0.3f;
		modelTransform.scale = { 0.1f, 0.1f, 0.1f };

		scene->DrawModel(smaugMap, modelTransform);

		++frameTicks;
		frameTime += dt;

		if (frameTime >= 1000000)
		{
			Log::Print("FPS: {}\n", frameTicks);
			frameTime = 0;
			frameTicks = 0;
		}

		UpdateCamera();

		if (!drawUI)
			InputCamera(flDt);

		renderer->DrawUI(window->GetGuiContext());
		renderer->DrawScene(scene, view3D, camera->viewMatrix);

		renderer->BeginFrame();
		renderer->DrawFrame();
		renderer->EndFrame();

		renderer->ClearDrawLists();
		scene->ClearDrawLists();
	}

private:
	IGraphics *graphics = Get<IGraphics>();
	IInput *input = Get<IInput>();
	IImGuiService *imgui = Get<IImGuiService>();
	INetwork *network = Get<INetwork>();

	const ActionSetID ACTIONSET_FPS_CAMERA = input->CreateActionSet("FpsCamera");

	const Button IN_FORWARD =	input->BindKey(input->CreateButton(ACTIONSET_FPS_CAMERA, "Forward"), SDLK_w);
	const Button IN_BACK =		input->BindKey(input->CreateButton(ACTIONSET_FPS_CAMERA, "Back"), SDLK_s);
	const Button IN_LEFT =		input->BindKey(input->CreateButton(ACTIONSET_FPS_CAMERA, "Left"), SDLK_a);
	const Button IN_RIGHT =		input->BindKey(input->CreateButton(ACTIONSET_FPS_CAMERA, "Right"), SDLK_d);
	const Button IN_LOOKUP =	input->BindKey(input->CreateButton(ACTIONSET_FPS_CAMERA, "LookUp"), SDLK_UP);
	const Button IN_LOOKDOWN =	input->BindKey(input->CreateButton(ACTIONSET_FPS_CAMERA, "LookDown"), SDLK_DOWN);
	const Button IN_TURNLEFT =	input->BindKey(input->CreateButton(ACTIONSET_FPS_CAMERA, "TurnLeft"), SDLK_LEFT);
	const Button IN_TURNRIGHT =	input->BindKey(input->CreateButton(ACTIONSET_FPS_CAMERA, "TurnRight"), SDLK_RIGHT);
	const Button IN_JUMP =		input->BindKey(input->CreateButton(ACTIONSET_FPS_CAMERA, "Jump"), SDLK_SPACE);
	const Button IN_CROUCH =	input->BindKey(input->CreateButton(ACTIONSET_FPS_CAMERA, "Crouch"), SDLK_LCTRL);
	const Button IN_SPEED =		input->BindKey(input->CreateButton(ACTIONSET_FPS_CAMERA, "Speed"), SDLK_LSHIFT);
	const Button IN_ZOOMIN =	input->BindKey(input->CreateButton(ACTIONSET_FPS_CAMERA, "ZoomIn"), SDLK_KP_PLUS);
	const Button IN_ZOOMOUT =	input->BindKey(input->CreateButton(ACTIONSET_FPS_CAMERA, "ZoomOut"), SDLK_KP_MINUS);
	const Button IN_MENU =		input->BindKey(input->CreateButton(ACTIONSET_FPS_CAMERA, "Menu"), SDLK_ESCAPE);

	IWindow *window = graphics->CreateWindow("Thermite Engine", 1280, 720);
	//IWindow *window = graphics->CreateWindowFullscreen("Thermite Engine");
	IRenderer *renderer = graphics->CreateRenderer(window);

	IScene *scene = renderer->CreateScene();
	CEntityManager entityManager;
	Entity<Camera> camera;

	IModel *smaugMap = scene->GetModelCache()->LoadModel("models/test23_rexport.obj");
	std::vector<IBillboardSprite*> sprites;

	const float zNear = 0.01f;
	const float zFar = 10000.0f;
	const float FOV = 90.0f;

	CView3D view3D{0, 0, window->GetWidth(), window->GetHeight(), zNear, zFar, FOV, NULL_ENTITY<Camera>};
	IPointLight *pl1 = nullptr;
	IPointLight *pl2 = nullptr;
	IPointLight *pl3 = nullptr;

	float mX = 0.0f;
	float mY = 0.0f;

	uint64_t frameTicks = 0;
	uint64_t frameTime = 0;

	float elapsedTime = 0.0f;

	bool killed = false;
	bool signalQuit = false;
	bool drawUI = true;
	bool drawNetworkUI = false;
};

class CLauncherApp : public CThermiteApp
{
public:
	int AppMain(int, char *[]) override
	{
		/*if (argc < 2)
		{
			std::cerr << "Pass the path to thermite.toml as an argument please.\n";
			return 1;
		}*/

		LoadFromConfig("thermite.toml");
		CMainService mainService(this);

		EventLoop();

		return 0;
	}
};

int main(int argc, char *argv[])
{
	CLauncherApp app;
	return app.AppMain(argc, argv);
}
