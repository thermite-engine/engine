#include "ui/the_imgui.h"

#include <cassert>

namespace ThermiteImGui
{
	class CImGuiContextSetter : public IImGuiContextSetter
	{
	public:
		void SetCurrentContext(ImGuiContext *ctx) override
		{
			ImGui::SetCurrentContext(ctx);
		}
	};

	CImGuiContextSetter setter;
	IImGuiService *imgui = nullptr;

	void Init(CServer *server)
	{
		imgui = server->Get<IImGuiService>();
		imgui->RegisterContextSetter(&setter);
	}

	void Shutdown()
	{
		imgui->UnregisterContextSetter(&setter);
		imgui = nullptr;
	}

	void SetCurrentContext(ImGuiContext *ctx)
	{
		assert(imgui);
		imgui->SetCurrentContext(ctx);
	}
}