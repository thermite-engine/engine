# volk is not prepared to be a DLL.
vcpkg_check_linkage(ONLY_STATIC_LIBRARY)

vcpkg_from_github(
    OUT_SOURCE_PATH SOURCE_PATH
    REPO zeux/volk
    REF 760a782f295a66de7391d6ed573d65e3fb1c8450
    SHA512 0d946a2e117f8de335ce6453d8320b033a69f5ea945c57eea15331f6ba512f8ae303b4385dcd5b0ddfd5e8bd14a04ee518b768608afdc8404ecad0e108e0be99
    HEAD_REF master
)

vcpkg_configure_cmake(
    SOURCE_PATH ${SOURCE_PATH}
    PREFER_NINJA
    OPTIONS -DVOLK_INSTALL=ON
)
vcpkg_install_cmake()
vcpkg_copy_pdbs()
vcpkg_fixup_cmake_targets(CONFIG_PATH lib/cmake/volk)

file(REMOVE_RECURSE ${CURRENT_PACKAGES_DIR}/debug/include)

# Put the file containing the license where vcpkg expects it
file(COPY ${SOURCE_PATH}/README.md DESTINATION ${CURRENT_PACKAGES_DIR}/share/volk/)
file(RENAME ${CURRENT_PACKAGES_DIR}/share/volk/README.md ${CURRENT_PACKAGES_DIR}/share/volk/copyright)
