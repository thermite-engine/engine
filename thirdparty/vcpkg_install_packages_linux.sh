#!/bin/bash
./vcpkg/bootstrap-vcpkg.sh

./vcpkg/vcpkg install fmt:x64-linux --overlay-ports=../_vcpkg/overlay-ports
./vcpkg/vcpkg install glm:x64-linux --overlay-ports=../_vcpkg/overlay-ports
./vcpkg/vcpkg install volk:x64-linux --overlay-ports=../_vcpkg/overlay-ports
./vcpkg/vcpkg install tracy[callstack,on-demand]:x64-linux --overlay-ports=../_vcpkg/overlay-ports
./vcpkg/vcpkg install vulkan-memory-allocator:x64-linux --overlay-ports=../_vcpkg/overlay-ports
./vcpkg/vcpkg install tinyobjloader:x64-linux --overlay-ports=../_vcpkg/overlay-ports
./vcpkg/vcpkg install tinygltf:x64-linux --overlay-ports=../_vcpkg/overlay-ports
./vcpkg/vcpkg install enet:x64-linux --overlay-ports=../_vcpkg/overlay-ports