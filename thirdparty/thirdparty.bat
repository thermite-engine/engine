@echo off
rem I'm not a batch master so feel free to improve this file

rem We should no longer require being in VS command prompt
:: IF NOT DEFINED VSCMD_VER (
::	echo Please run this file in the Developer Command Prompt for Visual Studio
::	pause
::	EXIT /B
::)

rem Check for 7za
WHERE 7za >nul 2>nul
IF %ERRORLEVEL% NEQ 0 (
	echo Failed! Could not find 7za! Please download the standalone console version of 7-Zip from https://www.7-zip.org/download.html
	pause
	EXIT /B
)

rem Check for CMake
WHERE cmake >nul 2>nul
IF %ERRORLEVEL% NEQ 0 (
	echo Failed! Count not find CMake! Please download and install CMake from https://cmake.org/download/
	pause
	EXIT /B
)

echo Downloading thirdparty libraries...

:ULTRALIGHT
IF EXIST ultralight GOTO STEAMAUDIO
echo Downloading Ultralight
call download-unzip.bat https://github.com/ultralight-ux/Ultralight/releases/download/v1.1.0 ultralight-sdk-1.1.0-win-x64.7z ultralight
robocopy ultralight\bin dist\bin /S
robocopy ultralight\lib dist\lib /S
robocopy ultralight\include dist\include\Ultralight /S

:STEAMAUDIO
IF EXIST steamaudio_api GOTO DONE
echo Downloading Steam Audio
call download-unzip.bat https://github.com/ValveSoftware/steam-audio/releases/download/v2.0-beta.17 steamaudio_api_2.0-beta.17.zip .
robocopy steamaudio_api\bin\Windows\x64 dist\bin /S
robocopy steamaudio_api\lib\Windows\x64 dist\lib /S
robocopy steamaudio_api\include dist\include\steamaudio /S


:DONE
