@echo off
call "vcpkg/bootstrap-vcpkg.bat"

"vcpkg/vcpkg.exe" install sdl2[vulkan]:x64-mingw-dynamic --overlay-ports=../_vcpkg/overlay-ports
"vcpkg/vcpkg.exe" install fmt:x64-mingw-dynamic --overlay-ports=../_vcpkg/overlay-ports
"vcpkg/vcpkg.exe" install glm:x64-mingw-dynamic --overlay-ports=../_vcpkg/overlay-ports
"vcpkg/vcpkg.exe" install volk:x64-mingw-dynamic --overlay-ports=../_vcpkg/overlay-ports
"vcpkg/vcpkg.exe" install vulkan-memory-allocator:x64-mingw-dynamic --overlay-ports=../_vcpkg/overlay-ports
"vcpkg/vcpkg.exe" install tinyobjloader:x64-mingw-dynamic --overlay-ports=../_vcpkg/overlay-ports
"vcpkg/vcpkg.exe" install tinygltf:x64-mingw-dynamic --overlay-ports=../_vcpkg/overlay-ports
"vcpkg/vcpkg.exe" install enet:x64-mingw-dynamic --overlay-ports=../_vcpkg/overlay-ports