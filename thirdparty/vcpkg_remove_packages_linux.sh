#!/bin/bash
./vcpkg/bootstrap-vcpkg.sh

./vcpkg/vcpkg remove fmt:x64-linux --overlay-ports=../_vcpkg/overlay-ports
./vcpkg/vcpkg remove glm:x64-linux --overlay-ports=../_vcpkg/overlay-ports
./vcpkg/vcpkg remove volk:x64-linux --overlay-ports=../_vcpkg/overlay-ports
./vcpkg/vcpkg remove tracy[callstack,on-demand]:x64-linux --overlay-ports=../_vcpkg/overlay-ports
./vcpkg/vcpkg remove vulkan-memory-allocator:x64-linux --overlay-ports=../_vcpkg/overlay-ports
./vcpkg/vcpkg remove tinyobjloader:x64-linux --overlay-ports=../_vcpkg/overlay-ports
./vcpkg/vcpkg remove tinygltf:x64-linux --overlay-ports=../_vcpkg/overlay-ports
./vcpkg/vcpkg remove enet:x64-linux --overlay-ports=../_vcpkg/overlay-ports