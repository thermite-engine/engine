@echo off

call "vcpkg/bootstrap-vcpkg.bat"

"vcpkg/vcpkg.exe" remove sdl2[vulkan]:x64-windows --overlay-ports=../_vcpkg/overlay-ports
"vcpkg/vcpkg.exe" remove fmt:x64-windows --overlay-ports=../_vcpkg/overlay-ports
"vcpkg/vcpkg.exe" remove glm:x64-windows --overlay-ports=../_vcpkg/overlay-ports
"vcpkg/vcpkg.exe" remove volk:x64-windows --overlay-ports=../_vcpkg/overlay-ports
"vcpkg/vcpkg.exe" remove tracy[callstack,on-demand]:x64-windows --overlay-ports=../_vcpkg/overlay-ports
"vcpkg/vcpkg.exe" remove vulkan-memory-allocator:x64-windows --overlay-ports=../_vcpkg/overlay-ports
"vcpkg/vcpkg.exe" remove tinyobjloader:x64-windows --overlay-ports=../_vcpkg/overlay-ports
"vcpkg/vcpkg.exe" remove tinygltf:x64-windows --overlay-ports=../_vcpkg/overlay-ports
"vcpkg/vcpkg.exe" remove enet:x64-windows --overlay-ports=../_vcpkg/overlay-ports