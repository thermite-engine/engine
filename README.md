# How to Clone Repository

Clone the repository recursively using git clone --recurse https://gitlab.com/thermite-engine/engine.git

# Prerequisites

## General:
- CMake
- Vulkan SDK

## Linux:
- SDL2

Setup and install the Vulkan SDK from here: https://vulkan.lunarg.com/sdk/home

If you're on Linux, you may install the Vulkan development packages for your distro instead of setting up the Vulkan SDK.

# Setting up Thirdparty Libraries

## MSVC 2019:
In the thirdparty directory, run vcpkg_install_packages_msvc.bat, and run thirdparty.bat and follow any instructions printed if either fails.

You should now be able to generate VS2019 solutions or use CMake integration and build

## MinGW-w64/MSYS2:
Navigate to the engine/thirdparty directory from your shell and run vcpkg_install_packages_mingw.bat.

How you build with and use MinGW is up to you. You may build and run from your shell, add your MinGW's bin folder to your PATH, or gather your binaries manually from your installation.

When generating projects with CMake, be sure to pass -DVCPKG_TARGET_TRIPLET=x64-mingw-dynamic